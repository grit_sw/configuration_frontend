#!/bin/bash

docker run -it \
    -v ${PWD}:/usr/src/app \
    -v /usr/src/app/node_modules \
    --name configurations_frontend \
    -p 3000:3000 \
    --rm dashboard_frontend
