# build environment
FROM node:11.11.0 as node
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
RUN npm install
RUN npm install react-scripts@3.0.1
COPY . /usr/src/app

RUN npm run build



# CMD ["serve", "-s", "build", "-l", "5603"]

# # stage 2
FROM nginx:1.16
COPY --from=node /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
# COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
