import * as React from "react";
import cookie from "react-cookies";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./component/Header/Header";
import UserWindow from "./component/UserWindow/UserWindow";
import { UserDetails } from "./Services/UsersRequests/UsersRequests";

interface Istate {
    id: string;
    listonephasegrid: any[];
    listthreephasegrid: any[];
    open1: boolean;
    test: string;
}

interface SetRouteProps {
    user_token: string | null;
    user_role_id: string | null;
    user_id: string | null;
}

localStorage.setItem("user_token", cookie.load("userToken"));
localStorage.setItem("user_role", cookie.load("userRole"));


const verifyTokenByGettingUserDetails = () => {
    UserDetails()
        .then(res => {
            // everything is fine so do nothing
            // tslint:disable
            // console.log(res.data.data.user_id)
            localStorage.setItem("user_id", res.data.data.user_id);
            // console.log(localStorage.getItem("user_id"))
            // console.log(res);
        })
        .catch(err => {
            if (localStorage.getItem("user_id") === null) {
                // window.location.href = "http://mydomain.com:3000";
                // localStorage.clear();
                // location.reload()
                // if (err) {
                window.location.href = "https://dev.gtg.grit.systems";
                // }
            } else if (err.response.status === 400) {
                window.location.href = "https://dev.gtg.grit.systems";
            }
        });
};

function SetRoutes(props: SetRouteProps) {
    const userToken = props.user_token;
    const userRole = props.user_role_id;
    const userId = localStorage.getItem("user_id")

    // console.log(props.user_role_id)

    verifyTokenByGettingUserDetails();

    if (userToken && (userRole === "5" || userRole === "4" || userRole === "3" || userRole === "2" || userRole === "1")) {
        // location.reload()
        return (
            <BrowserRouter>
                <div>
                    <Switch>
                        <Route path="/" component={Header} />
                    </Switch>
                    <Switch>
                        <Route path="/"
                            render={props => (
                                <UserWindow
                                    {...props}
                                    user_role={userRole}
                                    user_id={userId}
                                />
                            )}
                        />
                    </Switch>
                </div>
            </BrowserRouter>

        );
    } else if (userRole === "null" || userRole === "undefined" || !userRole) {
        window.location.href = "https://dev.gtg.grit.systems";
        // window.location.href = "http://mydomain.com:3001";
    }
}

class App extends React.Component<{}, Istate> {
    userToken: string | null = localStorage.getItem("user_token");
    userRoleId: string | null = localStorage.getItem("user_role");
    userName: string | null = localStorage.getItem("user_name");
    userId: string | null = localStorage.getItem("user_id");

    constructor(props) {
        super(props);

        this.state = {
            id: "",
            listonephasegrid: [],
            listthreephasegrid: [],
            open1: false,
            test: ""
        };
    }

    public componentWillMount() {
        localStorage.setItem("user_token", cookie.load("userToken"));
        localStorage.setItem("user_role", cookie.load("userRole"));
        localStorage.setItem("user_name", cookie.load("userName"));
    }

    handleClose1 = () => {
        this.setState({ open1: false });
    };

    public render() {


        return (
            // <BrowserRouter>
            // @ts-ignore
            <SetRoutes
                user_token={this.userToken}
                user_role_id={this.userRoleId}
            />
            // </BrowserRouter>
            // <div>
            //     <BrowserRouter>
            //         <div>
            //             <Switch>
            //                 <Route path="/" component={Header} />
            //             </Switch>
            //             <Switch>
            //                 <Route path="/" component={UserWindow} />
            //             </Switch>
            //         </div>
            //     </BrowserRouter>
            // </div>
        );
    }
}

export default App;
