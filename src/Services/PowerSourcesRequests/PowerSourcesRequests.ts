import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REaACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

// export const CreateLine3 = body => {
//   return axios.post(
//     `${MyBaseUrl}/three-phase-consumer-line/new/`,
//     body,
//     axiosConfig
//   );
// };

// export const CreateLine1 = body => {
//   return axios.post(
//     `${MyBaseUrl}/single-phase-consumer-line/new/`,
//     body,
//     axiosConfig
//   );
// };

export const ListOnePhaseGrid = () => {
  return axios.get(`${MyBaseUrl}/single-phase-grid/all/`, axiosConfig);
};

export const ListThreePhaseGrid = () => {
  return axios.get(`${MyBaseUrl}/three-phase-grid/all/`, axiosConfig);
};

export const ListOnePhaseGenerator = () => {
  return axios.get(`${MyBaseUrl}/single-phase-generator/all/`, axiosConfig);
};

export const ListThreePhaseGenerator = () => {
  return axios.get(`${MyBaseUrl}/three-phase-generator/all/`, axiosConfig);
};

export const ListOnePhaseInverter = () => {
  return axios.get(`${MyBaseUrl}/single-phase-inverter/all/`, axiosConfig);
};

export const ListThreePhaseInverter = () => {
  return axios.get(`${MyBaseUrl}/three-phase-inverter/all/`, axiosConfig);
};

export const CreateOnePhaseGrid = body => {
  return axios.post(`${MyBaseUrl}/single-phase-grid/new/`, body, axiosConfig);
};

export const CreateThreePhaseGrid = body => {
  return axios.post(`${MyBaseUrl}/three-phase-grid/new/`, body, axiosConfig);
};

export const CreateOnePhaseGenerator = body => {
  return axios.post(
    `${MyBaseUrl}/single-phase-generator/new/`,
    body,
    axiosConfig
  );
};

export const CreateThreePhaseGenerator = body => {
  return axios.post(
    `${MyBaseUrl}/three-phase-generator/new/`,
    body,
    axiosConfig
  );
};

export const CreateOnePhaseInverter = body => {
  return axios.post(
    `${MyBaseUrl}/single-phase-inverter/new/`,
    body,
    axiosConfig
  );
};

export const CreateThreePhaseInverter = body => {
  return axios.post(
    `${MyBaseUrl}/three-phase-inverter/new/`,
    body,
    axiosConfig
  );
};

export const ViewOnePhaseGrid = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/single-phase-grid/one/${GridId}/
    `,
    axiosConfig
  );
};

export const ViewThreePhaseGrid = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/three-phase-grid/one/${GridId}/
    `,
    axiosConfig
  );
};

export const ViewOnePhaseGen = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/single-phase-generator/one/${GridId}/
    `,
    axiosConfig
  );
};

export const ViewThreePhaseGen = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/three-phase-generator/one/${GridId}/
    `,
    axiosConfig
  );
};

export const ViewOnePhaseInv = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/single-phase-inverter/one/${GridId}/
    `,
    axiosConfig
  );
};

export const ViewThreePhaseInv = (GridId: string) => {
  return axios.get(
    `${MyBaseUrl}/three-phase-inverter/one/${GridId}/
    `,
    axiosConfig
  );
};

export const EditOnePhaseGen = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/single-phase-generator/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const EditThreePhaseGen = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/three-phase-generator/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const EditOnePhaseGrid = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/single-phase-grid/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const EditThreePhaseGrid = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/three-phase-grid/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const EditOnePhaseInv = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/single-phase-inverter/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const EditThreePhaseInv = (body, GenId: string) => {
  return axios.put(
    `${MyBaseUrl}/three-phase-inverter/edit/${GenId}/`,
    body,
    axiosConfig
  );
};

export const DeleteOnePhaseGrid = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/single-phase-grid/one/${groupId}/`,
    axiosConfig
  );
};

export const DeleteThreePhaseGrid = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/three-phase-grid/one/${groupId}/`,
    axiosConfig
  );
};

export const DeleteOnePhaseGen = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/single-phase-generator/one/${groupId}/`,
    axiosConfig
  );
};

export const DeleteThreePhaseGen = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/three-phase-generator/one/${groupId}/`,
    axiosConfig
  );
};

export const DeleteOnePhaseInv = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/single-phase-inverter/one/${groupId}/`,
    axiosConfig
  );
};

export const DeleteThreePhaseInv = (groupId: string) => {
  return axios.delete(
    `${MyBaseUrl}/three-phase-inverter/one/${groupId}/`,
    axiosConfig
  );
};

export const ListChannels = () => {
  return axios.get(
    `${MyBaseUrl}/configuration/one/{configuration_id}/
    `,
    axiosConfig
  );
};

export const ListConfiguration = (userId: string) => {
  return axios.get(
    `${MyBaseUrl}/configuration/one/${userId}/
      `,
    axiosConfig
  );
};

export const DeleteFuels = (fuelId: string) => {
  return axios.delete(
    `${MyBaseUrl}/consumer-group/one/${fuelId}/`,
    axiosConfig
  );
};

export const ViewGroups = (fuelId: string) => {
  return axios.get(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
};

// /single-phase-grid/all/
