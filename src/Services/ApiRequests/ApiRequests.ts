import axios from "axios";


const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

// export const ListProbe = () => {
// 	return axios.get(`${MyBaseUrl}/probe/all/`, axiosConfig);
// };

export const DeviceType = () => {
  return axios.get(`${MyBaseUrl}/device-type/all/`, axiosConfig);
};

export const GetUserDetails = () => {
  return axios.get(`${MyBaseUrl}/users/me/`, axiosConfig);
};


// times(this.state.channelnumber, (i) =>{
//   body.channels.push({
//         "channel_threshold_current": 0,
//      "power_factor": 0,
//      "default_voltage": 0,
//      "scaling_factor_voltage": 0,
//      "scaling_factor_current": 0,
//      "sensor_type_id": "string"
//   })
// })
