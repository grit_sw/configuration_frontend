import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
    headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateSensor = body => {
    return axios.post(`${MyBaseUrl}/sensor-type/new/`, body, axiosConfig);
};

export const ListSensor = () => {
    return axios.get(`${MyBaseUrl}/sensor-type/all/`, axiosConfig);
};

export const ViewSensor = (sensortypeId: string) => {
    return axios.get(
        `${MyBaseUrl}/sensor-type/one/${sensortypeId}/`,
        axiosConfig
    );
};

export const EditSensor = (body, sensortypeId: string) => {
    return axios.put(
        `${MyBaseUrl}/sensor-type/edit/${sensortypeId}/`,
        body,
        axiosConfig
    );
};

export const ListUserSensor = () => {
    return axios.get(`${MyBaseUrl}/configuration/user/my-power-sources/`, axiosConfig);
};
