import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
    headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

// export const CreateFuels = (body) => {
//     return axios.post(`${MyBaseUrl}/fuel/new/`, body, axiosConfig);
// };

export const ListRoles = () => {
    return axios.get(`${MyBaseUrl}/users/role/all/`, axiosConfig);
};

// export const EditFuels = (body, fuelId:string) => {
//   return axios.put(`${MyBaseUrl}/fuel/edit/${fuelId}/`, body, axiosConfig);
// };

// export const DeleteFuels = (fuelId:string) => {
//   return axios.delete(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
// };

export const CreateProbes = body => {
    return axios.post(`${MyBaseUrl}/probe/new/`, body, axiosConfig);
};

export const EditProbes = body => {
    return axios.post(`${MyBaseUrl}/probe/edit/`, body, axiosConfig);
};

export const EditProbes2 = (body, probename: string) => {
    return axios.post(`${MyBaseUrl}/save/probe/${probename}/`, body, axiosConfig);
};

export const ListProbe = () => {
    return axios.get(`${MyBaseUrl}/probe/all/`, axiosConfig);
};

export const ViewProbes = (probeid: string) => {
    return axios.get(`${MyBaseUrl}/probe/by-probe-id/${probeid}/`, axiosConfig);
};

export const ViewUserProbes = (userid: string) => {
    return axios.get(`${MyBaseUrl}/probe/user/${userid}/`, axiosConfig);
};

export const DeviceType = () => {
    return axios.get(`${MyBaseUrl}/device-type/all/`, axiosConfig);
};

export const VoltageSensor = () => {
    return axios.get(`${MyBaseUrl}/voltage-sensor/all/`, axiosConfig);
};

export const CurrentSensor = () => {
    return axios.get(`${MyBaseUrl}/current-sensor/all/`, axiosConfig);
};

