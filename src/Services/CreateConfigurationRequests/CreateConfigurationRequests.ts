import axios from "axios";


const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};


const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateConfigurations = (body) => {
    return axios.post(`${MyBaseUrl}/configuration/new/`, body, axiosConfig);
};