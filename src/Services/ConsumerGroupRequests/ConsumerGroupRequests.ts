import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";


export const CreateGroup = (body) => {
    return axios.post(`${MyBaseUrl}/consumer-group/new/`, body, axiosConfig);
};

export const CreateGroupType = (body) => {
    return axios.post(`${MyBaseUrl}/consumer-group-type/new/`, body, axiosConfig);
};

export const ListGroups = () => {
    return axios.get(`${MyBaseUrl}/consumer-group/all/`, axiosConfig);
};

export const ListGroupTypes = () => {
    return axios.get(`${MyBaseUrl}/consumer-group-type/all/`, axiosConfig);
};

export const EditGroups = (body, GroupId:string) => {
  return axios.put(`${MyBaseUrl}/consumer-group/edit/${GroupId}/`, body, axiosConfig);
};

export const DeleteGroup = (GroupId:string) => {
  return axios.delete(`${MyBaseUrl}/consumer-group/one/${GroupId}/`, axiosConfig);
};

export const ViewGroups = (GroupId:string) => {
  return axios.get(`${MyBaseUrl}/consumer-group/one/${GroupId}/`, axiosConfig);
};

export const CreateConsumerOne = (body) => {
    return axios.post(`${MyBaseUrl}/single-phase-consumer/new/`, body, axiosConfig);
};

export const CreateConsumerThree = (body) => {
    return axios.post(`${MyBaseUrl}/three-phase-consumer/new/`, body, axiosConfig);
};