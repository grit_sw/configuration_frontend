import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
    headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REaACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateLine3 = body => {
    return axios.post(
        `${MyBaseUrl}/three-phase-consumer-line/new/`,
        body,
        axiosConfig
    );
};

export const CreateLine1 = body => {
    return axios.post(
        `${MyBaseUrl}/single-phase-consumer-line/new/`,
        body,
        axiosConfig
    );
};

export const ListGroupType = () => {
    return axios.get(`${MyBaseUrl}/consumer-group-type/all/`, axiosConfig);
};

export const ListGroups = () => {
    return axios.get(`${MyBaseUrl}/consumer-group/all/`, axiosConfig);
};

export const ListProbe = () => {
    return axios.get(`${MyBaseUrl}/probe/all/`, axiosConfig);
};

export const DeleteGroup = (groupId: string) => {
    return axios.get(`${MyBaseUrl}/consumer-group/one/${groupId}/`, axiosConfig);
};

export const ListChannels = () => {
    return axios.get(
        `${MyBaseUrl}/configuration/one/{configuration_id}/
    `,
        axiosConfig
    );
};

export const ListConfiguration = (userId: string) => {
    return axios.get(
        `${MyBaseUrl}/configuration/one/${userId}/
      `,
        axiosConfig
    );
};

export const List3PhaseConsumerLine = () => {
    return axios.get(
        `${MyBaseUrl}/three-phase-consumer-line/all/
      `,
        axiosConfig
    );
};

export const ListOnePhaseConsumerLine = () => {
    return axios.get(
        `${MyBaseUrl}/single-phase-consumer-line/all/
      `,
        axiosConfig
    );
};

export const Delete3PhaseConsumerLine = (fuelId: string) => {
    return axios.delete(
        `${MyBaseUrl}/three-phase-consumer-line/one/${fuelId}/`,
        axiosConfig
    );
};

export const DeleteOnePhaseConsumerLine = (fuelId: string) => {
    return axios.delete(
        `${MyBaseUrl}/single-phase-consumer-line/one/${fuelId}/`,
        axiosConfig
    );
};

export const EditFuels = (body, fuelId: string) => {
    return axios.put(`${MyBaseUrl}/fuel/edit/${fuelId}/`, body, axiosConfig);
};

export const DeleteFuels = (fuelId: string) => {
    return axios.delete(
        `${MyBaseUrl}/consumer-group/one/${fuelId}/`,
        axiosConfig
    );
};

export const ViewGroups = (fuelId: string) => {
    return axios.get(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
};

export const UserLine = (userId: string) => {
    return axios.get(`${MyBaseUrl}/configuration/user/all-consumers/${userId}/`, axiosConfig);
};

export const UserConsumerLine = () => {
    return axios.get(`${MyBaseUrl}/configuration/user/my-power-sources/`, axiosConfig);
};