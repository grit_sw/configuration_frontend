import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";


export const CreateFuels = (body) => {
    return axios.post(`${MyBaseUrl}/fuel/new/`, body, axiosConfig);
};

export const ListFuels = () => {
    return axios.get(`${MyBaseUrl}/fuel/all/`, axiosConfig);
};

export const EditFuels = (body, fuelId:string) => {
  return axios.put(`${MyBaseUrl}/fuel/edit/${fuelId}/`, body, axiosConfig);
};

export const DeleteFuels = (fuelId:string) => {
  return axios.delete(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
};

export const ViewFuels = (fuelId:string) => {
  return axios.get(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
};

export const ViewUserFuels = (userId:string) => {
  return axios.get(`${MyBaseUrl}/fuel/user/${userId}/`, axiosConfig);
};

export const ViewUserFuels1 = (userId:string) => {
    return axios.get(`${MyBaseUrl}/fuel/user/${userId}/`, axiosConfig);
  };