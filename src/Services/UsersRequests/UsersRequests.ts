import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
    headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";


// export const CreateFuels = (body) => {
//     return axios.post(`${MyBaseUrl}/fuel/new/`, body, axiosConfig);
// };

export const ListRoles = () => {
    return axios.get(`${MyBaseUrl}/users/role/all/`, axiosConfig);
};

// export const EditFuels = (body, fuelId:string) => {
//   return axios.put(`${MyBaseUrl}/fuel/edit/${fuelId}/`, body, axiosConfig);
// };

// export const DeleteFuels = (fuelId:string) => {
//   return axios.delete(`${MyBaseUrl}/fuel/one/${fuelId}/`, axiosConfig);
// };

export const ListUser = () => {
    return axios.get(`${MyBaseUrl}/users/all/`, axiosConfig);
};

export const ListRole5 = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/5/`, axiosConfig);
};

export const ListRole4 = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/4/`, axiosConfig);
};

export const ListRole3 = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/3/`, axiosConfig);
};

export const ListRole2 = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/2/`, axiosConfig);
};

export const GroupDetail = (groupId: string) => {
    return axios.get(`${MyBaseUrl}/user-groups/id/${groupId}/`, axiosConfig);
};

export const ListGroups = () => {
    return axios.get(`${MyBaseUrl}/user-groups/all/`, axiosConfig);
};

export const UserDetails = () => {
    return axios.get(`${MyBaseUrl}/users/me/`, axiosConfig);
};

export const OtherUserDetails = (userId: string) => {
    return axios.get(`${MyBaseUrl}/users/id/${userId}/`, axiosConfig);
};

export const ConfigUsers = () => {
    return axios.get(`${MyBaseUrl}/users/configuration/all/`, axiosConfig);
};

export const DeleteUsers = (userId: string) => {
    return axios.get(`${MyBaseUrl}/users/delete/${userId}/`, axiosConfig);
};

// GET /users/configuration/all/