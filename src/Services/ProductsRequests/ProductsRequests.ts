import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateDevice = body => {
  return axios.post(`${MyBaseUrl}/device-type/new/`, body, axiosConfig);
};

export const ListDevice = () => {
  return axios.get(`${MyBaseUrl}/device-type/all/`, axiosConfig);
};

export const ViewDevicess = (devicetypeid: string) => {
  return axios.get(
    `${MyBaseUrl}/device-type/one/${devicetypeid}/`,
    axiosConfig
  );
};

export const EditDevices = (body, devicetypeid: string) => {
  return axios.put(
    `${MyBaseUrl}/device-type/edit/${devicetypeid}/`,
    body,
    axiosConfig
  );
};

