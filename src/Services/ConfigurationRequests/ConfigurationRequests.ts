import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
    headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateConfigurationGroups = body => {
    return axios.post(`${MyBaseUrl}/configuration-group/new/`, body, axiosConfig);
};

export const GroupAdmin = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/5/`, axiosConfig);
};

export const GroupAdmin2 = () => {
    return axios.get(`${MyBaseUrl}/users/by-role/4/`, axiosConfig);
};

export const SourceConfig = (groupAdminId: string) => {
    return axios.get(
        `${MyBaseUrl}/configuration/all/${groupAdminId}/`,
        axiosConfig
    );
};

export const CreateConfigurations = body => {
    return axios.post(`${MyBaseUrl}/configuration/new/`, body, axiosConfig);
};

export const SelectProbes = () => {
    return axios.get(`${MyBaseUrl}/probe/all/`, axiosConfig);
};

export const ListConfigGroup = () => {
    return axios.get(`${MyBaseUrl}/configuration-group/all/`, axiosConfig);
};

export const ListConfig = () => {
    return axios.get(`${MyBaseUrl}/configuration/all/`, axiosConfig);
};

export const EditConfig = (body, configurationId: string) => {
    return axios.post(
        `${MyBaseUrl}/configuration/edit/${configurationId}/`,
        body,
        axiosConfig
    );
};

export const ViewConfig = (configurationId: string) => {
    return axios.get(
        `${MyBaseUrl}/configuration/one/${configurationId}/`,
        axiosConfig
    );
};

export const ListConfigurations = (userId: string) => {
    return axios.get(
        `${MyBaseUrl}/configuration/user-configurations/${userId}/`,
        axiosConfig
    );
};

export const ListUserPowerSources = (userId: string) => {
    return axios.get(
        `${MyBaseUrl}/configuration/user/all-power-sources/${userId}/`,
        axiosConfig
    );
};
