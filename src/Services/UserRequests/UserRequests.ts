import axios from "axios";

const USER_TOKEN = localStorage.getItem('user_token')
const axiosConfig = {
  headers: { "X-API-KEY": USER_TOKEN }
};

// const MyBaseUrl =
//   process.env.REACT_APP_BACKEND_SERVER || "http://dos.grit.systems:5500";
const MyBaseUrl = "http://dev-backend.gtg.grit.systems:5500";

export const CreateConfigurationGroup = (body) => {
    return axios.post(`${MyBaseUrl}/configuration-group/new/`, body, axiosConfig);
  };

export const GroupAdmin = () => {
	return axios.get(`${MyBaseUrl}/users/by-role/4/`, axiosConfig);
};

export const SourceConfig = () => {
	return axios.get(`${MyBaseUrl}/configuration/all/0df1928f-1265-4b0d-bc45-ad726c6431d1/`, axiosConfig);
};

export const CreateConfigurations = (body) => {
  return axios.post(`${MyBaseUrl}/configuration/new/`, body, axiosConfig);
};

export const SelectProbes = () => {
  return axios.get(`${MyBaseUrl}/probe/all/`, axiosConfig);
};

