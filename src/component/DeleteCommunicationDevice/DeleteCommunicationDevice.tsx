import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
// import TextField from "@material-ui/core/TextField";
import * as React from "react";
import "./DeleteCommunicationDevice.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
}

class DeleteCommunicationDevice extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      name: [],
      open: true,
      sourceconfig: "",
      // profitmargin: '',
      value: ""
    };
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Probe</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Typography>
                  The Communication Device B8:27:EB:F2:8E:52 will be deleted
                </Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                >
                  Save Device
                </Button>
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                >
                  Save Device
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default DeleteCommunicationDevice;
