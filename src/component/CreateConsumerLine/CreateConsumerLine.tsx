import Button from "@material-ui/core/Button";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import "./CreateConsumerLine.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};
class CreateConsumerLine extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Select Power Consumer Type</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Link to="/consumer/create-consumer-line3" style={style}>
                  <Button
                    variant="contained"
                    style={{ backgroundColor: "#337ab7" }}
                  >
                    3 Phase Consumer
                  </Button>
                </Link>
              </TableCell>
              <TableCell>
                <Link to="/consumer/create-consumer-line1" style={style}>
                  <Button
                    variant="contained"
                    style={{ backgroundColor: "#337ab7" }}
                  >
                    1 Phase Consumer
                  </Button>
                </Link>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default CreateConsumerLine;
