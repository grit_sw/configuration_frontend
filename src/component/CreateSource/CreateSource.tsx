import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import "./CreateSource.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class CreateSource extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Power Sources associated with: user</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <ul className="sourcetable">
                  <li className="sourcetable">
                    <Link to="/source/create-grid3" style={style}>
                      3 Phase GRID
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-gen3" style={style}>
                      3 Phase Generator
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-inv3" style={style}>
                      3 Phase Inverter
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-solar-ac" style={style}>
                      Solar Ac
                    </Link>
                  </li>
                </ul>
              </TableCell>
            </TableRow>

            <TableRow className="tablerow">
              <TableCell>
                <ul className="sourcetable">
                  <li className="sourcetable">
                    <Link to="/source/create-grid1" style={style}>
                      Single Phase GRID
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-gen1" style={style}>
                      Single Phase Generator
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-inv1" style={style}>
                      Single Phase Inverter
                    </Link>
                  </li>

                  <li className="sourcetable">
                    <Link to="/source/create-solar-ac" style={style}>
                      Solar DC
                    </Link>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default CreateSource;
