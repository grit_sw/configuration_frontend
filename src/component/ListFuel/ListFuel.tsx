import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import { DeleteFuels, ListFuels, ViewUserFuels } from "../../Services/FuelRequests/FuelRequests";
import "./ListFuel.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Istate {
    id: string;
    listfuels: any[];
    open1a: boolean;
    open1b: boolean;
    display: boolean;
    error: string;
    viewuserfuel: any[];
    admin: boolean
    display2: boolean
}

interface Iprops {
    user;
    username;
    display2;
    userfuel;
    user_role
    user_id
}

class ListFuel extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            admin: false,
            display: false,
            display2: false,
            error: "",
            id: "",
            listfuels: [],
            open1a: false,
            open1b: false,
            viewuserfuel: []
        };
    }

    public componentWillMount() {
        ListFuels()
            .then(res => {
                this.setState({ listfuels: res.data.data });
                // tslint:disable
                console.log(this.state.listfuels);
                console.log(this.state.listfuels.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }

        console.log(this.props.user_id)
        let user = this.props.user_id

        ViewUserFuels(user)
            .then(res => {
                this.setState({ viewuserfuel: res.data.data, display2: true });
                // tslint:disable
                console.log(this.state.viewuserfuel);
                // console.log(this.state.viewuserfuel.length);
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display2: false });
                console.log(this.state.error);
            });
    }

    public handleSubmitFuel = () => {
        const fuelid = this.state.id;
        // @ts-ignore

        // @ts-ignore
        DeleteFuels(fuelid)
            .then(res => {
                this.setState({ open1b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    handleClickOpen = id => {
        console.log(id);
        this.setState({ open1a: true, id: id });
    };

    handleClose = () => {
        this.setState({ open1a: false, open1b: false });
    };

    public CreateColumn(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/fuel/view/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/fuel/edit/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.handleClickOpen.bind(this, id)}
                        style={{ border: "none", margin: 0 }}
                    >
                        Delete
                    </Button>
                    <Dialog
                        open={this.state.open1a}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are You Sure You Want To Delete This?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Disagree
                             </Button>
                            <Button
                                onClick={this.handleSubmitFuel.bind(this, id)}
                                color="primary"
                                autoFocus
                            >
                                Agree
                            </Button>
                            <Dialog
                                open={this.state.open1b}
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Deleted
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose} color="primary">
                                        Ok
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </DialogActions>
                    </Dialog>
                </li>
            </ul>
        );
    }

    public render() {
        const Fueltype = this.state.listfuels.map((ListFuel, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{this.CreateColumn(ListFuel.fuel_id)}</TableCell>
            </TableRow>
        ));
        const FuelTypelength = this.state.listfuels.length;
        console.log(FuelTypelength);

        const userId = this.props.user;
        const userName = this.props.username;

        console.log(userId);
        console.log(userName);

        const UserFuel = !this.props.userfuel ? <div />
            : this.props.userfuel.map((ListFuel, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell>{ListFuel.name}</TableCell>
                    <TableCell>{this.CreateColumn(ListFuel.fuel_id)}</TableCell>
                </TableRow>
            ));

        const NonAdminUserFuel = this.state.viewuserfuel.map((ListFuel, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{this.CreateColumn(ListFuel.fuel_id)}</TableCell>
            </TableRow>
        ));

        const userfuel = this.props.userfuel;

        const userfuellength = userfuel.length;

        return (
            <div>
                {(!this.props.display2 && userfuellength === 0) || (this.props.display2 && userfuellength === 0) ? (
                    <div>
                        {this.props.username ? (
                            <div>{userName} Has No Fuel</div>
                        ) : (
                                <div>Select a user to view their Fuel</div>
                            )}
                    </div>
                ) : (
                        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                            <TableHead className="head">
                                <TableRow className="tablecell">
                                    <TableCell className="tablecell">
                                        <span id="tablecell">Fuel associated with {userName}</span>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className="tablerow">
                                    <TableCell>Fuel Type</TableCell>
                                    <TableCell>Action</TableCell>
                                </TableRow>
                                {UserFuel}
                            </TableBody>
                        </Table>
                    )}

                {this.state.admin ? (
                    <div>
                        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                            <TableHead className="head">
                                <TableRow className="tablecell">
                                    <TableCell className="tablecell">
                                        <span id="tablecell">Fuel</span>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className="tablerow">
                                    <TableCell>Fuel Type</TableCell>
                                    <TableCell>Action</TableCell>
                                </TableRow>
                                {Fueltype}
                            </TableBody>
                        </Table>
                    </div>
                ) : (
                        <div>
                            <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Fuel</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Fuel Type</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {NonAdminUserFuel}
                                </TableBody>
                            </Table>
                        </div>
                    )}
            </div>
        );
    }
}

export default ListFuel;
