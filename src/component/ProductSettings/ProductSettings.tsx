import * as React from "react";
import { Link } from "react-router-dom";
import "./ProductSettings.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class ProductSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className="lists">
          <li className="lists">
            <Link to="/sensors/list-sensors" style={style}>
              Sensors
            </Link>
          </li>

          <li className="lists">
            <Link to="/sensors/create-sensor" style={style}>
              Create Sensors
            </Link>
          </li>

          <li className="lists">
            <Link to="/devices/list-devices" style={style}>
              Devices
            </Link>
          </li>

          <li className="lists">
            <Link to="/devices/create-device" style={style}>
              Create Device
            </Link>
          </li>

          <li className="lists">
            <Link to="/products/list-products" style={style}>
              Products
            </Link>
          </li>

          <li className="lists">
            <Link to="/products/create-product" style={style}>
              Create Product
            </Link>
          </li>

          <li className="lists">
            <Link to="/products/list-comm-device" style={style}>
              Communication Device
            </Link>
          </li>

          <li className="listss">
            <Link to="/products/create-comm-device" style={style}>
              Create Communication Device
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default ProductSettings;
