import { Typography } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { CurrentSensor, ViewProbes, VoltageSensor } from "../../Services/ProbeRequests/ProbeRequests";
import "./ViewProbe.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    viewprobes: {};
    channels: number
    device_type: string
    wifi_network_name: string
    wifi_password: string
    channellist: any[]
    voltagesensorid: any[]
    currentsensorid: any[]
}

class ViewProbe extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            channellist: [],
            channels: 0,
            configs: true,
            currentsensorid: [],
            device_type: "",
            groupadmin: "",
            name: [],
            open: true,
            sourceconfig: "",
            // profitmargin: '',
            value: "",
            viewprobes: {},
            voltagesensorid: [],
            wifi_network_name: "",
            wifi_password: ""
        };
    }

    public componentWillMount() {
        // @ts-ignore works
        const probeid = this.props.match.params.id;

        // tslint:disable
        console.log(probeid);

        ViewProbes(probeid)
            .then(res => {
                this.setState({
                    viewprobes: res.data.data,
                    name: res.data.data.probe_name,
                    channels: res.data.data.channels.length,
                    device_type: res.data.data.device_type.name,
                    wifi_network_name: res.data.data.wifi_network_name,
                    wifi_password: res.data.data.wifi_password,
                    channellist: res.data.data.channels
                });
                // tslint:disable
                console.log(res.data.data);
                console.log(res.data.data.device_type.name);

                // console.log(this.state.listfuels.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        VoltageSensor()
            .then(res => {
                this.setState({
                    voltagesensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
                console.log(this.state.voltagesensorid);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        CurrentSensor()
            .then(res => {
                this.setState({
                    currentsensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
                console.log(this.state.currentsensorid);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public renderDetails = props => {

        const CurrentSensors = this.state.currentsensorid.map(
            (CurrentSensor, id) => (
                <MenuItem key={id} value={CurrentSensor.sensor_id}>
                    {CurrentSensor.name}
                </MenuItem>
            )
        );

        const CurrentSensorRatings = !this.state.channellist[props.name]["current_sensor_id"] ? ""
            : this.state.channellist[props.name]["current_sensor_id"].map(
                (CurrentSensorRating, id) => (
                    <MenuItem key={id} value={CurrentSensorRating.rating_id}>
                        {CurrentSensorRating.rating}
                    </MenuItem>
                )
            )

        const VoltageSensors = this.state.voltagesensorid.map(
            (VoltageSensor, id) => (
                <MenuItem key={id} value={VoltageSensor.sensor_id}>
                    {VoltageSensor.name}
                </MenuItem>
            )
        );

        // const sensorSelection = () => {
        //     let sensorId = this.state.channellist[props.name]['current_sensor'].sensor_id;
        //     // let probeId = "cbhdbhf-dmjn-dmf";
        //     console.log(sensorId);
        //     const selRating = this.state.currentsensorid.find(
        //         sensor => sensor.sensor_id === sensorId
        //     );
        //     console.log(selRating.ratings);

        //     let sensorArr = [...this.state.selrating];
        //     sensorArr[props.name] = selRating.ratings;

        //     this.setState({ selrating: sensorArr }, () => {
        //         console.log(this.state.selrating);
        //     });
        // };

        return (
            <div>
                <TableRow className="tablerow">
                    <TableCell>
                        <div className="viewlistprobeoptions">
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]["channel_name"]
                                    }
                                    inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                    }}
                                    name="channels"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    style={{ width: 100 }}
                                    font-size="true"
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["channel_threshold_current"]
                                            : ""
                                    }
                                    name="channelthreshold"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["power_factor"]
                                            : ""
                                    }
                                    name="powerfactor"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    margin="dense"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["current_sensor"].sensor_id
                                            : ""
                                    }
                                    name="current_sensor"
                                >
                                    {CurrentSensors}
                                </Select>
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["current_sensor_id"]
                                            : ""
                                    }
                                    name="current_sensor_id"
                                >
                                    {CurrentSensorRatings}
                                </Select>
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]["voltage_sensor"].sensor_id
                                    }
                                    name="voltage_sensor"
                                >
                                    {VoltageSensors}
                                </Select>
                            </div>
                        </div>
                    </TableCell>
                </TableRow>
            </div>
        );
    }

    public render() {

        const Timer = this.renderDetails;

        let ChannelDetails = [];

        times(this.state.channellist.length, i => {
            ChannelDetails.push(
                // @ts-ignore
                <Timer name={`${i}`} />
            );
            return ChannelDetails;
        });

        // @ts-ignore
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">
                                    View {this.state.channels} channel probe {this.state.name}
                                </span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Probe Name</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.name}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            required
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Number Of Channels</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.channels}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            required
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Device Type</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.device_type}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            required
                                                            style={{ width: '300px' }}
                                                        >
                                                            <MenuItem value={this.state.device_type}>
                                                                <em>{this.state.device_type}</em>
                                                            </MenuItem>
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>WIFI Network Name</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.wifi_network_name}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            required
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>WIFI Password</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            type="password"
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.wifi_password}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            required
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <div className="probeoptions probemain">
                                                        <div className="probeoptions" style={{ marginLeft: '1vw' }}>CHANNELS</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Channel Threshold</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Power Factor</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Current Sensor</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Current Sensor Rating</div>
                                                        <div className="probeoptions" style={{ marginLeft: '1vw' }}>Voltage Sensor</div>
                                                    </div>
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        {ChannelDetails}
                                    </TableBody>
                                </Table>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default ViewProbe;
