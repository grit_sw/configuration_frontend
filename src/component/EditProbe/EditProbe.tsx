import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import {
    CurrentSensor,
    DeviceType,
    EditProbes,
    ViewProbes,
    VoltageSensor
} from "../../Services/ProbeRequests/ProbeRequests";
import { ListSensor } from "../../Services/SensorRequests/SensorRequests";
import "./EditProbe.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    viewprobes: {};
    numberofchannels: number;
    devicetype: any[];
    wifi_network_name: string;
    wifi_password: string;
    communicationdevice: string;
    device_type: string;
    channels: any[];
    channelthreshold: any[];
    powerfactor: number;
    defaultvoltage: number;
    sensor_type: string;
    scalingfactora: number;
    scalingfactorv: number;
    viewsensor: any[];
    probe_id: string;
    probe_name: string;
    channellist: any[];
    channelsss: any[];
    disabled: boolean;
    devicetypeid: string;
    devicetypename: string;
    currentsensorid: any[];
    current_sensor_id: string;
    voltagesensorid: any[];
    voltage_sensor: string;
    current_rating_sensor_id: string;
    selrating: any[];
    open1: boolean;
    channel_id: number;
    channellistnumber: number
}

class EditProbe extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            channel_id: 0,
            channellist: [],
            channellistnumber: 0,
            channels: [],
            channelsss: [],
            channelthreshold: [],
            communicationdevice: "",
            configs: true,
            current_rating_sensor_id: "",
            current_sensor_id: "",
            currentsensorid: [],
            defaultvoltage: 0,
            device_type: "",
            devicetype: [],
            devicetypeid: "",
            devicetypename: "",
            disabled: true,
            groupadmin: "",
            name: [],
            numberofchannels: 0,
            open: true,
            open1: false,
            powerfactor: 0,
            probe_id: "",
            probe_name: "",
            scalingfactora: 0,
            scalingfactorv: 0,
            selrating: [],
            sensor_type: "",
            sourceconfig: "",
            // profitmargin: '',
            value: "",
            viewprobes: {},
            viewsensor: [],
            voltage_sensor: "",
            voltagesensorid: [],
            wifi_network_name: "",
            wifi_password: ""
        };
    }

    public componentWillMount() {

        // @ts-ignore works
        const fuelId = this.props.match.params.id;

        // tslint:disable
        console.log(fuelId);

        ViewProbes(fuelId)
            .then(res => {
                this.setState({
                    viewprobes: res.data.data,
                    probe_name: res.data.data.probe_name,
                    probe_id: res.data.data.probe_id,
                    channellist: res.data.data.channels,
                    channellistnumber: res.data.data.channels.length,
                    wifi_network_name: res.data.data.wifi_network_name,
                    wifi_password: res.data.data.wifi_password,
                });
                // tslint:disable
                console.log(res.data.data);
                // console.log(this.state.listfuels.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        DeviceType()
            .then(res => {
                this.setState({
                    devicetype: res.data.data,
                    devicetypeid: res.data.data.device_type_id,
                    devicetypename: res.data.data.name
                });
                // tslint:disable
                console.log(this.state.devicetype);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListSensor()
            .then(res => {
                this.setState({ viewsensor: res.data.data });
                // tslint:disable
                console.log(this.state.viewsensor);
            })
            .catch(err => {
                // tslint:disable
                console.log(err.response);
            });

        VoltageSensor()
            .then(res => {
                this.setState({
                    voltagesensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
                console.log(this.state.voltagesensorid);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        CurrentSensor()
            .then(res => {
                this.setState({
                    currentsensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
                console.log(this.state.currentsensorid);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public componentWillUpdate() {
        this.state.probe_name
    }

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            // if (
            //     // this.state.probe_name &&
            //     this.state.wifi_network_name &&
            //     this.state.wifipassword
            //     // this.state.probe_id &&
            //     // this.state.numberofchannels &&
            //     // this.state.voltage_sensor_id
            //     // this.state.current_rating_sensor_id
            // ) {
            //     this.setState({ disabled: false });
            // }
        });
        console.log(this.state);
    };

    public handleTimeChange = (countNumber, where, time, funcName = () => { }) => {
        let timeArr = [...this.state.channellist];
        timeArr[countNumber][where] = time;

        this.setState({ channellist: timeArr }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public handleVoltageSensor = (countNumber, where, where2, time, funcName = () => { }) => {
        let timeArr = [...this.state.channellist];

        timeArr[countNumber][where2] = time

        const selprobe = this.state.voltagesensorid.find(
            probe => probe.sensor_id === time
        );
        timeArr[countNumber][where] = selprobe;

        console.log(selprobe)

        this.setState({ channellist: timeArr }, () => {
            if (funcName) {
                funcName();
            }
            console.log(this.state.channellist)
        });
    };

    public handleCurrentSensor = (countNumber, where, time, funcName = () => { }) => {
        let timeArr = [...this.state.channellist];

        // timeArr[countNumber][where2] = time

        const selprobe = this.state.currentsensorid.find(
            probe => probe.sensor_id === time
        );
        timeArr[countNumber][where] = selprobe;

        console.log(selprobe)

        this.setState({ channellist: timeArr }, () => {
            if (funcName) {
                funcName();
            }
            console.log(this.state.channellist)
        });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    public sensorSelection = () => {
        let sensorId = this.state.current_sensor_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        console.log(sensorId);
        const selRating = this.state.currentsensorid.find(
            sensor => sensor.sensor_id === sensorId
        );
        console.log(selRating.ratings);
        this.setState({ selrating: selRating.ratings });
        console.log(this.state.selrating);
    };

    public renderDetails = props => {

        const CurrentSensors = this.state.currentsensorid.map(
            (CurrentSensor, id) => (
                <MenuItem key={id} value={CurrentSensor.sensor_id}>
                    {CurrentSensor.name}
                </MenuItem>
            )
        );

        const CurrentSensorRatings = !this.state.selrating[props.name] ? ""
            : this.state.selrating[props.name].map(
                (CurrentSensorRating, id) => (
                    <MenuItem key={id} value={CurrentSensorRating.rating_id}>
                        {CurrentSensorRating.rating}
                    </MenuItem>
                )
            )

        const VoltageSensors = this.state.voltagesensorid.map(
            (VoltageSensor, id) => (
                <MenuItem key={id} value={VoltageSensor.sensor_id}>
                    {VoltageSensor.name}
                </MenuItem>
            )
        );

        const sensorSelection = () => {
            let sensorId = this.state.channellist[props.name]['current_sensor'].sensor_id;
            // let probeId = "cbhdbhf-dmjn-dmf";
            console.log(sensorId);
            const selRating = this.state.currentsensorid.find(
                sensor => sensor.sensor_id === sensorId
            );
            console.log(selRating.ratings);

            let sensorArr = [...this.state.selrating];
            sensorArr[props.name] = selRating.ratings;

            this.setState({ selrating: sensorArr }, () => {
                console.log(this.state.selrating);
            });
        };

        return (
            <div>
                <TableRow className="tablerow">
                    <TableCell>
                        <div className="viewlistprobeoptions">
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["channel_name"]
                                            : this.state.channellist[props.name]["channel_name"]
                                    }
                                    onChange={e =>
                                        this.handleTimeChange(props.name, "channel_name", e.target.value)
                                    }
                                    inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                    }}
                                    name="channels"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    style={{ width: 100 }}
                                    font-size="true"
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["channel_threshold_current"]
                                            : ""
                                    }
                                    onChange={e =>
                                        this.handleTimeChange(props.name, "channel_threshold_current", e.target.value)
                                    }
                                    name="channelthreshold"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["power_factor"]
                                            : ""
                                    }
                                    onChange={e =>
                                        this.handleTimeChange(props.name, "power_factor", e.target.value)
                                    }
                                    name="powerfactor"
                                />
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    margin="dense"
                                    variant="outlined"
                                    font-size="true"
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["current_sensor"].sensor_id
                                            : ""
                                    }
                                    onChange={e =>
                                        this.handleCurrentSensor(props.name, "current_sensor", e.target.value, sensorSelection)
                                    }
                                    name="current_sensor"
                                >
                                    {CurrentSensors}
                                </Select>
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["current_sensor_id"]
                                            : ""
                                    }
                                    onChange={e =>
                                        this.handleTimeChange(props.name, "current_sensor_id", e.target.value)
                                    }
                                    name="current_sensor_id"
                                >
                                    {CurrentSensorRatings}
                                </Select>
                            </div>
                            <div className="viewlistprobeoptions">
                                <Select
                                    style={{ width: 100 }}
                                    value={
                                        this.state.channellist[props.name]
                                            ? this.state.channellist[props.name]["voltage_sensor"].sensor_id
                                            : ""
                                    }
                                    onChange={e =>
                                        this.handleVoltageSensor(props.name, "voltage_sensor", "voltage_sensor_id", e.target.value)
                                    }
                                    name="voltage_sensor"
                                >
                                    {VoltageSensors}
                                </Select>
                            </div>
                        </div>
                    </TableCell>
                </TableRow>
            </div>
        );
    }

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // @ts-ignore
            gen_id: this.props.match.params.id,

            // tslint:disable
            probe_id: this.state.probe_id,
            probe_name: this.state.probe_name,
            wifi_network_name: this.state.wifi_network_name,
            wifi_password: this.state.wifi_password,
            channels: this.state.channellist
        };

        console.log(body);
        console.log("here");
        // @ts-ignore
        EditProbes(body, body.gen_id)
            .then(res => {
                console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Probe not successfully edited. Please try again!");
            });
    };

    public render() {
        console.log(this.state)
        const Timer = this.renderDetails;

        let ChannelDetails = [];

        times(this.state.channellist.length, i => {
            ChannelDetails.push(
                // @ts-ignore
                <Timer name={`${i}`} />
            );
            return ChannelDetails;
        });

        // @ts-ignore
        const ViewFuels = this.state.viewprobes.probe_name;
        console.log(this.state.numberofchannels);

        // B8:27:EB:7F:A8:C8


        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">
                                    View {this.state.channellist.length} channel probe {this.state.probe_name}
                                </span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Probe Name</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.probe_name}
                                                            onChange={this.handleChange('probe_name')}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            name="probe_name"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Number Of Channels</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.channellistnumber}
                                                            onChange={this.handleChange("channellistnumber")}
                                                            name="channellistnumber"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>WIFI Network Name</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.wifi_network_name}
                                                            onChange={this.handleChange("wifinetworkname")}
                                                            name="wifinetworkname"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>WIFI Password</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            type="password"
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.wifi_password}
                                                            onChange={this.handleChange("wifipassword")}
                                                            inputProps={{
                                                                readOnly: Boolean("readOnly"),
                                                                disabled: Boolean("readOnly")
                                                            }}
                                                            name="wifipassword"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <div className="probeoptions probemain">
                                                        <div className="probeoptions" style={{ marginLeft: '1vw' }}>CHANNELS</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Channel Threshold</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Power Factor</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Current Sensor</div>
                                                        <div className="probeoptions" style={{ marginLeft: '3vw' }}>Current Sensor Rating</div>
                                                        <div className="probeoptions" style={{ marginLeft: '1vw' }}>Voltage Sensor</div>
                                                    </div>
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        {ChannelDetails}
                                    </TableBody>
                                </Table>

                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    // disabled={this.state.disabled}
                                >
                                    Save Probe
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Probe successfully edited
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/probe/list-probe">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div >
        );
    }
}

export default EditProbe;
