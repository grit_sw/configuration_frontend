import * as React from "react";
import { Link } from "react-router-dom";
import "./PowerConsumersSettings.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class PowerSourcesSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className="lists">
          <li className="lists">
            <Link to="/consumer/list-consumer" style={style}>
              List Consumers
            </Link>
          </li>

          <li className="lists">
            <Link to="/consumer/create-consumer-group" style={style}>
              Create Consumer Group
            </Link>
          </li>

          <li className="lists">
            <Link to="/consumer/create-consumer-line" style={style}>
              Create Consumer Line
            </Link>
          </li>

          <li className="lists">
            <Link to="/consumer/create-consumer" style={style}>
              Create Consumer
            </Link>
          </li>

          <li className="lists">
            <Link to="/consumer/create-consumer-group-type" style={style}>
              Create Consumer Group Type
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default PowerSourcesSettings;
