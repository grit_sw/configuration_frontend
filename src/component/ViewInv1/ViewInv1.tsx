import { Typography } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewOnePhaseInv } from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./ViewInv1.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  listconfig: any[];
  configuration_id: string;
  selpro: any[];
  probe: string;
  selcha: any[];
  names: string;
  signal_threshold: number;
  channel1: string;
  channel2: string;
  apparent_power_rating: number;
  efficiency: number;
  bulk_charging_current: number;
  cost: number;
  run_time: number;
  battery_capacity: number;
  battery_capacity1: number;
  battery_capacity2: number;
  battery_capacity3: number;
  battery_capacity4: number;
  battery_count: number;
  battery_voltage: number;
  battery_bank_voltage: number;
  depth_of_discharge: number;
  battery_chemistry: string;
  battery_end_of_life: number;
  dod1: number;
  dod2: number;
  dod3: number;
  dod4: number;
  number_of_cycles1: number;
  number_of_cycles2: number;
  number_of_cycles3: number;
  number_of_cycles4: number;
  start_timeOW: string;
  stop_timeOW: string;
  viewonephaseinv: {};
}

class ViewInv1 extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      apparent_power_rating: 0,
      battery_bank_voltage: 0,
      battery_capacity: 0,
      battery_capacity1: 0,
      battery_capacity2: 0,
      battery_capacity3: 0,
      battery_capacity4: 0,
      battery_chemistry: "",
      battery_count: 0,
      battery_end_of_life: 0,
      battery_voltage: 0,
      bulk_charging_current: 0,
      channel1: "",
      channel2: "",
      configs: true,
      configuration_id: "",
      cost: 0,
      depth_of_discharge: 0,
      dod1: 0,
      dod2: 0,
      dod3: 0,
      dod4: 0,
      efficiency: 0,
      groupadmin: "",
      listconfig: [],
      name: [],
      names: "",
      number_of_cycles1: 0,
      number_of_cycles2: 0,
      number_of_cycles3: 0,
      number_of_cycles4: 0,
      open: true,
      probe: "",
      run_time: 0,
      selcha: [],
      selpro: [],
      signal_threshold: 0,
      sourceconfig: "",
      start_timeOW: "",
      stop_timeOW: "",
      // profitmargin: '',
      value: "",
      viewonephaseinv: {}
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const sensortypeId = this.props.match.params.id;

    // tslint:disable
    console.log(sensortypeId);

    ViewOnePhaseInv(sensortypeId)
      .then(res => {
        this.setState({
          viewonephaseinv: res.data.data,
          names: res.data.data.name,
          signal_threshold: res.data.data.signal_threshold,
          config: res.data.data.configuration,
          probe: res.data.data.probe_names[0],
          channel1: res.data.data.output_channel[0].channel_name,
          channel2: res.data.data.input_channel[0].channel_name,
          apparent_power_rating: res.data.data.apparent_power_rating,
          efficiency: res.data.data.efficiency,
          bulk_charging_current: res.data.data.bulk_charging_current,
          start_timeOW: res.data.data.operating_window[0].start_time,
          stop_timeOW: res.data.data.operating_window[0].stop_time,
          cost: res.data.data.cost,
          battery_capacity: 0,
          // batter
          battery_bank_voltage: res.data.data.battery_bank[0].voltage,
          depth_of_discharge: res.data.data.battery_bank[0].depth_of_discharge,
          battery_chemistry: res.data.data.battery_bank[0].chemistry,
          battery_end_of_life: res.data.data.battery_bank[0].end_of_life,
          dod1: res.data.data.battery_bank[0].batteries[0].depth_of_discharge,
          number_of_cycles1:
            res.data.data.battery_bank[0].batteries[0].number_of_cycles,
          battery_capacity1:
            res.data.data.battery_bank[0].batteries[0].battery_capacity,
        });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create New Power Source</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Table className="table" style={{ margin: 10, width: 680 }}>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Name</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.names}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Signal Threshold</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          type="number"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.signal_threshold}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Type</TableCell>
                      <TableCell>Single Phase Inverter</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <div>&nbsp;</div>
                <Divider />
                <div>&nbsp;</div>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Channel Assignment</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Config</Typography>
                        <Select
                          value={this.state.config}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.config}>
                            <em>{this.state.config}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Probe(s)</Typography>
                        <Select
                          value={this.state.probe}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.probe}>
                            <em>{this.state.probe}</em>
                          </MenuItem>
                        </Select>
                        <Typography>Channel</Typography>
                        <div>&nbsp; &nbsp;</div>
                        <ul className="sourcetable">
                          <li className="sourcetable">Output</li>
                          <li className="sourcetable">
                            <Select
                              value={this.state.channel1}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            >
                              <MenuItem value={this.state.channel1}>
                                <em>{this.state.channel1}</em>
                              </MenuItem>
                            </Select>
                          </li>
                        </ul>
                        <ul className="sourcetable">
                          <li className="sourcetable">Input</li>
                          &nbsp; &nbsp;
                          <li className="sourcetable">
                            <Select
                              value={this.state.channel2}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            >
                              <MenuItem value={this.state.channel2}>
                                <em>{this.state.channel2}</em>
                              </MenuItem>
                            </Select>
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Capacity</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Rating [kVa]</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.apparent_power_rating}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Efficiency</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.efficiency}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Bulk Charging Current</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.bulk_charging_current}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Cost</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.cost}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Operating Window</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">From:</li>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">To:</li>
                          </ul>
                        </Typography>

                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Start Time"
                              type="time"
                              value={this.state.start_timeOW}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Stop Time"
                              type="time"
                              value={this.state.stop_timeOW}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Expected Run Time</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Run Time [Hours]</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.run_time}
                              name="run_time"
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Storage</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery Capacity</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.battery_capacity}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery Count</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.battery_count}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery Voltage [V]</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.battery_voltage}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery Bank Voltage [V]</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.battery_bank_voltage}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Depth of Discharge</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.depth_of_discharge}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery Chemistry</Typography>
                          </li>
                          <li className="sourcetable">
                            <Select
                              value={this.state.battery_chemistry}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            >
                              <MenuItem value={this.state.battery_chemistry}>
                                <em>{this.state.battery_chemistry}</em>
                              </MenuItem>
                            </Select>
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Battery End of Life</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.battery_end_of_life}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Table>
                          <TableHead>
                            <TableRow>
                              <TableCell>Battery Life</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow className="tablerow">
                              <TableCell>
                                <Typography>
                                  <ul className="viewlist">
                                    <li className="viewlist">DoD (%)</li>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp;
                                    <li className="viewlist">
                                      number Of Cycles
                                    </li>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <li className="viewlist">
                                      Battery Capacity (%)
                                    </li>
                                  </ul>
                                </Typography>

                                <ul className="viewlist">
                                  <li className="viewlist">
                                    <TextField
                                      className="textField"
                                      type="number"
                                      margin="normal"
                                      variant="outlined"
                                      font-size="true"
                                      value={this.state.dod1}
                                      inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                      }}
                                      required
                                    />
                                  </li>
                                  <li className="viewlist">
                                    <TextField
                                      className="textField"
                                      type="number"
                                      margin="normal"
                                      variant="outlined"
                                      font-size="true"
                                      value={this.state.number_of_cycles1}
                                      inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                      }}
                                      required
                                    />
                                  </li>
                                  <li className="viewlist">
                                    <TextField
                                      className="textField"
                                      type="number"
                                      margin="normal"
                                      variant="outlined"
                                      font-size="true"
                                      value={this.state.battery_capacity1}
                                      inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                      }}
                                      required
                                    />
                                  </li>
                                </ul>                                
                              </TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewInv1;
