import * as React from "react";
import { Link } from "react-router-dom";
import "./FuelSettings.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class FuelSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className="lists">
          <li className="lists">
            <Link to="/fuel/list-fuel" style={style}>
              List Fuel
            </Link>
          </li>

          <li className="lists">
            <Link to="/fuel/create-fuel" style={style}>
              Create Fuel
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default FuelSettings;
