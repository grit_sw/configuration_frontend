import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
// import { UserDetails } from "src/Services/UsersRequests/UsersRequests";
import { ListConfig, ListConfigurations } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./ListConfiguration.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Istate {
    listconfig: any[];
    listconfiguration: any[];
    error: string;
    listusers: any[];
    user: string;
    display: boolean;
    display1: boolean;
    admin: boolean;
    userid: string;
    roleid: number
}

interface Iprops {
    display1;
    user;
    username;
    userconfiguration;
    user_role;
    user_id
}

class ListConfiguration extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);

        this.state = {
            admin: false,
            display: false,
            display1: false,
            error: "",
            listconfig: [],
            listconfiguration: [],
            listusers: [],
            roleid: 0,
            user: "",
            userid: "",
        };
    }

    public componentWillMount() {
        ListConfig()
            .then(res => {
                this.setState({ listconfig: res.data.data });
                // tslint:disable
                console.log(this.state.listconfig);
                console.log(this.state.listconfig.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // @ts-ignore
        const adminid = this.props.match.params.id;
        // const userId = this.props.user;
        // const userName = this.props.username;

        // // tslint:disable
        // console.log(this.state.admin);
        // console.log(userName);

        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }

        console.log(this.props.user_id)
        let user = this.props.user_id

        ListConfigurations(user)
            .then(res => {
                this.setState({ listconfiguration: res.data.data, display1: true })
                // tslint:disable
                console.log(this.state.listconfiguration);
                console.log(this.state.listconfiguration.length);
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display1: false });
                console.log(this.state.error);
            });
    }

    public CreateColumn(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/configuration/view-configuration/" + id} style={style}>
                        View
          </Link>
                </li>
                <li className="ullist">
                    <Link to={"/configuration/edit-configuration/" + id} style={style}>
                        Edit
          </Link>
                </li>
            </ul>
        );
    }

    public render() {
        const Config = this.state.listconfig.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.configuration_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn(ListConfig.configuration_id)}</TableCell>
            </TableRow>
        ));
        const FuelTypelength = this.state.listconfig.length;
        console.log(FuelTypelength);
        console.log(this.props.user);

        const UserConfig = !this.props.userconfiguration ? <div />
            : this.props.userconfiguration.map((ListConfig, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell>{ListConfig.configuration_name}</TableCell>
                    <TableCell />
                    <TableCell />
                    <TableCell>{this.CreateColumn(ListConfig.configuration_id)}</TableCell>
                </TableRow>
            ));

        const NonAdminUserConfiguration = this.state.listconfiguration.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.configuration_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn(ListConfig.configuration_id)}</TableCell>
            </TableRow>
        ));

        console.log(this.props.userconfiguration)

        // @ts-ignore
        const userId = this.props.user;
        const userName = this.props.username;

        const userconfigurations = this.props.userconfiguration;

        const userconfigurationlength = userconfigurations.length;

        return (
            <div>
                {(!this.props.display1 && userconfigurationlength === 0) || (this.props.display1 && userconfigurationlength === 0) ? (
                    <div>
                        {this.props.username ? (
                            <div>{userName} Has No Configuration</div>
                        ) : (
                                <div>Select a user to view their configurations</div>
                            )}
                    </div>
                ) : (
                        <div>
                            <Table className="table" style={{ overflowX: "auto" }}>
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">
                                                Configurations associated with {userName}
                                            </span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow" style={{ fontWeight: "bold" }}>
                                        <TableCell>Configuration</TableCell>
                                        <TableCell>Owner</TableCell>
                                        <TableCell>Belongs To Group</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                    {UserConfig}
                                </TableBody>
                            </Table>
                        </div>
                    )}

                <div>&nbsp;</div>

                {this.state.admin ? (
                    <div>
                        <Table className="table" style={{ overflowX: "auto" }}>
                            <TableHead className="head">
                                <TableRow className="tablecell">
                                    <TableCell className="tablecell">
                                        <span id="tablecell">All Configurations</span>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className="tablerow" style={{ fontWeight: "bold" }}>
                                    <TableCell>Configuration</TableCell>
                                    <TableCell>Owner</TableCell>
                                    <TableCell>Belongs To Group</TableCell>
                                    <TableCell>Actions</TableCell>
                                </TableRow>
                                {Config}
                            </TableBody>
                        </Table>
                    </div>
                ) : (
                        <div>
                            <Table className="table" style={{ overflowX: "auto" }}>
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">All Configurations</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow" style={{ fontWeight: "bold" }}>
                                        <TableCell>Configuration</TableCell>
                                        <TableCell>Owner</TableCell>
                                        <TableCell>Belongs To Group</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                    {NonAdminUserConfiguration}
                                </TableBody>
                            </Table>
                        </div>
                    )}
            </div>
        );
    }
}

export default ListConfiguration;
