import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import DualListBox from "react-dual-listbox";
import "react-dual-listbox/lib/react-dual-listbox.css";
import { Link } from "react-router-dom";
import { CreateConfigurationGroups, GroupAdmin, ListConfig, ListConfigurations } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./CreateConfigurationGroup.css";

interface Istate {
    config?: "";
    groupadmin: any[];
    groupadminvalue: string;
    open: boolean;
    profitmargin?: number;
    groupname?;
    sourceconfig: any[];
    sourceconfigvalue: string;
    value: "";
    name: [];
    configs: boolean;
    colSpan?: number;
    selected: any[];
    listconfig: any[];
    disabled: boolean;
    open1: boolean
}
class CreateConfigurationGroup extends React.Component<
    { colSpan: true },
    Istate
    > {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            colSpan: 2,
            configs: true,
            disabled: true,
            groupadmin: [],
            groupadminvalue: "",
            listconfig: [],
            name: [],
            open: true,
            open1: false,
            selected: [],
            sourceconfig: [],
            sourceconfigvalue: "",
            value: ""
        };
    }

    public componentWillMount() {
        GroupAdmin()
            .then(res => {
                this.setState({ groupadmin: res.data.data });
                // tslint:disable
                // console.log(this.state.groupadmin);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListConfig()
            .then(res => {
                this.setState({ listconfig: res.data.data });
                // tslint:disable
                // console.log(this.state.listconfig);
                // console.log(this.state.listconfig.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public SourceConfiguration = () => {
        const groupAdminId = this.state.groupadminvalue;
        ListConfigurations(groupAdminId)
            .then(res => {
                this.setState({ sourceconfig: res.data.data });
                // console.log(res.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    };

    public onChange = selected => {
        // tslint:disable
        this.setState({
            selected: selected
        });

        if (
            this.state.groupname &&
            this.state.groupadminvalue &&
            this.state.sourceconfigvalue &&
            this.state.profitmargin &&
            this.state.selected
        ) {
            this.setState({ disabled: false });
        }
    };
    // @ts-ignore works

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.groupname &&
                this.state.groupadminvalue &&
                this.state.sourceconfigvalue &&
                this.state.profitmargin &&
                this.state.selected
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
    };

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.groupname,
            admin_id: this.state.groupadminvalue,
            source_config_id: this.state.sourceconfigvalue,
            profit_margin: this.state.profitmargin,
            group_members: this.state.selected
        };

        // @ts-ignore
        CreateConfigurationGroups(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Configuration not created. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public render() {
        const { selected } = this.state;
        const values = this.state.listconfig.map(value => {
            return {
                value: value.configuration_id,
                label: value.configuration_name
            };
        });

        const Groupadmins = this.state.groupadmin.map((GroupAdmin, id) => (
            <MenuItem key={id} value={GroupAdmin.user_id}>
                {GroupAdmin.first_name} {GroupAdmin.last_name}
            </MenuItem>
        ));

        const Config = this.state.sourceconfig.map((ListConfig, id) => (
            <MenuItem key={id} value={ListConfig.configuration_id}>
                {ListConfig.configuration_name}
            </MenuItem>
        ));
        // const FuelTypelength = this.state.listconfig.length;
        // console.log(FuelTypelength);

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New configuration</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell style={{ fontWeight: "bold" }}>Group Name</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.groupname}
                                    onChange={this.handleChange("groupname")}
                                    name="groupname"
                                />
                            </TableCell>
                            <TableCell rowSpan={4} colSpan={2} />
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell style={{ fontWeight: "bold" }}>Group Admin</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.groupadminvalue}
                                    onChange={this.handleChange(
                                        "groupadminvalue",
                                        this.SourceConfiguration
                                    )}
                                    name="groupadminvalue"
                                >
                                    {Groupadmins}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell style={{ fontWeight: "bold" }}>
                                Source Config
                            </TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.sourceconfigvalue}
                                    onChange={this.handleChange("sourceconfigvalue")}
                                    name="sourceconfig"
                                >
                                    {Config}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell style={{ fontWeight: "bold" }}>
                                Profit Margin
                            </TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    label="number"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.profitmargin}
                                    onChange={this.handleChange("profitmargin")}
                                    name="profitmargin"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell />
                            <TableCell>All Configs</TableCell>
                            <TableCell>Group Members</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell colSpan={3}>
                                <DualListBox
                                    name="moons"
                                    options={values}
                                    selected={selected}
                                    // tslint:disable

                                    onChange={this.onChange}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3} style={{ textAlign: "center" }}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("config")}
                                    disabled={this.state.disabled}
                                >
                                    Create Config Group
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Configuration Group successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/configuration/list-configuration">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                &nbsp;
      </div>
        );
    }
}

export default CreateConfigurationGroup;
