import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewThreePhaseGen } from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./ViewGen3.css";

interface Istate {
  config?: "";
  configuration_id: string;
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  signal_threshold: string;
  sourceconfig?: "";
  value: "";
  name: [];
  names: string;
  configs: boolean;
  listconfig: any[];
  listfuels: any[];
  selpro: any[];
  selcha: any[];
  fuel: string;
  fuel_store_size: string;
  apparent_power_rating: string;
  start_timeOW: string;
  stop_timeOW: string;
  start_timeCM: string;
  stop_timeCM: string;
  viewonephasegen: {};
  probe: string;
  channel1: string;
  channel2: string;
  channel3: string;
}

class ViewGen3 extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      apparent_power_rating: "",
      channel1: "",
      channel2: "",
      channel3: "",
      configs: true,
      configuration_id: "",
      fuel: "",
      fuel_store_size: "",
      groupadmin: "",
      listconfig: [],
      listfuels: [],
      name: [],
      names: "",
      open: true,
      probe: "",
      selcha: [],
      selpro: [],
      signal_threshold: "",
      sourceconfig: "",
      start_timeCM: "",
      start_timeOW: "",
      stop_timeCM: "",
      stop_timeOW: "",
      // profitmargin: '',
      value: "",
      viewonephasegen: ""
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const sensortypeId = this.props.match.params.id;

    // tslint:disable
    console.log(sensortypeId);

    ViewThreePhaseGen(sensortypeId)
      .then(res => {
        this.setState({
          viewonephasegen: res.data.data,
          names: res.data.data.name,
          signal_threshold: res.data.data.signal_threshold,
          config: res.data.data.configuration.configuration_name,
          probe: res.data.data.probe_names[0],
          channel1: res.data.data.channel[0].channel_name,
          channel2: res.data.data.channel[1].channel_name,
          channel3: res.data.data.channel[2].channel_name,
          apparent_power_rating: res.data.data.apparent_power_rating,
          fuel: res.data.data.fuel.name,
          fuel_store_size: res.data.data.fuel_store_size,
          start_timeOW: res.data.data.operating_window[0].start_time,
          stop_timeOW: res.data.data.operating_window[0].stop_time,
          start_timeCM: res.data.data.charge_map[0].start_time,
          stop_timeCM: res.data.data.charge_map[0].stop_time
        });
        // tslint:disable
        console.log(res.data.data);
        console.log(res.data.data.charge_map[0].start_time);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    console.log(this.state.start_timeOW);

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create New Power Source</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Table className="table">
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Name</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.names}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Signal Threshold</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          type="number"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.signal_threshold}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Type</TableCell>
                      <TableCell>Three Phase Generator</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <div>&nbsp;</div>
                <Divider />
                <div>&nbsp;</div>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Channel Assignment</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Config</Typography>
                        <Select
                          value={this.state.config}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.config}>
                            <em>{this.state.config}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Probe(s)</Typography>
                        <Select
                          value={this.state.probe}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.probe}>
                            <em>{this.state.probe}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Phase 1 Channel</Typography>
                        <Select
                          value={this.state.channel1}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.channel1}>
                            <em>{this.state.channel1}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Phase 2 Channel</Typography>
                        <Select
                          value={this.state.channel2}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.channel2}>
                            <em>{this.state.channel2}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Phase 3 Channel</Typography>
                        <Select
                          value={this.state.channel3}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.channel3}>
                            <em>{this.state.channel3}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table className="table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Capacity</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Apparent Power [kVa]</Typography>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.apparent_power_rating}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Fuel Type</Typography>
                          </li>
                          <li className="sourcetable">
                            <Select
                              value={this.state.fuel}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            >
                              <MenuItem value={this.state.fuel}>
                                <em>{this.state.fuel}</em>
                              </MenuItem>
                            </Select>
                          </li>
                        </ul>
                        <ul className="sourcetable">
                          <li className="sourcetable">
                            <Typography>Fuel Store</Typography>
                          </li>
                          <li className="sourcetable">
                            <TextField
                              className="textField"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.fuel_store_size}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                            <Button
                              variant="contained"
                              style={{ backgroundColor: "#337ab7" }}
                            >
                              Refill Tank
                            </Button>
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Operating Window</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">From:</li>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">To:</li>
                          </ul>
                        </Typography>

                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Start Time"
                              type="time"
                              value={this.state.start_timeOW}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Stop Time"
                              type="time"
                              value={this.state.stop_timeOW}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Charge Map</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">From:</li>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">To:</li>
                          </ul>
                        </Typography>

                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Start Time"
                              type="time"
                              value={this.state.start_timeCM}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              id="time"
                              label="Stop Time"
                              type="time"
                              value={this.state.stop_timeCM}
                              inputProps={{
                                readOnly: Boolean("readOnly"),
                                disabled: Boolean("readOnly")
                              }}
                              required
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Fuel Consumption Map</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">1/4 load</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">1/2 load</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">3/4 load</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">Full</li>
                          </ul>
                        </Typography>

                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewGen3;
