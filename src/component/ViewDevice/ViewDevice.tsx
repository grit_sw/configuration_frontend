import { Typography } from "@material-ui/core";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewDevicess } from "../../Services/DevicesRequests/DevicesRequests";
import "./ViewDevice.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  viewdevices: {};
  numberofchannels: string;
}

class ViewDevices extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      name: [],
      numberofchannels: "",
      open: true,
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewdevices: {}
    };
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public componentWillMount() {
    // @ts-ignore works
    const devicetypeId = this.props.match.params.id;

    // tslint:disable
    console.log(devicetypeId);

    ViewDevicess(devicetypeId)
      .then(res => {
        this.setState({
          viewdevices: res.data.data,
          name: res.data.data.name,
          numberofchannels: res.data.data.number_of_channels,
          profitmargin: res.data.data
        });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    // @ts-ignore works
    const ViewDevicesName = this.state.viewdevices.name;
    // @ts-ignore works
    const ViewChannelNumber = this.state.viewdevices.number_of_channels;

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Device</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={ViewDevicesName}
                  InputProps={{
                    readOnly: true
                  }}
                  onChange={this.handleChange("device_type")}
                  name="device_type"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>NUmber Of Channels</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={ViewChannelNumber}
                  InputProps={{
                    readOnly: true
                  }}
                  onChange={this.handleChange("numberofchannels")}
                  name="numberofchannels"
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewDevices;
