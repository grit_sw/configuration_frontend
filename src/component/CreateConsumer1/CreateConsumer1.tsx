import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { List3PhaseConsumerLine, ListOnePhaseConsumerLine } from "src/Services/ConsumerLineRequests/ConsumerLineRequests";
import { CreateConsumerOne, ListGroupTypes } from "../../Services/ConsumerGroupRequests/ConsumerGroupRequests";
import "./CreateConsumer1.css";


interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    listconfig: any[];
    viewgrouptypes: any[];
    groupname: string
    grouptype: any[]
    configuration: any[]
    disabled: boolean
    open1: boolean
    onephase: any[]
    threephase: any[]
}

class CreateConsumer1 extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            configuration: [],
            disabled: true,
            groupadmin: "",
            groupname: "",
            grouptype: [],
            listconfig: [],
            name: [],
            onephase: [],
            open: true,
            open1: false,
            sourceconfig: "",
            threephase: [],
            value: "",
            viewgrouptypes: []
        };
    }

    public componentWillMount() {
        // @ts-ignore works
        const fuelId = this.props.match.params.id;

        // tslint:disable
        // console.log(fuelId);

        ListOnePhaseConsumerLine()
            .then(res => {
                this.setState({ onephase: res.data.data });
                // tslint:disable
                console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        List3PhaseConsumerLine()
            .then(res => {
                this.setState({ threephase: res.data.data });
                // tslint:disable
                console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListGroupTypes()
            .then(res => {
                this.setState({ viewgrouptypes: res.data.data });
                // tslint:disable
                // console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (
                this.state.name &&
                this.state.groupname &&
                this.state.grouptype
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            line_id: this.state.configuration,
            name: this.state.groupname,
        };

        // @ts-ignore
        CreateConsumerOne(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Consumer not created. Please try again!");
            });
    };

    public render() {
        const Devicetypes = this.state.viewgrouptypes.map((DeviceType, id) => (
            <MenuItem key={id} value={DeviceType.consumer_group_type_id}>
                {DeviceType.name}
            </MenuItem>
        ));

        const channelist3 = this.state.threephase.map((ListFuel, id) => (
            <MenuItem key={id} value={ListFuel.line_id}>
                {ListFuel.name}
            </MenuItem>
        ));

        const Channelist1 = this.state.onephase.map((ListFuel, id) => (
            <MenuItem key={id} value={ListFuel.line_id}>
                {ListFuel.name}
            </MenuItem>
        ));

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">
                                    Create 1 phase Grid power source
                                </span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Line</Typography>
                                    </div>
                                    <div>
                                        <Select
                                            value={this.state.configuration}
                                            onChange={this.handleChange("configuration")}
                                            name="configuration"
                                            style={{ width: '300px' }}
                                        >
                                            {Channelist1}
                                            {channelist3}
                                        </Select>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Name</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.groupname}
                                            onChange={this.handleChange("groupname")}
                                            name="groupname"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Type</Typography>
                                    </div>
                                    <div>
                                        <Select
                                            value={this.state.grouptype}
                                            onChange={this.handleChange("grouptype")}
                                            name="grouptype"
                                            style={{ width: '300px' }}
                                        >
                                            {Devicetypes}
                                        </Select>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    disabled={this.state.disabled}
                                    onClick={this.handleSubmit}
                                >
                                    ADD CONSUMER
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Consumer successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/consumer/list-consumer">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateConsumer1;
