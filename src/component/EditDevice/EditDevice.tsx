import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import {
  EditDevices,
  ViewDevicess
} from "../../Services/DevicesRequests/DevicesRequests";
import "./EditDevice.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  number_of_channels: number;
  open: boolean;
  device_name: string;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  profitmargin?;
  viewdevices: {};
  disabled: boolean;
  open1: boolean;
}

class EditDevice extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      device_name: "",
      disabled: true,
      groupadmin: "",
      name: [],
      number_of_channels: 0,
      open: true,
      open1: false,
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewdevices: {}
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const devicetypeid = this.props.match.params.id;

    // tslint:disable
    console.log(devicetypeid);

    ViewDevicess(devicetypeid)
      .then(res => {
        this.setState({ viewdevices: res.data.data });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value }, () => {
      if (this.state.device_name && this.state.number_of_channels) {
        this.setState({ disabled: false });
      }
    });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  public handleClose1 = () => {
    this.setState({ open1: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public handleSubmit = event => {
    event.preventDefault();

    const body = {
      // tslint:disable
      name: this.state.device_name,
      // @ts-ignore works
      device_type_id: this.props.match.params.id,
      number_of_channels: this.state.number_of_channels
    };

    console.log(body);
    console.log("here");
    // @ts-ignore
    EditDevices(body, body.device_type_id)
      .then(res => {
        console.log(res);
        this.setState({ open1: true });
      })
      .catch(err => {
        console.log(err.response);
        alert("Device not successfully edited. Please try again!");
      });
  };

  public render() {
    // @ts-ignore works
    const ViewDevices = this.state.viewdevices.name;
    // @ts-ignore works
    const DevicesChannels = this.state.viewdevices.number_of_channels;

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Device</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={ViewDevices}
                  onChange={this.handleChange("device_name")}
                  name="device_name"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>NUmber Of Channels</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={DevicesChannels}
                  onChange={this.handleChange("number_of_channels")}
                  name="number_of_channels"
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography>Price</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.profitmargin}
                  onChange={this.handleChange("profitmargin")}
                  name="profitmargin"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Features</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                />
                &nbsp; &nbsp;
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                >
                  Save
                </Button>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={3}>
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                  onClick={this.handleSubmit}
                  disabled={this.state.disabled}
                >
                  Save
                </Button>
                <Dialog
                  open={this.state.open1}
                  onClose={this.handleClose1}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">
                    {"Alert!!!"}
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      Device successfully created
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Link to="/devices/list-devices">
                      <Button onClick={this.handleClose1} color="primary">
                        Close
                      </Button>
                    </Link>
                  </DialogActions>
                </Dialog>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default EditDevice;
