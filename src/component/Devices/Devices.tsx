import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import "react-dual-listbox/lib/react-dual-listbox.css";
import { Link } from "react-router-dom";
import { ListDevice } from "../../Services/DevicesRequests/DevicesRequests";
import "./Devices.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Istate {
    listdevice: any[];
    listdevicename: string;
    listdeviceid: string;
}
class Devices extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            listdevice: [],
            listdeviceid: "",
            listdevicename: ""
        };
    }

    public componentWillMount() {
        ListDevice()
            .then(res => {
                // tslint:disable
                // console.log(res.data.data);
                this.setState({
                    listdevice: res.data.data,
                    listdevicename: res.data.data.name,
                    listdeviceid: res.data.data.device_type_id
                });
            })
            .catch(err => {
                // tslint:disable
                console.log(err.response);
            });
    }

    public CreateColumn(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/devices/view/" + id} style={style}>
                        View
          </Link>
                </li>
                <li className="ullist">
                    <Link to={"/devices/edit/" + id} style={style}>
                        Edit
          </Link>
                </li>
            </ul>
        );
    }

    public render() {
        const ListDevices = this.state.listdevice.map((ListDevice, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListDevice.name}</TableCell>
                <TableCell>{this.CreateColumn(ListDevice.device_type_id)}</TableCell>
            </TableRow>
        ));

        // const ListDevice = this.state.listdevice;

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Devices</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>Device Name</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                        {ListDevices}
                        {/* <TableRow>
                            <TableCell>{this.state.listdevicename}</TableCell>
                            <TableCell>
                                {this.CreateColumn(this.state.listdeviceid)}
                            </TableCell>
                        </TableRow> */}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default Devices;
