import { Typography } from "@material-ui/core";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { OtherUserDetails } from "../../Services/UsersRequests/UsersRequests";
import "./EditUser.css";


interface Istate {
    name: string;
    username: string
    email: string
    phone: string
    location: string
    bio: string
    timewindow: string
    locale: string
    timezone: string
    verified: string
    roles: string
    permissions: string
}

class EditUser extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            bio: "",
            email: "",
            locale: "",
            location: "",
            name: "",
            permissions: "",
            phone: "",
            roles: "",
            timewindow: "",
            timezone: "",
            username: "",
            verified: ""

        };
    }

    public componentWillMount() {
        // @ts-ignore works

        const userid = this.props.match.params.id;

        OtherUserDetails(userid)
            .then(res => {
                this.setState({
                    email: res.data.data.email,
                    name: res.data.data.fullname,
                    roles: res.data.data.role_name,
                    username: res.data.data.first_name,
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(res.data.data);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            // if (this.state.rolename && this.state.permissions) {
            //     this.setState({ disabled: false });
            // }
        });
    };

    public render() {
        // tslint:disable

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">User</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Names</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            name="name"
                                            value={this.state.name}
                                            onChange={this.handleChange("name")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Username</Typography></div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            name="username"
                                            value={this.state.username}
                                            onChange={this.handleChange("username")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Email</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.email}
                                            name="email"
                                            onChange={this.handleChange("email")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Phone</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.phone}
                                            name="phone"
                                            onChange={this.handleChange("phone")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Location</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.location}
                                            name="location"
                                            onChange={this.handleChange("location")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Bio</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.bio}
                                            name="bio"
                                            onChange={this.handleChange("bio")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Timewindow</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.timewindow}
                                            name="timewindow"
                                            onChange={this.handleChange("timewindow")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Locale</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.locale}
                                            name="locale"
                                            onChange={this.handleChange("rolename")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Timezone</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.timezone}
                                            name="timezone"
                                            onChange={this.handleChange("timezone")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Verified</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.verified}
                                            name="verified"
                                            onChange={this.handleChange("verified")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Roles</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.roles}
                                            name="roles"
                                            onChange={this.handleChange("roles")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Permissions</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.permissions}
                                            name="permissions"
                                            onChange={this.handleChange("permissions")}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        {/* <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Update Firmware
                                </Button>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    disabled={this.state.disabled}
                                >
                                    Save Probe
                                </Button>
                            </TableCell>
                        </TableRow> */}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default EditUser;
