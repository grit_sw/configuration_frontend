import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import {
  EditSensor,
  ViewSensor
} from "../../Services/SensorRequests/SensorRequests";
import "./EditSensor.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  price: number;
  profitmargin?: number;
  rating: number;
  scaling_factor: number;
  sensor_name: string;
  size: number;
  sourceconfig?: "";
  value: "";
  name: [];
  names: string;
  configs: boolean;
  editsensors: {};
  viewsensors: {};
  disabled: boolean;
  open2: boolean;
}

class EditSensors extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      disabled: true,
      editsensors: {},
      groupadmin: "",
      name: [],
      names: "",
      open: true,
      open2: false,
      price: 0,
      rating: 0,
      scaling_factor: 0,
      sensor_name: "",
      size: 0,
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewsensors: {}
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const sensortypeId = this.props.match.params.id;

    // tslint:disable
    console.log(sensortypeId);

    ViewSensor(sensortypeId)
      .then(res => {
        this.setState({ editsensors: res.data.data });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value }, () => {
      if (this.state.sensor_name && this.state.rating) {
        this.setState({ disabled: false });
      }
    });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  public handleClose2 = () => {
    this.setState({ open2: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public handleSubmit = event => {
    event.preventDefault();

    const body = {
      // tslint:disable
      name: this.state.sensor_name,
      rating: this.state.rating,
      // @ts-ignore works
      sensortypeId: this.props.match.params.id

      // channels: []
    };

    console.log(body);
    console.log("here");
    // @ts-ignore
    EditSensor(body, body.sensortypeId)
      .then(res => {
        console.log(res);
        this.setState({ open2: true });
      })
      .catch(err => {
        console.log(err.response);
        alert("Sensor not successfully edited. Please try again!");
      });
  };

  public render() {
    // @ts-ignore works
    // @ts-ignore works

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Sensors</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.names}
                  onChange={this.handleChange("sensor_name")}
                  name="sensor_name"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Scaling Factor</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.scaling_factor}
                  onChange={this.handleChange("scaling_factor")}
                  name="scaling_factor"
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography>Max Current</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  // value={rating}
                  onChange={this.handleChange("rating")}
                  name="rating"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Size</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.size}
                  onChange={this.handleChange("size")}
                  name="size"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Price</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.price}
                  onChange={this.handleChange("price")}
                  name="price"
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={3}>
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                  onClick={this.handleSubmit}
                  disabled={this.state.disabled}
                >
                  Save
                </Button>
                <Dialog
                  open={this.state.open2}
                  onClose={this.handleClose2}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">
                    {"Alert!!!"}
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      Sensor successfully created
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Link to="/sensors/list-sensors">
                      <Button onClick={this.handleClose2} color="primary">
                        Close
                      </Button>
                    </Link>
                  </DialogActions>
                </Dialog>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default EditSensors;
