import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateProbes } from "../../Services/ProbeRequests/ProbeRequests";
import "./CreateDomain.css";

interface Istate {
    config?: "";
    device_type_id?: "";
    probe_name?: "";
    open: boolean;
    channelnumber?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    wifi_network_name?: "";
    password?: "";
    wifi_password?: "";
    disabled: boolean;
    open1: boolean;
}

class CreateDomain extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            disabled: true,
            name: [],
            open: true,
            open1: false,
            sourceconfig: "",
            value: ""
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (
                this.state.probe_name &&
                this.state.wifi_network_name &&
                this.state.wifi_password &&
                this.state.device_type_id &&
                this.state.channelnumber
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.open });
        // openState = this.state.open;
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            probe_name: this.state.probe_name,
            wifi_network_name: this.state.wifi_network_name,
            wifi_password: this.state.wifi_password,
            device_type_id: this.state.device_type_id
            // channels: []
        };
        const channels: any[] = [];
        times(this.state.channelnumber, i => {
            channels.push({
                channel_threshold_current: 0,
                power_factor: 0,
                default_voltage: 0,
                scaling_factor_voltage: 0,
                scaling_factor_current: 0,
                sensor_type_id: "string"
            });
        });

        if (channels !== []) {
            body["channels"] = channels;
        }

        // @ts-ignore
        CreateProbes(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err);
                alert("Domain not created. Please try again!");
            });
    };

    public render() {
        // tslint:disable
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create Domain</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Name</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    onChange={this.handleChange("probe_name")}
                                    name="probe_name"
                                    value={this.state.probe_name}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Number Of Channels</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.channelnumber}
                                    onChange={this.handleChange("channelnumber")}
                                    name="channelnumber"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <Typography>Device Type</Typography>
                                <Select
                                    value={this.state.device_type_id}
                                    onChange={this.handleChange("device_type_id")}
                                    name="device_type_id"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value="5">ok</MenuItem>
                                    <MenuItem>ten</MenuItem>
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>WIFI Network Name</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.wifi_network_name}
                                    onChange={this.handleChange("wifi_network_name")}
                                    name="wifi_network_name"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>WIFI Password</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.wifi_password}
                                    onChange={this.handleChange("wifi_password")}
                                    name="wifi_password"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Update Firmware
                                </Button>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Save Probe
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Domain successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/usergroup/list-domain">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateDomain;
