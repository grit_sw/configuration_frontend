import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import {
    CreateProbes,
    CurrentSensor,
    DeviceType,
    VoltageSensor
} from "../../Services/ProbeRequests/ProbeRequests";
import "./CreateProbe.css";

interface Istate {
    config?: "";
    devicetype: any[];
    device_type_id?: "";
    probe_name?: "";
    open: boolean;
    channelnumber?: number;
    configuration: string;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    wifi_network_name?: "";
    password?: "";
    wifi_password?: "";
    disabled: boolean;
    devicetypeid: string;
    devicetypename: string;
    currentsensorid: any[];
    current_sensor_id: string;
    voltagesensorid: any[];
    voltage_sensor_id: string;
    current_rating_sensor_id: string;
    selrating: any[];
    open1: boolean;
}

class CreateProbe extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            configuration: "",
            current_rating_sensor_id: "",
            current_sensor_id: "",
            currentsensorid: [],
            devicetype: [1],
            devicetypeid: "",
            devicetypename: "",
            disabled: true,
            name: [],
            open: true,
            open1: false,
            selrating: [],
            sourceconfig: "",
            value: "",
            voltage_sensor_id: "",
            voltagesensorid: []
        };
    }

    public componentWillMount() {
        DeviceType()
            .then(res => {
                this.setState({
                    devicetype: res.data.data,
                    devicetypeid: res.data.data.device_type_id,
                    devicetypename: res.data.data.name
                });
                // tslint:disable
                // console.log(this.state.devicetype);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        VoltageSensor()
            .then(res => {
                this.setState({
                    voltagesensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        CurrentSensor()
            .then(res => {
                this.setState({
                    currentsensorid: res.data.data
                    // devicetypeid: res.data.data.device_type_id,
                    // devicetypename: res.data.data.name
                });
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.probe_name &&
                this.state.wifi_network_name &&
                this.state.wifi_password &&
                this.state.device_type_id &&
                this.state.channelnumber &&
                this.state.voltage_sensor_id &&
                this.state.current_rating_sensor_id
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.open });
        // openState = this.state.open;
    };

    public sensorSelection = () => {
        let sensorId = this.state.current_sensor_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        const selRating = this.state.currentsensorid.find(
            sensor => sensor.sensor_id === sensorId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selrating: selRating.ratings });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            probe_name: this.state.probe_name,
            wifi_network_name: this.state.wifi_network_name,
            wifi_password: this.state.wifi_password,
            device_type_id: this.state.device_type_id
            // channels: []
        };
        const channels: any[] = [];
        times(this.state.channelnumber, i => {
            channels.push({
                channel_threshold_current: 0,
                power_factor: 0,
                default_voltage: 0,
                scaling_factor_voltage: 0,
                scaling_factor_current: 0,
                sensor_type_id: "",
                current_sensor_id: this.state.current_rating_sensor_id,
                voltage_sensor_id: this.state.voltage_sensor_id
            });
        });

        if (channels !== []) {
            body["channels"] = channels;
        }

        // @ts-ignore
        CreateProbes(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Probe not successfully created. Please try again!");
            });
    };

    public render() {
        // tslint:disable

        const Devicetypes = this.state.devicetype.map((DeviceType, id) => (
            <MenuItem key={id} value={DeviceType.device_type_id}>
                {DeviceType.name}
            </MenuItem>
        ));

        // const CurrentSensors = this.state.currentsensorid.map(
        //     (CurrentSensor, id) => (
        //         <MenuItem key={id} value={CurrentSensor.sensor_id}>
        //             {CurrentSensor.name}
        //         </MenuItem>
        //     )
        // );

        // const CurrentSensorRatings = this.state.selrating.map(
        //     (CurrentSensorRating, id) => (
        //         <MenuItem key={id} value={CurrentSensorRating.rating_id}>
        //             {CurrentSensorRating.rating}
        //         </MenuItem>
        //     )
        // );

        // const VoltageSensors = this.state.voltagesensorid.map(
        //     (VoltageSensor, id) => (
        //         <MenuItem key={id} value={VoltageSensor.sensor_id}>
        //             {VoltageSensor.name}
        //         </MenuItem>
        //     )
        // );

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create Probe</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Probe Name</Typography></div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            onChange={this.handleChange("probe_name")}
                                            name="probe_name"
                                            value={this.state.probe_name}
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Number Of Channels</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            type="number"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.channelnumber}
                                            onChange={this.handleChange("channelnumber")}
                                            name="channelnumber"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Device Type</Typography>
                                    </div>
                                    <div>
                                        <Select
                                            value={this.state.device_type_id}
                                            onChange={this.handleChange("device_type_id")}
                                            name="devicetype"
                                            style={{ width: '300px' }}
                                        >
                                            {Devicetypes}
                                            {/* <MenuItem value={this.state.devicetypeid}>
                                        {this.state.devicetypename}
                                    </MenuItem> */}
                                        </Select>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>WIFI Network Name</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.wifi_network_name}
                                            onChange={this.handleChange("wifi_network_name")}
                                            name="wifi_network_name"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>WIFI Password</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            type="password"
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.wifi_password}
                                            onChange={this.handleChange("wifi_password")}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        {/* <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Current Sensor</Typography>
                                <Select
                                    value={this.state.current_sensor_id}
                                    onChange={this.handleChange(
                                        "current_sensor_id",
                                        this.sensorSelection
                                    )}
                                    name="devicetype"
                                >
                                    {CurrentSensors}
                                </Select>
                            </TableCell>
                        </TableRow> */}
                        {/* <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Current Sensor Rating</Typography>
                                <Select
                                    value={this.state.current_rating_sensor_id}
                                    onChange={this.handleChange("current_rating_sensor_id")}
                                    name="devicetype"
                                >
                                    {CurrentSensorRatings}
                                </Select>
                            </TableCell>
                        </TableRow> */}
                        {/* <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Voltage Sensor</Typography>
                                <Select
                                    value={this.state.voltage_sensor_id}
                                    onChange={this.handleChange("voltage_sensor_id")}
                                    name="devicetype"
                                >
                                    {VoltageSensors}
                                </Select>
                            </TableCell>
                        </TableRow> */}
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Update Firmware
                                </Button>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Save Probe
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Probe successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/probe/list-probe">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateProbe;
