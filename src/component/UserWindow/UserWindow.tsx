// import Button from "@material-ui/core/Button";
import * as React from "react";
import { withRouter } from "react-router-dom";
// import SideBar from '../SideBar/SideBar';
import UserMenuWindow from "../UserMenuWindow/UserMenuWindow";
import "./UserWindow.css";

interface Iprops {
    user_role
    user_id
}

class UserWindow extends React.Component<Iprops, {}> {
    // @ts-ignore works
    constructor(props) {
        super(props);
    }

    // public componentWillMount() {
    //     // tslint:disable
    //     console.log(this.props.user_role)
    // }

    public render() {
        return (
            <div className="rootapp">
                <div className="userwindow">
                        <UserMenuWindow
                            // {...this.props}
                            user_role={this.props.user_role}
                            user_id={this.props.user_id}
                        />

                </div>
            </div>
        );
    }
}

export default withRouter(UserWindow);
