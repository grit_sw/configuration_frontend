import * as React from 'react';
import { Link } from 'react-router-dom';
import './PowerSourcesSettings.css';




const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class PowerSourcesSettings extends React.Component {

  // @ts-ignore works
  constructor(props) {
    super(props);

  }



  public render() {


    return (
      <div>
        <ul className = "lists">
          <li className = "lists">
            <Link to="/source/list-source" style={style}>List Sources</Link>
          </li>

          <li className = "lists">
            <Link to="/source/create-source" style={style}>Create Source</Link>
          </li>
        </ul>
      </div>

    )

  }


}



export default PowerSourcesSettings;
