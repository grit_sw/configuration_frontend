import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateGroupType } from "../../Services/ConsumerGroupRequests/ConsumerGroupRequests";
import "./CreateConsumerGroupType.css";

interface Istate {
    open: boolean;
    value: string;
    name: string;
    disabled: boolean;
    open1: boolean;
}

class CreateConsumerGroupType extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            disabled: true,
            name: "",
            open: true,
            open1: false,
            value: "",
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (
                this.state.name
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.name,
        };

        // @ts-ignore
        CreateGroupType(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Consumer Group not created. Please try again!");
            });
    };

    public render() {
        // @ts-ignore works
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Consumer Group</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Name</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.name}
                                    onChange={this.handleChange("name")}
                                    name="name"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    ADD GROUP TYPE
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Consumer Group Type successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/consumer/list-consumer">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateConsumerGroupType;
