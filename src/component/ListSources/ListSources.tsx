import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
// import cookie from "react-cookies";
import { Link } from "react-router-dom";
import { ListUserSensor } from "../../Services/SensorRequests/SensorRequests";
import ListGenerator from "../ListGenerator/ListGenerator";
import ListGrid from "../ListGrid/ListGrid";
import ListInverter from "../ListInverter/ListInverter";
import "./ListSources.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Iprops {
    display3;
    user;
    username;
    userpowersource;
    user_role;
    user_id
}

interface Istate {
    listonephasegrid: any[];
    admin: boolean
    listusersensor: any[]
    listusergenerator: any[]
    listusergrid: any[]
    listuserinverter: any[]
    open1a: boolean
    id: string;
    open1b: boolean
}

class ListSources extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);

        this.state = {
            admin: false,
            id: "",
            listonephasegrid: [],
            listusergenerator: [],
            listusergrid: [],
            listuserinverter: [],
            listusersensor: [],
            open1a: false,
            open1b: false
        };
    }

    public componentWillMount() {
        ListUserSensor()
            .then(res => {
                this.setState({
                    listusergenerator: res.data.generator,
                    listusergrid: res.data.grid,
                    listuserinverter: res.data.inverter,
                    listusersensor: res.data,
                });
                // tslint:disable
                console.log(this.state.listusersensor);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // tslint:disable

        // console.log(this.props.user_role)
        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }

        // console.log(this.props.user_id)
        // let user = this.props.user_id

        // ViewUserProbes(user)
        //     .then(res => {
        //         console.log(res)
        //         this.setState({ viewuserprobes: res.data.data, display: true });
        //         // tslint:disable
        //         // console.log(this.state.viewuserprobes);
        //         // console.log(this.state.viewuserprobes.length);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         this.setState({ error: err.response, display: false });
        //         console.log(this.state.error);
        //     });
    }

    handleClickOpen1 = id => {
        console.log(id);
        this.setState({ open1a: true, id: id });
    };

    handleClose1 = () => {
        this.setState({ open1a: false, open1b: false });
    };

    public handleSubmitGrid1 = () => {
        // event.preventDefault();

        const gridid = this.state.id;
        // @ts-ignore

        // @ts-ignore
        DeleteOnePhaseGrid(gridid)
            .then(res => {
                this.setState({ open1b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };


    public CreateColumn(id, probe_name) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/fuel/view/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/fuel/edit/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/fuel/delete/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            onClick={this.handleClickOpen1.bind(this, id)}
                            style={{ border: "none", margin: 0 }}
                        >
                            Delete
                        </Button>
                        <Dialog
                            open={this.state.open1a}
                            onClose={this.handleClose1}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    Are You Sure You Want To Delete This?
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleClose1} color="primary">
                                    No
                                </Button>
                                <Button
                                    onClick={this.handleSubmitGrid1.bind(this, id)}
                                    color="primary"
                                    autoFocus
                                >
                                    Yes
                                </Button>
                                <Dialog
                                    open={this.state.open1b}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Deleted
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose1} color="primary">
                                            Ok
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </DialogActions>
                        </Dialog>
                    </Link>
                </li>
            </ul>
        );
    }

    public render() {

        console.log(this.props.userpowersource)

        const Generator = this.state.listusergenerator.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
            </TableRow>
        ));

        const Grid = this.state.listusergrid.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
            </TableRow>
        ));

        const Inverter = this.state.listuserinverter.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
            </TableRow>
        ));



        const Generator1 = !this.props.userpowersource.generator ? ""
            : this.props.userpowersource.generator.map((ListConfig, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                    <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                    <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
                </TableRow>
            ));

        const Grid1 = !this.props.userpowersource.grid ? ""
            : this.props.userpowersource.grid.map((ListConfig, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                    <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                    <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
                </TableRow>
            ));

        const Inverter1 = !this.props.userpowersource.inverter ? ""
            : this.props.userpowersource.inverter.map((ListConfig, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.name}</TableCell>
                    <TableCell style={{ minWidth: 200, maxWidth: 200 }}>{ListConfig.source_type}</TableCell>
                    <TableCell style={{ minWidth: 100, maxWidth: 100 }} />
                    <TableCell>{this.CreateColumn(ListConfig.meter_id, ListConfig.name)}</TableCell>
                </TableRow>
            ));

        const userName = this.props.username;

        const userName1 = localStorage.getItem("user_name");

        const userprobes = this.props.userpowersource;

        console.log(userprobes.count)

        const userprobeslength = userprobes.length;


        return (
            <div>
                {(!this.props.display3 && userprobeslength === 0) || (this.props.display3 && userprobeslength === 0) ? (
                    <div>
                        {this.props.username ? (
                            <div>{userName} Has No Probes</div>
                        ) : (
                                <div>Select a user to view their Power Sources</div>
                            )}
                    </div>
                ) : (
                        <div>
                            <Table className="table" style={{ overflowX: "auto", tableLayout: 'auto', width: 900 }}>
                                <TableHead className="head">
                                    <TableRow className="tablerow">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Sources associated with: {userName}</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Name</TableCell>
                                        <TableCell>Type</TableCell>
                                        <TableCell>phases</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {Generator1}
                                    {Grid1}
                                    {Inverter1}
                                </TableBody>
                            </Table>
                        </div>
                    )}

                <div>&nbsp;</div>

                {this.state.admin ? (
                    <div>
                        <Table className="table" style={{ overflowX: "auto", tableLayout: 'auto', width: 900 }}>
                            <TableHead className="head">
                                <TableRow className="tablecell">
                                    <TableCell className="tablecell">
                                        <span id="tablecell">Power Sources</span>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <ListGrid />
                                <ListGenerator />
                                <ListInverter />
                            </TableBody>
                        </Table>
                    </div>
                ) : (
                        <div>
                            <Table className="table" style={{ overflowX: "auto", tableLayout: 'auto', width: 900 }}>
                                <TableHead className="head">
                                    <TableRow className="tablerow">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Sources associated with: {userName1}</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Name</TableCell>
                                        <TableCell>Type</TableCell>
                                        <TableCell>phases</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {Generator}
                                    {Grid}
                                    {Inverter}
                                </TableBody>
                            </Table>
                        </div>
                    )}
            </div>
        );
    }
}

export default ListSources;
