import "./ConfigurationSettings.css";

import * as React from "react";
import { Link } from "react-router-dom";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class ConfigurationSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className = 'lists'>
          <li className = 'lists'>
            <Link to="/configuration/create-group" style={style}>
              Create configuration Group
            </Link>
          </li>

          <li className = 'lists'>
            <Link to="/configuration/list-group" style={style}>
              List configuration Group
            </Link>
          </li>

          <li className = 'lists'>
            <Link to="/configuration/create-configuration" style={style}>
              Create Configuration
            </Link>
          </li>

          <li className = 'lists'>
            <Link to="/configuration/list-configuration" style={style}>
              List Configuration
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default ConfigurationSettings;
