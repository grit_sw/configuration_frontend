// import Checkbox from "@material-ui/core/Checkbox";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListProbe, ViewUserProbes } from "../../Services/ProbeRequests/ProbeRequests";
// import { UserDetails } from "../../Services/UsersRequests/UsersRequests";
import "./ListProbes.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Istate {
    checkedC: boolean;
    checkedD: boolean;
    listprobe: any[];
    display: boolean;
    error: string;
    viewuserprobes: any[];
    userid: string;
    admin: boolean
    roleid: number
    test: string
}

interface Iprops {
    display;
    user;
    username;
    userprobe;
    user_role;
    user_id
}

class ListProbes extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            admin: false,
            checkedC: true,
            checkedD: true,
            display: this.props.display,
            error: "",
            listprobe: [],
            roleid: 0,
            test: "",
            userid: "",
            viewuserprobes: [],
        };
    }

    public componentWillMount() {
        ListProbe()
            .then(res => {
                this.setState({ listprobe: res.data.data });
                // tslint:disable
                console.log(this.state.listprobe);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // console.log(this.props.user_role)
        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }

        console.log(this.props.user_id)
        let user = this.props.user_id

        ViewUserProbes(user)
            .then(res => {
                console.log(res)
                this.setState({ viewuserprobes: res.data.data, display: true });
                // tslint:disable
                // console.log(this.state.viewuserprobes);
                // console.log(this.state.viewuserprobes.length);
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display: false });
                console.log(this.state.error);
            });
    }

    public CreateColumn(id, probe_name) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/probe/view-probe/" + id} style={style}>
                        View
          </Link>
                </li>
                <li className="ullist">
                    <Link to={"/probe/edit-probe/" + id} style={style}>
                        Edit
          </Link>
                </li>
            </ul>
        );
    }

    public handleChange = name => event => {
        this.setState({ ...this.state, [name]: event.target.checked });
    };

    public render() {

        // console.log(this.props.userprobe)
        // console.log(this.state.display)

        const Roles = this.state.listprobe.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.probe_name}</TableCell>
                <TableCell>{this.CreateColumn(ListConfig.probe_id, ListConfig.probe_name)}</TableCell>
            </TableRow>
        ));
        const FuelTypelength = this.state.listprobe.length;
        console.log(FuelTypelength);

        // const userId = this.props.user;
        const userName = this.props.username;

        console.log(userName)

        const UserProbes = !this.props.userprobe ? <div />
            : this.props.userprobe.map((ListConfig, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell>{ListConfig.probe_name}</TableCell>
                    <TableCell>{this.CreateColumn(ListConfig.probe_id, ListConfig.probe_name)}</TableCell>
                </TableRow>
            ));

        const NonAdminUserProbes = this.state.listprobe.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.probe_name}</TableCell>
                <TableCell>{this.CreateColumn(ListConfig.probe_id, ListConfig.probe_name)}</TableCell>
            </TableRow>
        ));

        const userprobes = this.props.userprobe;

        const userprobeslength = userprobes.length;

        return (
            <div>
                {(!this.props.display && userprobeslength === 0) || (this.props.display && userprobeslength === 0) ? (
                    <div>
                        {this.props.username ? (
                            <div>{userName} Has No Probes</div>
                        ) : (
                                <div>Select a user to view their Probes</div>
                            )}
                    </div>
                ) : (
                        <div>
                            <Table className="table1" style={{ overflowX: "auto", width: 900 }}>
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">
                                                Probes with channels associated with {userName}
                                            </span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>Probe Name</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                    {UserProbes}
                                </TableBody>
                            </Table>
                        </div>
                    )}

                <div>&nbsp;</div>

                {this.state.admin ? (
                    <div>
                        <Table className="table1" style={{ overflowX: "auto", width: 900 }}>
                            <TableHead className="head">
                                <TableRow className="tablecell">
                                    <TableCell className="tablecell">
                                        <span id="tablecell">Probes</span>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className="tablerow">
                                    <TableCell>Probe Name</TableCell>
                                    <TableCell>Actions</TableCell>
                                </TableRow>
                                {Roles}
                            </TableBody>
                        </Table>
                    </div>
                ) : (
                        <div>
                            <Table className="table1" style={{ overflowX: "auto", width: 900 }}>
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Probes</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Probe Name</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                    {NonAdminUserProbes}
                                </TableBody>
                            </Table>
                        </div>
                    )}

            </div>
        );
    }
}

export default ListProbes;
