import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import "./ViewCommunicationDevice.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
}

class ViewCommunicationDevice extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      name: [],
      open: true,
      sourceconfig: "",
      // profitmargin: '',
      value: ""
    };
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Probe</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Table className="table">
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Name</Typography>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>WIFI Network Name</Typography>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>WIFI Password</Typography>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Measurement Interval</Typography>
                        <TextField
                          className="textField"
                          type="number"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.profitmargin}
                          onChange={this.handleChange("profitmargin")}
                          name="profitmargin"
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <div>&nbsp;</div>

                <Table className="table">
                  <TableHead className="head">
                    <TableRow className="tablecell">
                      <TableCell className="tablecell">
                        <span id="tablecell">Meter Configs</span>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">IP Address</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">Port</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">Meter type</li>
                          </ul>
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              style={{ width: 100 }}
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              style={{ width: 100 }}
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              style={{ width: 100 }}
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell colSpan={3}>
                        <Button
                          variant="contained"
                          style={{ backgroundColor: "#337ab7" }}
                        >
                          Add Meter Config
                        </Button>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                >
                  Save Device
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewCommunicationDevice;
