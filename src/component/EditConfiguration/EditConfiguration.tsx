import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
// import ListBox from 'react-listbox';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import DualListBox from "react-dual-listbox";
import {
    EditConfig,
    GroupAdmin,
    SelectProbes,
    ViewConfig
} from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./EditConfiguration.css";

interface Istate {
    config?: "";
    groupadmin: any[];
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    selected: any[];
    selected1: any[];
    probes?: "";
    channels?: "";
    viewconfig: {};
    selectprobe: any[];
    selpro: any[];
    configuration_name: string;
    configuration_user: string;
    configdetailid: string;
    configdetailname: string;
    error_threshold: number;
    memberid: string;
    membername: string;
    energy: boolean;
    channelid: string;
    membersdetails: any[];
    open1: boolean;
    disabled: boolean;
}
class EditConfiguration extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            channelid: "",
            configdetailid: "",
            configdetailname: "",
            configs: true,
            configuration_name: "",
            configuration_user: "",
            disabled: true,
            energy: true,
            error_threshold: 1,
            groupadmin: [],
            memberid: "",
            membername: "",
            membersdetails: [],
            name: [],
            open: true,
            open1: false,
            selected: [],
            selected1: [],
            selectprobe: [],
            selpro: [],
            sourceconfig: "",
            // profitmargin: '',
            value: "",
            viewconfig: {}
        };
    }

    public componentWillMount() {
        GroupAdmin()
            .then(res => {
                this.setState({ groupadmin: res.data.data });
                // tslint:disable
                // console.log(this.state.groupadmin);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // @ts-ignore works
        const fuelId = this.props.match.params.id;

        // tslint:disable

        SelectProbes()
            .then(res => {
                this.setState({ selectprobe: res.data.data });
                // tslint:disable
                // console.log(res);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ViewConfig(fuelId)
            .then(res => {
                this.setState({
                    viewconfig: res.data.data,
                    configuration_name: res.data.data.configuration_name,
                    configuration_user: res.data.data.admin_id
                });
                // tslint:disable
                console.log(res.data.data);
                // console.log(this.state.listfuels.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public probeSelection = () => {
        let probeId = this.state.probes;
        const selprobe = this.state.selectprobe.find(
            probe => probe.probe_id === probeId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selpro: selprobe.channels });
    };

    public array = () => {
        let configId = this.state.configuration_user;
        const setconfigdetail = this.state.groupadmin.find(
            configdetail => configdetail.id === configId
        );


        this.setState({
            configdetailid: setconfigdetail.user_id,
            configdetailname: setconfigdetail.first_name
        });
    };

    // @ts-ignore works
    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.configuration_name &&
                this.state.error_threshold &&
                this.state.configdetailid &&
                this.state.configdetailname &&
                this.state.selected &&
                this.state.probes &&
                this.state.selected1
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public onChange = selected => {
        console.log(selected);
        console.log(typeof selected);
        const members = selected.map(member => {
            // 'a*b'  'c*d'
            const [id, name] = member.split("*");
            return {
                member_id: id,
                member_name: name
            };
        });

        this.setState(
            {
                selected: members
            },
            () => {
                if (
                    this.state.configuration_name &&
                    this.state.error_threshold &&
                    this.state.configdetailid &&
                    this.state.configdetailname &&
                    this.state.selected &&
                    this.state.probes &&
                    this.state.selected1
                ) {
                    this.setState({ disabled: false });
                }
            }
        );
        console.log(members);
        console.log(this.state.selected);
    };

    public onChange1 = selected1 => {
        // tslint:disable
        console.log(selected1);

        this.setState(
            {
                selected1: selected1
            },
            () => {
                if (
                    this.state.configuration_name &&
                    this.state.error_threshold &&
                    this.state.configdetailid &&
                    this.state.configdetailname &&
                    this.state.selected &&
                    this.state.probes &&
                    this.state.selected1
                ) {
                    this.setState({ disabled: false });
                }
            }
        );
    };

    handleClose1 = () => {
        this.setState({ open1: false });
    };

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });

        const body = {
            // tslint:disable
            configuration_name: this.state.configuration_name,
            error_threshold: this.state.error_threshold,
            configuration_user: {
                configuration_user_id: this.state.configdetailid,
                configuration_user_name: this.state.configdetailname
            },
            configuration_members: this.state.selected,
            display_energy_today: true,
            probe_channels: [
                {
                    probe_id: this.state.probes,
                    channel_ids: this.state.selected1
                }
            ],

            // @ts-ignore works
            fuel_id: this.props.match.params.id

            // channels: []
        };

        console.log(body);
        console.log("here");
        // @ts-ignore
        EditConfig(body, body.fuel_id)
            .then(res => {
                console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                console.log(this.state.memberid);
                console.log(this.state.selected);
                alert("Unsuccessful. Please try again");
            });
    };

    public render() {
        console.log(this.props);

        // const { selected } = this.state;
        const selected = this.state.selected.map(
            member => `${member.member_id}*${member.member_name}`
        );
        console.log(selected);
        const { selected1 } = this.state;

        const values = this.state.groupadmin.map(value => {
            return {
                value: `${value.user_id}*${value.full_name}`,
                label: `${value.full_name}`
            };
        });

        const Groupadmins = this.state.groupadmin.map((GroupAdmin, id) => (
            <MenuItem key={id} value={GroupAdmin.id}>
                {GroupAdmin.first_name} {GroupAdmin.last_name}
            </MenuItem>
        ));

        const SelectProbe = this.state.selectprobe.map((Probes, id) => {
            return (
                <MenuItem key={id} value={Probes.probe_id}>
                    {Probes.probe_name}
                </MenuItem>
            );
        });

        const ProbeChannels = this.state.selpro.map(channels => {
            return {
                value: channels.channel_id,
                label: channels.channel_name
            };
        });

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Edit {this.state.configuration_name}</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>Name</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.configuration_name}
                                    onChange={this.handleChange("configuration_name")}
                                    name="configuration_name"
                                />
                            </TableCell>
                            <TableCell colSpan={2} rowSpan={2} />
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Assign to User</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.configuration_user}
                                    onChange={this.handleChange("configuration_user", this.array)}
                                    name="configuration_user"
                                >
                                    {Groupadmins}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell colSpan={3}>
                                <DualListBox
                                    name="moons"
                                    options={values}
                                    selected={selected}
                                    // tslint:disable

                                    onChange={this.onChange}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Error Threshold</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.error_threshold}
                                    onChange={this.handleChange("error_threshold")}
                                    name="error_threshold"
                                />
                            </TableCell>
                            <TableCell colSpan={2} rowSpan={3} />
                        </TableRow>
                        <TableRow>
                            <TableCell>Display Energy Today</TableCell>
                            <TableCell>
                                <Select
                                    inputProps={{
                                        readOnly: Boolean("readOnly"),
                                        disabled: Boolean("readOnly")
                                    }}
                                    required
                                    value={this.state.energy}
                                    onChange={this.handleChange("energy")}
                                    name="energy"
                                >
                                    <MenuItem value="true">True</MenuItem>
                                    <MenuItem value="false">False</MenuItem>
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Select Probes</TableCell>
                            <TableCell>
                                <Select
                                    // open={this.state.configs}
                                    // onClose={this.handleClose}
                                    // onOpen={this.handleOpen("probes")}
                                    value={this.state.probes}
                                    onChange={this.handleChange("probes", this.probeSelection)}
                                    name="probes"
                                >
                                    {SelectProbe}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell>Available Channels</TableCell>
                            <TableCell>Assigned Channels</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell colSpan={2}>
                                <DualListBox
                                    name="moons"
                                    options={ProbeChannels}
                                    selected={selected1}
                                    // tslint:disable

                                    onChange={this.onChange1}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("dualist")}
                                    disabled={this.state.disabled}
                                >
                                    Save
                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Configuration successfully edited
                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose1} color="primary">
                                            Close
                    </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }

    // interface IConfigProps {
    //     configprops: {};
    // }

    // interface IConfigState {
    //     configState: {value: string};
    // }
}

export default EditConfiguration;
