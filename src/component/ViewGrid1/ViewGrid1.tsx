import { Typography } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewOnePhaseGrid } from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./ViewGrid1.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  viewonephasegrid: {};
  names: string;
  signal_threshold: string;
  probe: string;
  channel: string;
}

class ViewGrid1 extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      channel: "",
      configs: true,
      groupadmin: "",
      name: [],
      names: "",
      open: true,
      probe: "",
      signal_threshold: "",
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewonephasegrid: {}
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const sensortypeId = this.props.match.params.id;

    // tslint:disable
    console.log(sensortypeId);

    ViewOnePhaseGrid(sensortypeId)
      .then(res => {
        this.setState({
          viewonephasegrid: res.data.data,
          names: res.data.data.name,
          signal_threshold: res.data.data.signal_threshold,
          config: res.data.data.configuration.configuration_name,
          probe: res.data.data.probe.probe_name,
          channel: res.data.data.channel[0].channel_name
        });
        // tslint:disable
        console.log(res.data.data);
        console.log(res.data.data.channel[0].channel_name);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">
                  View 3 phase Grid power source : GRID (AEDC)
                </span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Table className="table" style={{ margin: 10, width: 680 }}>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Name</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.names}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Signal Threshold</Typography>
                      </TableCell>
                      <TableCell>
                        <TextField
                          className="textField"
                          type="number"
                          margin="normal"
                          variant="outlined"
                          font-size="true"
                          value={this.state.signal_threshold}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <div>&nbsp;</div>
                <Divider />
                <div>&nbsp;</div>

                <Table className="table">
                  <TableHead className="head">
                    <TableRow className="tablecell">
                      <TableCell className="tablecell">
                        <span id="tablecell">Channel Assignment</span>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Config</Typography>
                        <Select
                          value={this.state.config}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.config}>
                            <em>{this.state.config}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Probe(s)</Typography>
                        <Select
                          value={this.state.probe}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.probe}>
                            <em>{this.state.probe}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>Phase 1 Channel</Typography>
                        <Select
                          value={this.state.channel}
                          inputProps={{
                            readOnly: Boolean("readOnly"),
                            disabled: Boolean("readOnly")
                          }}
                          required
                        >
                          <MenuItem value={this.state.channel}>
                            <em>{this.state.channel}</em>
                          </MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Utility Charge Map</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className="tablerow">
                      <TableCell>
                        <Typography>
                          <ul className="viewlist">
                            <li className="viewlist">00:00 to 06:00</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">06:00 to 12:00</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">12:00 to 18:00</li>
                            &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">18:00 to 00:00</li>
                          </ul>
                        </Typography>

                        <ul className="viewlist">
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                          <li className="viewlist">
                            <TextField
                              className="textField"
                              type="number"
                              margin="normal"
                              variant="outlined"
                              font-size="true"
                              value={this.state.profitmargin}
                              name="profitmargin"
                            />
                          </li>
                        </ul>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewGrid1;
