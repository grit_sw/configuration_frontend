import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { CreateSensor } from "../../Services/SensorRequests/SensorRequests";
import "./DeleteSensor.css";


interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    price: number;
    profitmargin?: number;
    rating: number;
    scaling_factor: number;
    sensor_name: string;
    size: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
}

class DeleteSensors extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            groupadmin: "",
            name: [],
            open: true,
            price: 0,
            rating: 0,
            scaling_factor: 0,
            sensor_name: "",
            size: 0,
            sourceconfig: "",
            // profitmargin: '',
            value: ""
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public handleSubmit = (event) => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.sensor_name,
            rating: this.state.rating,

            // channels: []
        };

        // @ts-ignore
        CreateSensor(body)
            .then(res => {
                // console.log(res);
            })
            .catch(err => {
                console.log(err);
            });
    };

    public render() {
        return (
            <div>
                <Table className="table" style={{ borderColor: "#337ab7", width: 700 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create Sensors</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Name</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.sensor_name}
                                    onChange={this.handleChange("sensor_name")}
                                    name="sensor_name"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Scaling Factor</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.scaling_factor}
                                    onChange={this.handleChange("scaling_factor")}
                                    name="scaling_factor"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <Typography>Max Current</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.rating}
                                    onChange={this.handleChange("rating")}
                                    name="rating"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Size</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.size}
                                    onChange={this.handleChange("size")}
                                    name="size"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Price</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.price}
                                    onChange={this.handleChange("price")}
                                    name="price"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                >
                                    Save
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default DeleteSensors;
