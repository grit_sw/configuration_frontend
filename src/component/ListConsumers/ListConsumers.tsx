import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import {
    DeleteGroup,
    ListGroups
} from "../../Services/ConsumerGroupRequests/ConsumerGroupRequests";
import {
    Delete3PhaseConsumerLine,
    DeleteOnePhaseConsumerLine,
    List3PhaseConsumerLine,
    ListOnePhaseConsumerLine,
    UserConsumerLine
} from "../../Services/ConsumerLineRequests/ConsumerLineRequests";
import "./ListConsumers.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Iprops {
    display4;
    user;
    username;
    userline;
    user_role;
    user_id
}

interface Istate {
    id: string;
    listgroups: any[];
    open1a: boolean;
    open1b: boolean;
    openL1a: boolean;
    openL1b: boolean;
    openL3a: boolean;
    openL3b: boolean;
    onephase: any[];
    threephase: any[];
    onephasechannels: any[];
    threephasechannels: any[];
    admin: boolean
    userconsumerline: any[]
}

class ListConsumers extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            admin: false,
            id: "",
            listgroups: [],
            onephase: [],
            onephasechannels: [],
            open1a: false,
            open1b: false,
            openL1a: false,
            openL1b: false,
            openL3a: false,
            openL3b: false,
            threephase: [],
            threephasechannels: [],
            userconsumerline: []
        };
    }

    public componentWillMount() {
        ListGroups()
            .then(res => {
                this.setState({ listgroups: res.data.data });
                // tslint:disable
                console.log(this.state.listgroups);
                console.log(this.state.listgroups.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListOnePhaseConsumerLine()
            .then(res => {
                this.setState({ onephase: res.data.data });
                // tslint:disable
                console.log(res.data.data);
                console.log(this.state.listgroups.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        List3PhaseConsumerLine()
            .then(res => {
                this.setState({ threephase: res.data.data });
                // tslint:disable
                console.log(res.data.data);
                console.log(this.state.listgroups.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        List3PhaseConsumerLine()
            .then(res => {
                this.setState({ threephasechannels: res.data.data });
                // tslint:disable
                console.log(res.data.data.channels);
                console.log(this.state.listgroups.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        UserConsumerLine()
            .then(res => {
                this.setState({ userconsumerline: res.data.data });
                // tslint:disable
                console.log(res.data.data.channels);
                console.log(this.state.listgroups.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });


        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }
    }

    public handleSubmitConsumer1 = () => {
        const gridid = this.state.id;

        // @ts-ignore
        DeleteOnePhaseConsumerLine(gridid)
            .then(res => {
                this.setState({ openL1b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    public handleSubmitConsumer3 = () => {
        const gridid = this.state.id;

        // @ts-ignore
        Delete3PhaseConsumerLine(gridid)
            .then(res => {
                this.setState({ openL3b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    public handleSubmitConsumer = () => {
        const groupid = this.state.id;

        // @ts-ignore
        DeleteGroup(groupid)
            .then(res => {
                this.setState({ open1b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    handleClickOpen = id => {
        console.log(id);
        this.setState({ open1a: true, id: id });
    };

    handleClose = () => {
        this.setState({ open1a: false, open1b: false });
    };

    handleClickOpen1 = id => {
        console.log(id);
        this.setState({ openL1a: true, id: id });
    };

    handleClickOpen3 = id => {
        console.log(id);
        this.setState({ openL3a: true, id: id });
    };

    handleClose1 = () => {
        this.setState({ openL1a: false, openL1b: false });
    };

    handleClose3 = () => {
        this.setState({ openL3a: false, openL3b: false });
    };

    public CreateColumn(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/consumer-group/view-group/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/consumer-group/edit-group/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.handleClickOpen.bind(this, id)}
                        style={{ border: "none", margin: 0 }}
                    >
                        Delete
                    </Button>
                    <Dialog
                        open={this.state.open1a}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are You Sure You Want To Delete This?
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Disagree
                            </Button>
                            <Button
                                onClick={this.handleSubmitConsumer.bind(this, id)}
                                color="primary"
                                autoFocus
                            >
                                Agree
                            </Button>
                            <Dialog
                                open={this.state.open1b}
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Deleted
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose} color="primary">
                                        Ok
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </DialogActions>
                    </Dialog>
                </li>
            </ul>
        );
    }

    public CreateColumn1(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/consumer-group/view-group/" + id} style={style}>
                        View
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/consumer-group/edit-group/" + id} style={style}>
                        Edit
                    </Link>
                </li>
                <li className="ullist">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.handleClickOpen1.bind(this, id)}
                        style={{ border: "none", margin: 0 }}
                    >
                        Delete
                    </Button>
                    <Dialog
                        open={this.state.openL1a}
                        onClose={this.handleClose1}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are You Sure You Want To Delete This?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose1} color="primary">
                                Disagree
                            </Button>
                            <Button
                                onClick={this.handleSubmitConsumer1.bind(this, id)}
                                color="primary"
                                autoFocus
                            >
                                Agree
                            </Button>
                            <Dialog
                                open={this.state.openL1b}
                                onClose={this.handleClose1}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Deleted
                                </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose1} color="primary">
                                        Ok
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </DialogActions>
                    </Dialog>
                </li>
            </ul>
        );
    }

    public CreateColumn3(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/consumer-group/view-group/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/consumer-group/edit-group/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.handleClickOpen3.bind(this, id)}
                        style={{ border: "none", margin: 0 }}
                    >
                        Delete
                    </Button>
                    <Dialog
                        open={this.state.openL3a}
                        onClose={this.handleClose3}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are You Sure You Want To Delete This?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose3} color="primary">
                                Disagree
                            </Button>
                            <Button
                                onClick={this.handleSubmitConsumer3.bind(this, id)}
                                color="primary"
                                autoFocus
                            >
                                Agree
                            </Button>
                            <Dialog
                                open={this.state.openL3b}
                                onClose={this.handleClose3}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Deleted
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose3} color="primary">
                                        Ok
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </DialogActions>
                    </Dialog>
                </li>
            </ul>
        );
    }

    public render() {
        const channelist3 = this.state.threephase.map((ListFuel, id) => {
            let channelList;
            channelList = ListFuel.channels.map((channel, id) => (
                <ul>
                    <li>{channel.channel_name}</li>
                </ul>
            ));
            return channelList;
        });

        const channelist1 = this.state.onephase.map((ListFuel, id) => {
            let channelList;
            channelList = ListFuel.channel.map((channel, id) => (
                <ul>
                    <li>{channel.channel_name}</li>
                </ul>
            ));
            return channelList;
        });

        const Grouptype = this.state.listgroups.map((ListFuel, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn(ListFuel.group_id)}</TableCell>
            </TableRow>
        ));

        const Singlephase = this.state.onephase.map((ListFuel, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListFuel.consumer_group}</TableCell>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{channelist1[id]}</TableCell>
                <TableCell>{ListFuel.phase_type.name}</TableCell>
                <TableCell />
                <TableCell>{this.CreateColumn(ListFuel.group_id)}</TableCell>
            </TableRow>
        ));

        const Threephase = this.state.threephase.map((ListFuel, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListFuel.consumer_group}</TableCell>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{ListFuel.name}</TableCell>
                <TableCell>{channelist3[id]}</TableCell>
                <TableCell>{ListFuel.phase_type.name}</TableCell>
                <TableCell />
                <TableCell>{this.CreateColumn(ListFuel.group_id)}</TableCell>
            </TableRow>
        ));

        const Consumer = !this.props.userline ? ""
            : this.props.userline.map((ListLine, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell>{ListLine.consumer_group}</TableCell>
                    <TableCell>{ListLine.name}</TableCell>
                    <TableCell>{ListLine.name}</TableCell>
                    <TableCell>{channelist3[id]}</TableCell>
                    <TableCell>{ListLine.phase_type.name}</TableCell>
                    <TableCell />
                    <TableCell>{this.CreateColumn(ListLine.group_id)}</TableCell>
                </TableRow>
            ));

        const ConsumerLine = !this.state.userconsumerline ? ""
            : this.state.userconsumerline.map((ListFuel, id) => (
                <TableRow key={id} style={{ height: 50 }}>
                    <TableCell>{ListFuel.consumer_group}</TableCell>
                    <TableCell>{ListFuel.name}</TableCell>
                    <TableCell>{ListFuel.name}</TableCell>
                    <TableCell>{channelist3[id]}</TableCell>
                    <TableCell>{ListFuel.phase_type.name}</TableCell>
                    <TableCell />
                    <TableCell>{this.CreateColumn(ListFuel.group_id)}</TableCell>
                </TableRow>
            ));

        const userName = this.props.username;

        // const userName1 = localStorage.getItem("user_name");

        const userlines = this.props.userline;

        console.log(userlines.count)

        const userlineslength = userlines.length;

        return (
            <div>
                {(!this.props.display4 && userlineslength === 0) || (this.props.display4 && userlineslength === 0) ? (
                    <div>
                        {this.props.username ? (
                            <div>{userName} Has No Probes</div>
                        ) : (
                                <div>Select a user to view their Power Sources</div>
                            )}
                    </div>
                ) : (
                        <div className="table" style={{ overflowX: "auto" }}>
                            <Table className="table">
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Consumer Lines</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Group</TableCell>
                                        <TableCell>Line</TableCell>
                                        <TableCell>Consumers</TableCell>
                                        <TableCell>Channels</TableCell>
                                        <TableCell>Phases</TableCell>
                                        <TableCell>Probes</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {Consumer}
                                </TableBody>
                            </Table>
                        </div>
                    )}

                <div>&nbsp;</div>

                {this.state.admin ? (
                    <div>
                        <div className="table" style={{ overflowX: "auto" }}>
                            <Table className="table">
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Consumer Lines</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Group</TableCell>
                                        <TableCell>Line</TableCell>
                                        <TableCell>Consumers</TableCell>
                                        <TableCell>Channels</TableCell>
                                        <TableCell>Phases</TableCell>
                                        <TableCell>Probes</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {Singlephase}
                                    {Threephase}
                                </TableBody>
                            </Table>
                        </div>

                        <div>&nbsp;</div>

                        <div className="table" style={{ overflowX: "auto" }}>
                            <Table className="table">
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Consumer Groups</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Group</TableCell>
                                        <TableCell>Lines</TableCell>
                                        <TableCell>Consumers</TableCell>
                                        <TableCell>Channels</TableCell>
                                        <TableCell>Probes</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {Grouptype}
                                </TableBody>
                            </Table>
                        </div>
                    </div>
                ) : (
                        <div className="table" style={{ overflowX: "auto" }}>
                            <Table className="table">
                                <TableHead className="head">
                                    <TableRow className="tablecell">
                                        <TableCell className="tablecell">
                                            <span id="tablecell">Power Consumer Lines</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow className="tablerow">
                                        <TableCell>Group</TableCell>
                                        <TableCell>Line</TableCell>
                                        <TableCell>Consumers</TableCell>
                                        <TableCell>Channels</TableCell>
                                        <TableCell>Phases</TableCell>
                                        <TableCell>Probes</TableCell>
                                        <TableCell>Action</TableCell>
                                    </TableRow>
                                    {ConsumerLine}
                                </TableBody>
                            </Table>
                        </div>
                    )}
            </div>
        );
    }
}

export default ListConsumers;

// {Singlephase}
// {Threephase}
