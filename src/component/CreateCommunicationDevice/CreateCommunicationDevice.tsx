import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import "./CreateCommunicationDevice.css";

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
}

class CreateCommunicationDevice extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      name: [],
      open: true,
      sourceconfig: "",
      value: ""
    };
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
  };

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Probe</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>WIFI Network Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                />
              </TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>WIFI Password</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={3}>
                <Button variant="contained" style={{backgroundColor:'#337ab7'}}>Save Device</Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default CreateCommunicationDevice;
