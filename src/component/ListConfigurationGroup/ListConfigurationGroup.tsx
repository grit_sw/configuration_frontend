import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfigGroup } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./ListConfigurationGroup.css";

const style = {
  color: "#337ab7",
  textDecoration: "none"
};

interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  listconfiggroup: any[];
}
class ListConfigurationGroup extends React.Component<{}, Istate> {
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      listconfiggroup: [],
      name: [],
      open: true,
      sourceconfig: "",
      value: ""
    };
  }

  public componentWillMount() {
    ListConfigGroup()
      .then(res => {
        this.setState({ listconfiggroup: res.data.data });
        // tslint:disable
        console.log(this.state.listconfiggroup);
        console.log(this.state.listconfiggroup.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }


  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  public handleOpen = name => event => {

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
  };

  public CreateColumn(id) {
    return (
      <ul className="ullist">
        <li className="ullist">
          <Link to={"/configuration/view-group/" + id} style={style}>
            View
          </Link>
        </li>
        <li className="ullist">
          <Link to={"/configuration/edit-group/" + id} style={style}>
            Edit
          </Link>
        </li>
      </ul>
    );
  }

  public render() {
    const ConfigGroup = this.state.listconfiggroup.map((ListConfig, id) => (
      <TableRow key={id} style={{ height: 50 }}>
        <TableCell>{ListConfig.name}</TableCell>
        <TableCell />
        <TableCell />
        <TableCell />
        <TableCell>{this.CreateColumn(ListConfig.id)}</TableCell>
      </TableRow>
    ));
    const FuelTypelength = this.state.listconfiggroup.length;
    console.log(FuelTypelength);


    return (
      <div>
        {/* <Table className="table1">
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">
                  Configuraion Groups associated with user
                </span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow" style={{ fontWeight: "bold" }}>
              <TableCell>Group Name</TableCell>
              <TableCell>Group Admin</TableCell>
              <TableCell>Source Configuration</TableCell>
              <TableCell>Member Configuration</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableBody>
        </Table> */}

        <div>&nbsp;</div>

        <Table className="table1">
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">All Configuration Groups</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow" style={{ fontWeight: "bold" }}>
              <TableCell>Group Name</TableCell>
              <TableCell>Group Admin</TableCell>
              <TableCell>Source Configuration</TableCell>
              <TableCell>Member Configuration</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>

            {ConfigGroup}
          </TableBody>
        </Table>
      </div>
    );
  }

}

export default ListConfigurationGroup;
