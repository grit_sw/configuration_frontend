import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import "react-dual-listbox/lib/react-dual-listbox.css";
import { Link } from "react-router-dom";
import "./CommunicationDevice.css";

const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class CommunicationDevice extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Communication Devices</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>Device Name</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>GEPM</TableCell>
              <TableCell>
                <ul className='ullist'>
                  <li className='ullist'>
                    <Link to="/products/view-comm-device" style={style}>View</Link>
                  </li>
                  <li className='ullist'>
                    <Link to="/products/edit-comm-device" style={style}>Edit</Link>
                  </li>
                  <li className='ullist'>
                    <Link to="/products/delete-comm-device" style={style}>Delete</Link>
                  </li>
                </ul>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default CommunicationDevice;
