import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewFuels } from "../../Services/FuelRequests/FuelRequests";
import "./ViewFuel.css";

interface Istate {
  config?: "";
  fuel_type: string;
  groupadmin?: "";
  open: boolean;
  price: number;
  sourceconfig?: "";
  val: string;
  value: any;
  name: [];
  configs: boolean;
  viewfuels: {};
}

class ViewFuel extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      fuel_type: "njnj",
      groupadmin: "",
      name: [],
      open: true,
      price: 20,
      sourceconfig: "",
      // profitmargin: '',
      val: `${ViewFuels}`,
      value: "",
      viewfuels: {}
    };
  }

  public handleChange = name => event => {
    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.value });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };

  // @ts-ignore works
  public handleOpen = name => event => {
    // this.setState({ open: true });

    // @ts-ignore works
    this.setState({ ...this.State, [name]: event.target.open });
    // openState = this.state.open;
  };

  public componentWillMount() {
    // @ts-ignore works
    const fuelId = this.props.match.params.id;

    // tslint:disable
    console.log(fuelId);

    ViewFuels(fuelId)
      .then(res => {
        this.setState({
          viewfuels: res.data.data,
          fuel_type: res.data.data.name
        });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    // tslint:disable
    // @ts-ignore works
    // console.log(ViewFuels.name);

    const ViewFuels = this.state.viewfuels.name;

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Fuel Type</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Fuel Type:</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.fuel_type}
                  InputProps={{
                    readOnly: true
                  }}
                  name="fuel_type"
                />
              </TableCell>
              {/* <TableCell>
                <Typography>Unit Price:</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.price}
                  InputProps={{
                    readOnly: true
                  }}
                  onChange={this.handleChange("price")}
                  name="price"
                />
              </TableCell> */}
            </TableRow>
            <TableRow>
              <TableCell colSpan={3}>
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#337ab7" }}
                >
                  Back
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewFuel;
