import { MenuItem } from "@material-ui/core";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { ListGroups } from "../../Services/UsersRequests/UsersRequests";
import "./ListUserGroup.css";

interface Istate {
  listgroups: any[];
}

class ListUserGroup extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);

    this.state = {
      listgroups: []
    };
  }

  public componentWillMount() {
    ListGroups()
      .then(res => {
        this.setState({ listgroups: res.data.data });
        // tslint:disable
        console.log(this.state.listgroups);
        console.log(this.state.listgroups.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    const Groups = this.state.listgroups.map((Group, id) => (
      <MenuItem key={id} value={Group.id}>
        {Group.name}
      </MenuItem>
    ));
    return (
      <div>
        <Table className="table1">
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">User Groups</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Admin</TableCell>
              <TableCell>Members</TableCell>
              <TableCell>Margin</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
            <TableRow className="tablerow">
              {Groups}
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }

  // interface IConfigProps {
  //     configprops: {};
  // }

  // interface IConfigState {
  //     configState: {value: string};
  // }
}

export default ListUserGroup;
