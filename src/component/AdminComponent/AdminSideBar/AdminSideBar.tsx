import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
// import MenuItem from "material-ui/MenuItem";
// import Avatar from "material-ui/Avatar";
// import List from "material-ui/List/List";
// import RaisedButton from "material-ui/RaisedButton";
// import ListItem from "material-ui/List/ListItem";
import Battery20Icon from "@material-ui/icons/Battery20";
import BuildIcon from "@material-ui/icons/Build";
import HomeIcon from "@material-ui/icons/Home";
import PaymentIcon from "@material-ui/icons/Payment";
import PeopleIcon from "@material-ui/icons/People";
import SettingsIcon from "@material-ui/icons/Settings";
import SettingsInputHdmiIcon from "@material-ui/icons/SettingsInputHdmi";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import * as React from "react";
import { Link } from "react-router-dom";
import "./AdminSideBar.css";

const styles = {
	avatar: {
		height: 70,
		width: 70
	},
	button: {
		backgroundColor: "#373737",
		color: "#fff",
		marginTop: "20px"
	},
	sidebarSubtitle: {
		color: "#8080808c",
		fontSize: "85%",
		margin: "20px"
	}
};

const userRole = localStorage.getItem("user_role");
const userToken = localStorage.getItem("user_token");

interface AdminSidebarProps extends WithStyles<typeof styles> {
	// tslint:disable
	close?: Function;
	menu?;
}
interface AdminSidebarState {
	grit_admin: boolean;
	admin: boolean;
}

class AdminSidebar extends React.Component<
	AdminSidebarProps,
	AdminSidebarState
	> {
	constructor(props: AdminSidebarProps) {
		super(props);
		this.state = {
			admin: false,
			grit_admin: false
		};
	}

	// @ts-ignore
	public closeMenu = () => this.props.close();

	public logout = () => {
		localStorage.clear();
		window.location.href = "https://dev.gtg.grit.systems";
	};

	public componentWillMount() {
		const userRoleId = localStorage.getItem("user_role_id");

		// tslint:disable
		// console.log(userRoleId);
		if (userRoleId === "5") {
			this.setState({
				grit_admin: true,
				admin: true
			});
		}

		if (userToken && userRole === "5") {
			this.setState({ admin: true });
		}
	}

	render() {
		const sidebar = (
			<div className="adminlists">
				{this.state.admin ? (
					<div>
						<ListSubheader className="list-subheader">Menu</ListSubheader>
						<List>
							<Link
								to="/configuration/list-configuration"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
								className="list-item-link"
							>
								<ListItem className="list">
									<ListItemIcon
										children={<SettingsIcon nativeColor="#757575" />}
									/>
									<ListItemText primary="Configurations" />
								</ListItem>
							</Link>

							<Link
								to="/usergroup/list-users"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<PeopleIcon nativeColor="#757575" />}
									/>
									<ListItemText>Users</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/probe/list-probe"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<BuildIcon nativeColor="#757575" />}
									/>
									<ListItemText>Probes</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/source/list-source"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<SettingsInputHdmiIcon nativeColor="#757575" />}
									/>
									<ListItemText>Power sources</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/consumer/list-consumer"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<PeopleIcon nativeColor="#757575" />}
									/>
									<ListItemText>Power Consumers</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/configuration"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<PaymentIcon nativeColor="#757575" />}
									/>
									<ListItemText>Payments</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/products/list-products"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<VerifiedUserIcon nativeColor="#757575" />}
									/>
									<ListItemText>Products</ListItemText>
								</ListItem>
							</Link>

							<Link
								to="/fuel/list-fuel"
								style={{
									color: "#337ab7",
									textDecoration: "none"
								}}
								onClick={this.closeMenu}
							>
								<ListItem className="list">
									<ListItemIcon
										children={<Battery20Icon nativeColor="#757575" />}
									/>
									<ListItemText>Fuel</ListItemText>
								</ListItem>
							</Link>
						</List>
					</div>
				) : (
						<div>
							<ListSubheader className="list-subheader">Menu</ListSubheader>
							<List>
								<Link
									to="/probe/list-probe"
									style={{
										color: "#337ab7",
										textDecoration: "none"
									}}
									onClick={this.closeMenu}
								>
									<ListItem className="list">
										<ListItemIcon
											children={<BuildIcon nativeColor="#757575" />}
										/>
										<ListItemText>Probes</ListItemText>
									</ListItem>
								</Link>

								<Link
									to="/consumer/list-consumer"
									style={{
										color: "#337ab7",
										textDecoration: "none"
									}}
									onClick={this.closeMenu}
								>
									<ListItem className="list">
										<ListItemIcon
											children={<PeopleIcon nativeColor="#757575" />}
										/>
										<ListItemText>Power Consumers</ListItemText>
									</ListItem>
								</Link>

								<Link
									to="/fuel/list-fuel"
									style={{
										color: "#337ab7",
										textDecoration: "none"
									}}
									onClick={this.closeMenu}
								>
									<ListItem className="list">
										<ListItemIcon
											children={<Battery20Icon nativeColor="#757575" />}
										/>
										<ListItemText>Fuel</ListItemText>
									</ListItem>
								</Link>
							</List>
						</div>
					)}
			</div>
		);
		return (
			<div className="admin-sidebar bodyy">
				<List component="nav">
					<header style={{ display: "flex" }}>
						<img
							style={{ margin: "10px auto" }}
							src={window.location.origin + "/img/logo.png"}
						/>
					</header>
					<Divider className="sidebar-divider" />
					<section className="avatar-section centered">
						<Avatar
							src={window.location.origin + "/img/avatar.png"}
							alt="Profile avatar"
							className={this.props.classes.avatar}
						/>
					</section>
					{/* <BrowserRouter> */}
					<Link to="/" className="list-item-link" onClick={this.closeMenu}>
						<ListItem button={true}>
							<ListItemIcon children={<HomeIcon nativeColor="#757575" />} />
							<ListItemText primary="Home" />
						</ListItem>
					</Link>
					{/* </BrowserRouter> */}
					<Divider className="sidebar-divider" />
					{sidebar}
					<Divider className="sidebar-divider" />
					<ListSubheader className="list-subheader">
						GTG
                    </ListSubheader>
					<a
						href="https://dev.gtg.grit.systems"
						style={{ textDecoration: "none" }}
					>
						<ListItem>
							<ListItemIcon
								children={<AccountBalanceIcon nativeColor="#757575" />}
							/>
							<ListItemText primary="GTG" />
						</ListItem>
					</a>
					<Divider className="sidebar-divider" />
					<ListSubheader className="list-subheader">
						Command and Control
                    </ListSubheader>
					<a
						href="https://cmdctrl.grit.systems"
						style={{ textDecoration: "none" }}
					>
						<ListItem>
							<ListItemIcon
								children={<AccountBalanceIcon nativeColor="#757575" />}
							/>
							<ListItemText primary="Command Control" />
						</ListItem>
					</a>
					<Divider className="sidebar-divider" />
                </List>

				<div className="logout centered">
					<Button
						// href="http://mydomain.com:3001"
						onClick={this.logout}
						variant="contained"
						size="medium"
						classes={{ root: this.props.classes.button }}
					>
						Log Out
          </Button>
				</div>
			</div>
		);
	}
}

export default withStyles(styles)(AdminSidebar);
