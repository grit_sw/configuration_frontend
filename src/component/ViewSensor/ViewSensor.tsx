import { Typography } from "@material-ui/core";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { ViewSensor } from "../../Services/SensorRequests/SensorRequests";
import "./ViewSensor.css";


interface Istate {
  config?: "";
  groupadmin?: "";
  open: boolean;
  price: number;
  profitmargin?: number;
  rating: number;
  scaling_factor: number;
  sensor_name: string;
  size: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  viewsensors: {};
}

class ViewSensors extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      groupadmin: "",
      name: [],
      open: true,
      price: 0,
      rating: 0,
      scaling_factor: 0,
      sensor_name: "",
      size: 0,
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewsensors: {},
    };
  }


  public componentWillMount() {
    // @ts-ignore works
    const sensortypeId = this.props.match.params.id;

    // tslint:disable
    console.log(sensortypeId);

    ViewSensor(sensortypeId)
      .then(res => {
        this.setState({ viewsensors: res.data.data });
        // tslint:disable
        console.log(res.data.data);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {

    // @ts-ignore works
    const ViewSensors = this.state.viewsensors.name;
    // @ts-ignore works
    const rating = this.state.viewsensors.rating;

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Create Sensors</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>
                <Typography>Name</Typography>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={ViewSensors}
                  name="sensor_name"
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography>Max Current</Typography>
                <TextField
                  className="textField"
                  type="number"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={rating}
                  name="rating"
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ViewSensors;
