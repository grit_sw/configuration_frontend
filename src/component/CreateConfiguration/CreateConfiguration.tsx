import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import DualListBox from "react-dual-listbox";
import { Link } from "react-router-dom";
import {
    CreateConfigurations,
    GroupAdmin,
    SelectProbes
} from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./CreateConfiguration.css";

interface Istate {
    config?: "";
    configuration_members: string;
    groupadmin: any[];
    open: boolean;
    profitmargin?: number;
    error_threshold: number;
    configuration_user: string;
    sourceconfig?: "";
    value: "";
    name: [];
    configuration_name: string;
    configs: boolean;
    selected: any[];
    selectprobe: any[];
    selpro: any[];
    probes?: "";
    probe_channels: string;
    channels?: "";
    configdetailid: string;
    configdetailname: string;
    memberid: string;
    membername: string;
    membersdetails: any[];
    disabled: boolean;
    open1: boolean;
}

class CreateConfiguration extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configdetailid: "",
            configdetailname: "",
            configs: true,
            configuration_members: "",
            configuration_name: "",
            configuration_user: "",
            disabled: true,
            error_threshold: 1,
            groupadmin: [],
            memberid: "",
            membername: "",
            membersdetails: [],
            name: [],
            open: true,
            open1: false,
            probe_channels: "",
            selected: [],
            selectprobe: [],
            selpro: [],
            sourceconfig: "",
            value: ""
        };
    }

    public componentWillMount() {
        GroupAdmin()
            .then(res => {
                this.setState({ groupadmin: res.data.data });
                // tslint:disable
                // console.log(this.state.groupadmin);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        SelectProbes()
            .then(res => {
                this.setState({ selectprobe: res.data.data });
                // tslint:disable
                // console.log(this.state.selectprobe);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public selectProbe = () => {
        // const probeId = this.state.probes;
        SelectProbes()
            .then(res => {
                this.setState({ selectprobe: res.data.data });
                // tslint:disable
                // console.log(this.state.selectprobe);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    };

    public probeSelection = () => {
        let probeId = this.state.probes;
        const selprobe = this.state.selectprobe.find(
            probe => probe.probe_id === probeId
        );
        this.setState({ selpro: selprobe.channels });
    };

    public array = () => {
        let configId = this.state.configuration_user;
        const setconfigdetail = this.state.groupadmin.find(
            configdetail => configdetail.id === configId
        );

        this.setState({
            configdetailid: setconfigdetail.user_id,
            configdetailname: setconfigdetail.first_name
        });
    };


    // @ts-ignore works
    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.configuration_name &&
                this.state.error_threshold &&
                this.state.configdetailid &&
                this.state.configdetailname &&
                this.state.membersdetails &&
                this.state.selected &&
                this.state.probes &&
                this.state.channels
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    handleClose1 = () => {
        this.setState({ open1: false });
    };

    public onChange = selected => {
        // tslint:disable
        const members = selected.map(member => {
            const [id, name] = member.split("*");
            return {
                member_id: id,
                member_name: name
            };
        });

        this.setState({
            selected: members
        });

        if (
            this.state.configuration_name &&
            this.state.error_threshold &&
            this.state.configdetailid &&
            this.state.configdetailname &&
            this.state.membersdetails &&
            // this.state.selected &&
            this.state.probes &&
            this.state.channels
        ) {
            this.setState({ disabled: false });
        }
    };

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            configuration_name: this.state.configuration_name,
            error_threshold: this.state.error_threshold,
            configuration_user: {
                configuration_user_id: this.state.configdetailid,
                configuration_user_name: this.state.configdetailname
            },
            configuration_members: this.state.selected,
            display_energy_today: true,
            probe_channels: [
                {
                    probe_id: this.state.probes,
                    channel_ids: [this.state.channels]
                }
            ]
        };

        // @ts-ignore
        CreateConfigurations(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Configuration not created. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public render() {
        {
            this.state;
        }
        const selected = this.state.selected.map(
            member => `${member.member_id}*${member.member_name}`
        );

        const values = this.state.groupadmin.map(value => {
            return {
                value: `${value.user_id}*${value.full_name}`,
                label: `${value.full_name}`
            };
        });

        const Groupadmins = this.state.groupadmin.map((GroupAdmin, id) => (
            <MenuItem key={id} value={GroupAdmin.id}>
                {GroupAdmin.first_name} {GroupAdmin.last_name}
            </MenuItem>
        ));

        const SelectProbe = this.state.selectprobe.map((Probes, id) => {
            return (
                <MenuItem key={id} value={Probes.probe_id}>
                    {Probes.probe_name}
                </MenuItem>
            );
        });

        const ProbeChannels = this.state.selpro.map((channels, id) => (
            <MenuItem key={id} value={channels.channel_id}>
                {channels.channel_name}
            </MenuItem>
        ));

        // const members = this.state.selected.map((prob, id) => {
        //     let configId = prob;
        //     const setmemberdetail = this.state.groupadmin.find(
        //         memberdetail => memberdetail.user_id === configId.member_id
        //     );

        //     return setmemberdetail;
        // });

        // console.log(members);
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto" }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Configuration</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>Name</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.configuration_name}
                                    onChange={this.handleChange("configuration_name")}
                                    name="configuration_name"
                                />
                            </TableCell>
                            <TableCell colSpan={2} rowSpan={2} />
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Assign to User</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.configuration_user}
                                    onChange={this.handleChange("configuration_user", this.array)}
                                    name="configuration_user"
                                >
                                    {Groupadmins}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell />
                            <TableCell>All Users</TableCell>
                            <TableCell>Config Members</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell colSpan={3}>
                                <DualListBox
                                    name="moons"
                                    options={values}
                                    selected={selected}
                                    onChange={this.onChange}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Error Threshold</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    label="number"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.error_threshold}
                                    onChange={this.handleChange("error_threshold")}
                                    name="error_threshold"
                                />
                            </TableCell>
                            <TableCell colSpan={2} rowSpan={3} />
                        </TableRow>
                        <TableRow>
                            <TableCell>Select Probes</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.probes}
                                    onChange={this.handleChange("probes", this.probeSelection)}
                                    name="probes"
                                >
                                    {SelectProbe}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Assign Channels</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.channels}
                                    onChange={this.handleChange("channels")}
                                    name="channels"
                                >
                                    {ProbeChannels}
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3} style={{ textAlign: "center" }}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("dualist")}
                                    disabled={this.state.disabled}
                                >
                                    Create Config Group
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Configuration successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/configuration/list-configuration">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                &nbsp;
      </div>
        );
    }
}

export default CreateConfiguration;
