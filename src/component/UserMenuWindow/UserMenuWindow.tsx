// import Divider from "@material-ui/core/Divider";
import Divider from "@material-ui/core/Divider";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import BackButton from "../BackButton/BackButton";
import CommunicationDevice from "../CommunicationDevice/CommunicationDevice";
import Create1PhaseConsumerLine from "../Create1PhaseConsumerLine/Create1PhaseConsumerLine";
import Create3PhaseConsumerLine from "../Create3PhaseConsumerLine/Create3PhaseConsumerLine";
import CreateCommunicationDevice from "../CreateCommunicationDevice/CreateCommunicationDevice";
import CreateConfiguration from "../CreateConfiguration/CreateConfiguration";
import CreateConfigurationGroup from "../CreateConfigurationGroup/CreateConfigurationGroup";
import CreateConsumer from "../CreateConsumer/CreateConsumer";
import CreateConsumer1 from "../CreateConsumer1/CreateConsumer1";
import CreateConsumer3 from "../CreateConsumer3/CreateConsumer3";
import CreateConsumerGroup from "../CreateConsumerGroup/CreateConsumerGroup";
import CreateConsumerGroupType from "../CreateConsumerGroupType/CreateConsumerGroupType";
import CreateConsumerLine from "../CreateConsumerLine/CreateConsumerLine";
import CreateDevices from "../CreateDevices/CreateDevices";
import CreateDomain from "../CreateDomain/CreateDomain";
import CreateFuel from "../CreateFuel/CreateFuel";
import CreateGen1 from "../CreateGen1/CreateGen1";
import CreateGen3 from "../CreateGen3/CreateGen3";
import CreateGrid1 from "../CreateGrid1/CreateGrid1";
import CreateGrid3 from "../CreateGrid3/CreateGrid3";
import CreateInv1 from "../CreateInv1/CreateInv1";
import CreateInv3 from "../CreateInv3/CreateInv3";
import CreateNewUserGroup from "../CreateNewUserGroup/CreateNewUserGroup";
import CreateProbe from "../CreateProbe/CreateProbe";
import CreateProduct from "../CreateProduct/CreateProduct";
import CreateRole from "../CreateRole/CreateRole";
import CreateSensors from "../CreateSensors/CreateSensors";
import CreateSource from "../CreateSource/CreateSource";
import DeleteCommunicationDevice from "../DeleteCommunicationDevice/DeleteCommunicationDevice";
// import DeleteConfiguration from "../DeleteConfiguration/DeleteConfiguration";
// import DeleteConfigurationGroup from "../DeleteConfigurationGroup/DeleteConfigurationGroup";
// import DeleteDevice from "../DeleteDevice/DeleteDevice";
// import DeleteFuel from "../DeleteFuel/DeleteFuel";
// import DeleteGen1 from "../DeleteGen1/DeleteGen1";
// import DeleteGen3 from "../DeleteGen3/DeleteGen3";
// import DeleteGrid1 from "../DeleteGrid1/DeleteGrid1";
// import DeleteGrid3 from "../DeleteGrid3/DeleteGrid3";
// import DeleteInv1 from "../DeleteInv1/DeleteInv1";
// import DeleteInv3 from "../DeleteInv3/DeleteInv3";
// import DeleteProbe from "../DeleteProbe/DeleteProbe";
import DeleteSensors from "../DeleteSensor/DeleteSensor";
import Devices from "../Devices/Devices";
import EditCommunicationDevice from "../EditCommunicationDevice/EditCommunicationDevice";
import EditConfiguration from "../EditConfiguration/EditConfiguration";
import EditConfigurationGroup from "../EditConfigurationGroup/EditConfigurationGroup";
import EditConsumerGroup from "../EditConsumerGroup/EditConsumerGroup";
import EditDevice from "../EditDevice/EditDevice";
import EditFuel from "../EditFuel/EditFuel";
import EditGen1 from "../EditGen1/EditGen1";
import EditGen3 from "../EditGen3/EditGen3";
import EditGrid1 from "../EditGrid1/EditGrid1";
import EditGrid3 from "../EditGrid3/EditGrid3";
import EditInv1 from "../EditInv1/EditInv1";
import EditInv3 from "../EditInv3/EditInv3";
import EditProbe from "../EditProbe/EditProbe";
import EditRole from "../EditRole/EditRole";
import EditSensors from "../EditSensor/EditSensor";
import EditUser from "../EditUser/EditUser";
import ListConfiguration from "../ListConfiguration/ListConfiguration";
import ListConfigurationGroup from "../ListConfigurationGroup/ListConfigurationGroup";
import ListConsumers from "../ListConsumers/ListConsumers";
import ListDomains from "../ListDomains/ListDomains";
import ListFuel from "../ListFuel/ListFuel";
import ListProbes from "../ListProbes/ListProbes";
import ListSources from "../ListSources/ListSources";
import ListUserGroup from "../ListUserGroup/ListUserGroup";
import ListUsers from "../ListUsers/ListUsers";
import Product from "../Products/Products";
import SelectUser from "../SelectUser/SelectUser";
import Sensors from "../Sensors/Sensors";
import UserMenuBar from "../UserMenuBar/UserMenuBar";
import ViewCommunicationDevice from "../ViewCommunicationDevice/ViewCommunicationDevice";
import ViewConfiguration from "../ViewConfiguration/ViewConfiguration";
import ViewConfigurationGroup from "../ViewConfigurationGroup/ViewConfigurationGroup";
import ViewDevice from "../ViewDevice/ViewDevice";
import ViewFuel from "../ViewFuel/ViewFuel";
import ViewGen1 from "../ViewGen1/ViewGen1";
import ViewGen3 from "../ViewGen3/ViewGen3";
import ViewGrid1 from "../ViewGrid1/ViewGrid1";
import ViewGrid3 from "../ViewGrid3/ViewGrid3";
import ViewInv1 from "../ViewInv1/ViewInv1";
import ViewInv3 from "../ViewInv3/ViewInv3";
import ViewProbe from "../ViewProbe/ViewProbe";
import ViewSensors from "../ViewSensor/ViewSensor";
import ViewUser from "../ViewUser/ViewUser";
import "./UserMenuWindow.css";

interface Istate {
    user: string;
    username: string;
    userprobe: string
    display: string
    userconfiguration: string
    display1: string
    userfuel: string
    display2: string
    usertoken: string
    admin: boolean
    userpowersource: string
    display3: string
    userline: string
    display4: string
}

interface Iprops {
    user_role
    user_id
}

class UserMenuWindow extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);

        this.state = {
            admin: false,
            display: "",
            display1: "",
            display2: "",
            display3: "",
            display4: "",
            user: "",
            userconfiguration: "",
            userfuel: "",
            userline: "",
            username: "",
            userpowersource: "",
            userprobe: "",
            usertoken: ""
        };
    }

    public componentWillMount() {
        // tslint:disable
        // console.log(this.props.user_role)

        if (this.props.user_role === '5') {
            console.log(this.state.admin)
            this.setState({ admin: true })
        }
    }

    public User = useremail => {
        // tslint:disable
        console.log(useremail);
        this.setState({ user: useremail });
        console.log(this.state.user);
    };

    public Username = username => {
        // tslint:disable
        console.log(username);
        this.setState({ username: username });
    };

    public Userprobe = Userprobe => {
        // tslint:disable
        console.log(Userprobe);
        this.setState({ userprobe: Userprobe });
    };

    public Display = display => {
        // tslint:disable
        console.log(display);
        this.setState({ display: display });
    };

    public Userconfiguration = Userconfiguration => {
        // tslint:disable
        console.log(Userconfiguration);
        this.setState({ userconfiguration: Userconfiguration });
    };

    public Display1 = display1 => {
        // tslint:disable
        console.log(display1);
        this.setState({ display1: display1 });
    };

    public Userfuel = Userfuel => {
        // tslint:disable
        console.log(Userfuel);
        this.setState({ userfuel: Userfuel });
    };

    public Display2 = display2 => {
        // tslint:disable
        console.log(display2);
        this.setState({ display2: display2 });
    };

    public UserPowerSource = UserPowerSource => {
        // tslint:disable
        console.log(UserPowerSource);
        this.setState({ userpowersource: UserPowerSource });
    };

    public Display3 = display3 => {
        // tslint:disable
        console.log(display3);
        this.setState({ display3: display3 });
    };

    public UserLine = UserLine => {
        // tslint:disable
        console.log(UserLine);
        this.setState({ userline: UserLine });
    };

    public Display4 = display4 => {
        // tslint:disable
        console.log(display4);
        this.setState({ display4: display4 });
    };

    public render() {
        // tslint:disable
        console.log(this.state.user);
        console.log(this.state.username);

        return (
            <div className='main'>
                <UserMenuBar />
                <div className="usermenuwindow">

                    {this.state.admin ? (
                        <div>
                            <SelectUser
                                storeUser={this.User}
                                storeUsername={this.Username}
                                storeUserProbe={this.Userprobe}
                                storeDisplay={this.Display}
                                storeUserConfiguration={this.Userconfiguration}
                                storeDisplay1={this.Display1}
                                storeUserFuel={this.Userfuel}
                                storeDisplay2={this.Display2}
                                storeUserPowerSource={this.UserPowerSource}
                                storeDisplay3={this.Display3}
                                storeUserLine={this.UserPowerSource}
                                storeDisplay4={this.Display4}
                            />
                        </div>
                    ) : (
                            <div />
                        )}


                    <div className="another">
                        <Divider />
                        <div>&nbsp;</div>
                        <div>
                            <div className="button">
                                <Switch>
                                    <Route path="/" component={BackButton} />
                                </Switch>
                            </div>
                        </div>
                        <div className="switch">
                            <Switch className="switch2">
                                <Route
                                    exact={true}
                                    path="/"
                                    render={props => (
                                        <ListConfiguration
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userconfiguration={this.state.userconfiguration}
                                            display1={this.state.display1}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/list-configuration"
                                    render={props => (
                                        <ListConfiguration
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userconfiguration={this.state.userconfiguration}
                                            display1={this.state.display1}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/list-group"
                                    component={ListConfigurationGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/create-configuration"
                                    component={CreateConfiguration}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/view-configuration/:id"
                                    component={ViewConfiguration}
                                />
                                {/* <Route
									exact={true}
									path="/configuration/delete-Configuration/:id"
									component={DeleteConfiguration}
								/> */}
                                <Route
                                    exact={true}
                                    path="/configuration/edit-configuration/:id"
                                    component={EditConfiguration}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/view-group/:id"
                                    component={ViewConfigurationGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/edit-group/:id"
                                    component={EditConfigurationGroup}
                                />
                                {/* <Route
									exact={true}
									path="/configuration/delete-group/:id"
									component={DeleteConfigurationGroup}
								/> */}
                                <Route
                                    exact={true}
                                    path="/usergroup/list-users"
                                    component={ListUsers}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/edit-users/:id"
                                    component={EditUser}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/view-users/:id"
                                    component={ViewUser}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/create-role"
                                    component={CreateRole}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/edit-role/:id"
                                    component={EditRole}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/list-group"
                                    component={ListUserGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/list-domain"
                                    component={ListDomains}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/create-domain"
                                    component={CreateDomain}
                                />
                                CreateDomain
                                <Route
                                    exact={true}
                                    path="/probe/list-probe"
                                    render={props => (
                                        <ListProbes
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userprobe={this.state.userprobe}
                                            display={this.state.display}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                />
                                {/* <Route
									exact={true}
									path="/probe/delete-probe/:id"
									component={DeleteProbe}
								/> */}
                                <Route
                                    exact={true}
                                    path="/probe/create-probe"
                                    component={CreateProbe}
                                />
                                <Route
                                    exact={true}
                                    path="/probe/view-probe/:id"
                                    component={ViewProbe}
                                />
                                <Route
                                    exact={true}
                                    path="/probe/edit-probe/:id"
                                    component={EditProbe}
                                />
                                <Route
                                    exact={true}
                                    path="/source/list-source"
                                    render={props => (
                                        <ListSources
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userpowersource={this.state.userpowersource}
                                            display3={this.state.display3}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                // ListSources
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/list-consumer"
                                    // component={ListConsumers}
                                    render={props => (
                                        <ListConsumers
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userline={this.state.userline}
                                            display4={this.state.display4}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer"
                                    component={CreateConsumer}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer1"
                                    component={CreateConsumer1}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer3"
                                    component={CreateConsumer3}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer-group"
                                    component={CreateConsumerGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer-group-type"
                                    component={CreateConsumerGroupType}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/edit-consumer-group"
                                    component={EditConsumerGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer-line"
                                    component={CreateConsumerLine}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer-group/view-group/:id"
                                    component={CreateConsumerLine}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer-group/edit-group/:id"
                                    component={CreateConsumerLine}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer-group/delete-group/:id"
                                    component={CreateConsumerLine}
                                />
                                <Route
                                    exact={true}
                                    path="/fuel/list-fuel"
                                    render={props => (
                                        <ListFuel
                                            {...props}
                                            user={this.state.user}
                                            username={this.state.username}
                                            userfuel={this.state.userfuel}
                                            display2={this.state.display2}
                                            user_role={this.props.user_role}
                                            user_id={this.props.user_id}
                                        />
                                    )}
                                />
                                <Route
                                    exact={true}
                                    path="/fuel/create-fuel"
                                    component={CreateFuel}
                                />
                                <Route
                                    exact={true}
                                    path="/fuel/view/:id"
                                    component={ViewFuel}
                                />
                                <Route
                                    exact={true}
                                    path="/fuel/edit/:id"
                                    component={EditFuel}
                                />
                                {/* <Route
									exact={true}
									path="/fuel/delete/:id"
									component={DeleteFuel}
								/> */}
                                <Route
                                    exact={true}
                                    path="/sensors/list-sensors"
                                    component={Sensors}
                                />
                                <Route
                                    exact={true}
                                    path="/sensors/view/:id"
                                    component={ViewSensors}
                                />
                                <Route
                                    exact={true}
                                    path="/sensors/edit/:id"
                                    component={EditSensors}
                                />
                                <Route
                                    exact={true}
                                    path="/sensors/delete/:id"
                                    component={DeleteSensors}
                                />
                                <Route
                                    exact={true}
                                    path="/sensors/create-sensor"
                                    component={CreateSensors}
                                />
                                <Route
                                    exact={true}
                                    path="/devices/list-devices"
                                    component={Devices}
                                />
                                <Route
                                    exact={true}
                                    path="/devices/create-device"
                                    component={CreateDevices}
                                />
                                <Route
                                    exact={true}
                                    path="/devices/view/:id"
                                    component={ViewDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/devices/edit/:id"
                                    component={EditDevice}
                                />
                                {/* <Route
									exact={true}
									path="/devices/delete/:id"
									component={DeleteDevice}
								/> */}
                                <Route
                                    exact={true}
                                    path="/products/list-products"
                                    component={Product}
                                />
                                <Route
                                    exact={true}
                                    path="/products/create-product"
                                    component={CreateProduct}
                                />
                                <Route
                                    exact={true}
                                    path="/products/list-comm-device"
                                    component={CommunicationDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/products/create-comm-device"
                                    component={CreateCommunicationDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/products/view-comm-device"
                                    component={ViewCommunicationDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/products/edit-comm-device"
                                    component={EditCommunicationDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/products/delete-comm-device"
                                    component={DeleteCommunicationDevice}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-source"
                                    component={CreateSource}
                                />
                                <Route
                                    exact={true}
                                    path="/usergroup/create-group"
                                    component={CreateNewUserGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/users"
                                    component={ListConfiguration}
                                />
                                <Route
                                    exact={true}
                                    path="/configuration/create-group"
                                    component={CreateConfigurationGroup}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-grid1/:id"
                                    component={ViewGrid1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-grid3/:id"
                                    component={ViewGrid3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-gen1/:id"
                                    component={ViewGen1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-gen3/:id"
                                    component={ViewGen3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-inv1/:id"
                                    component={ViewInv1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/view-inv3/:id"
                                    component={ViewInv3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-grid3/:id"
                                    component={EditGrid3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-grid1/:id"
                                    component={EditGrid1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-gen3/:id"
                                    component={EditGen3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-gen1/:id"
                                    component={EditGen1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-inv1/:id"
                                    component={EditInv1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/edit-inv3/:id"
                                    component={EditInv3}
                                />
                                {/* <Route
									exact={true}
									path="/source/delete-grid3/:id"
									component={DeleteGrid3}
								/> */}
                                {/* <Route
									exact={true}
									path="/source/delete-grid1/:id"
									component={DeleteGrid1}
								/> */}
                                {/* <Route
									exact={true}
									path="/source/delete-gen3/:id"
									component={DeleteGen3}
								/> */}
                                {/* <Route
									exact={true}
									path="/source/delete-gen1/:id"
									component={DeleteGen1}
								/> */}
                                {/* <Route
									exact={true}
									path="/source/delete-inv3/:id"
									component={DeleteInv3}
								/> */}
                                {/* <Route
									exact={true}
									path="/source/delete-inv3/:id"
									component={DeleteInv1}
								/> */}
                                <Route
                                    exact={true}
                                    path="/source/create-grid3"
                                    component={CreateGrid3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-grid1"
                                    component={CreateGrid1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-gen3"
                                    component={CreateGen3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-gen1"
                                    component={CreateGen1}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-inv3"
                                    component={CreateInv3}
                                />
                                <Route
                                    exact={true}
                                    path="/source/create-inv1"
                                    component={CreateInv1}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer-line3"
                                    component={Create3PhaseConsumerLine}
                                />
                                <Route
                                    exact={true}
                                    path="/consumer/create-consumer-line1"
                                    component={Create1PhaseConsumerLine}
                                />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserMenuWindow;
