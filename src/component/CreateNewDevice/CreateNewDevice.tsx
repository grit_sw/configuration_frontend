import Button from "@material-ui/core/Button";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Stepper from "@material-ui/core/Stepper";
import { withStyles, WithStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
// import PropTypes from "prop-types";
import * as React from "react";
import CreateConfiguration from "../CreateConfiguration/CreateConfiguration";

const styles = theme => ({
  backButton: {
    marginRight: theme.spacing.unit
  },
  instructions: {
    marginBottom: theme.spacing.unit,
    marginTop: theme.spacing.unit
  },
  root: {
    backButton: {
      marginRight: theme.spacing.unit
    },
    instructions: {
      marginBottom: theme.spacing.unit,
      marginTop: theme.spacing.unit
    },
    width: "90%"
  }
});

interface Iprops extends WithStyles<typeof styles> {}

interface Istate {
  activeStep: number;
}

function getSteps() {
  return [
    "Select master blaster campaign settings",
    "Create an ad group",
    "Create an ad"
  ];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <div>{CreateConfiguration}</div>;
    case 1:
      return "What is an ad group anyways?";
    case 2:
      return "This is the bit I really care about!";
    default:
      return "Unknown stepIndex";
  }
}

class CreateNewDevice extends React.Component<Iprops, Istate> {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0
    };
  }

  public handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1
    }));
  };

  public handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  public handleReset = () => {
    this.setState({
      activeStep: 0
    });
  };

  public render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel={true}>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed
              </Typography>
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              {/* <Typography className={classes.instructions}> */}
              {getStepContent(activeStep)}
              {/* </Typography> */}
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.backButton}
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleNext}
                >
                  {activeStep === steps.length - 1 ? "Finish" : "Next"}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

// HorizontalLabelPositionBelowStepper.propTypes = {
//   classes: PropTypes.object
// };

export default withStyles(styles)(CreateNewDevice);
