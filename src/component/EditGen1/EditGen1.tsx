import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfig } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import { ListFuels } from "../../Services/FuelRequests/FuelRequests";
import {
    EditOnePhaseGen,
    ViewOnePhaseGen
} from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./EditGen1.css";

interface Istate {
    config?: "";
    configuration_id: string;
    groupadmin?: "";
    open: boolean;
    probe_id: string;
    profitmargin?: number;
    signal_threshold: string;
    sourceconfig?: "";
    value: "";
    name: [];
    names: string;
    configs: boolean;
    listconfig: any[];
    listfuels: any[];
    channel_id: string;
    selpro: any[];
    selcha: any[];
    fuel_id: string;
    fuel_store_size: string;
    apparent_power_rating: string;
    start_timeOW: string;
    stop_timeOW: string;
    start_timeCM: string;
    stop_timeCM: string;
    viewonephasegen: {};
    map_id: string;
    old_start_timeCM: string;
    old_start_timeOW: string;
    old_stop_timeOW: string;
    old_stop_timeCM: string;
    window_id: string;
    data: any[];
    count: number;
    open1: boolean;
    quarter_load: number;
    half_load: number;
    three_quarter_load: number;
    full_load: number;
    old_quarter: number;
    old_half: number;
    old_three_quarter: number;
    old_full: number;
    old_map_id: string;
    disabled: boolean;
    open2: boolean;
}

const styles = theme => ({
    heading: {
        flexBasis: "33.33%",
        flexShrink: 0,
        fontSize: theme.typography.pxToRem(15)
    },

    root: {
        width: "100%"
    },

    secondaryHeading: {
        color: theme.palette.text.secondary,
        fontSize: theme.typography.pxToRem(15)
    }
});

class EditGen1 extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            apparent_power_rating: "",
            channel_id: "",
            configs: true,
            configuration_id: "",
            count: 1,
            data: [{}],
            disabled: true,
            fuel_id: "",
            fuel_store_size: "",
            full_load: 0,
            groupadmin: "",
            half_load: 0,
            listconfig: [],
            listfuels: [],
            map_id: "",
            name: [],
            names: "",
            old_full: 0,
            old_half: 0,
            old_map_id: "",
            old_quarter: 0,
            old_start_timeCM: "",
            old_start_timeOW: "",
            old_stop_timeCM: "",
            old_stop_timeOW: "",
            old_three_quarter: 0,
            open: true,
            open1: false,
            open2: false,
            probe_id: "",
            quarter_load: 0,
            selcha: [],
            selpro: [],
            signal_threshold: "",
            sourceconfig: "",
            start_timeCM: "",
            start_timeOW: "",
            stop_timeCM: "",
            stop_timeOW: "",
            three_quarter_load: 0,
            value: "",
            viewonephasegen: {},
            window_id: ""
        };
    }

    public componentWillMount() {
        ListConfig()
            .then(res => {
                this.setState({
                    listconfig: res.data.data
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(this.state.listconfig);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListFuels()
            .then(res => {
                this.setState({ listfuels: res.data.data });
                // tslint:disable
                console.log(this.state.listfuels);
                console.log(this.state.listfuels.length);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // @ts-ignore
        const sensortypeId = this.props.match.params.id;

        // tslint:disable
        console.log(sensortypeId);

        ViewOnePhaseGen(sensortypeId)
            .then(res => {
                this.setState({
                    viewonephasegen: res.data.data,
                    map_id: res.data.data.charge_map[0].map_id,
                    old_start_timeOW: res.data.data.operating_window[0].start_time,
                    old_stop_timeOW: res.data.data.operating_window[0].stop_time,
                    old_start_timeCM: res.data.data.charge_map[0].start_time,
                    old_stop_timeCM: res.data.data.charge_map[0].stop_time,
                    old_quarter: res.data.data.generator_map[0].quarter_load_price,
                    old_half: res.data.data.generator_map[0].half_load_price,
                    old_three_quarter:
                        res.data.data.generator_map[0].three_quarter_load_price,
                    old_full: res.data.data.generator_map[0].full_load_price,
                    old_map_id: res.data.data.generator_map[0].map_id
                    // window_id: res.data.data.operating_window[0].window_id
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(this.state.viewonephasegen);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public probeSelection = () => {
        let configId = this.state.configuration_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        console.log(configId);
        const selprobe = this.state.listconfig.find(
            probe => probe.configuration_id === configId
        );
        console.log(selprobe.probe_channels);
        // const probechannels = selprobe.channels;
        this.setState({ selpro: selprobe.probe_channels });
        console.log(this.state.selpro);
    };

    public channelSelection = () => {
        let probeId = this.state.probe_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        console.log(probeId);
        const selchannel = this.state.selpro.find(
            probe => probe.probe_id === probeId
        );
        console.log(selchannel.channels);
        // const probechannels = selprobe.channels;
        this.setState({ selcha: selchannel.channels });
        console.log(this.state.selcha);
    };

    // @ts-ignore works
    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id &&
                this.state.apparent_power_rating &&
                this.state.fuel_id &&
                this.state.fuel_store_size &&
                // this.state.window_id &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data &&
                this.state.quarter_load &&
                this.state.half_load &&
                this.state.three_quarter_load &&
                // this.state.map_id &&
                this.state.full_load
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleTimeChange = (countNumber, where, time) => {
        let timeArr = [...this.state.data];
        timeArr[countNumber][where] = time;

        this.setState({ data: timeArr }, () => {
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id &&
                this.state.apparent_power_rating &&
                this.state.fuel_id &&
                this.state.fuel_store_size &&
                // this.state.window_id &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data &&
                this.state.quarter_load &&
                this.state.half_load &&
                this.state.three_quarter_load &&
                // this.state.map_id &&
                this.state.full_load
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleExtra = () => {
        const data = this.state.data;
        let oneData = {
            start_time: "",
            stop_time: "",
            amount: ""
        };
        data.push(oneData);
        // event.preventDefault();
        this.setState({ count: this.state.count + 1 });
        console.log(this.state.count);
        console.log(this.state.data);
        console.log(this.state);
    };

    public handleClickOpen = () => {
        this.setState({ open: true, open1: true });
    };

    public handleClose = () => {
        this.setState({ open: false, open1: false });
        console.log(this.state.count);
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    // public onChange = date => this.setState({ date });
    // public onChange1 = date1 => this.setState({ date1 });

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // @ts-ignore
            gen_id: this.props.match.params.id,

            // tslint:disable
            name: this.state.names,
            signal_threshold: this.state.signal_threshold,
            configuration_id: this.state.configuration_id,
            probe_id: this.state.probe_id,
            probe_channel_id: this.state.channel_id,
            apparent_power_rating: this.state.apparent_power_rating,
            fuel_id: this.state.fuel_id,
            fuel_store_size: this.state.fuel_store_size,
            operating_window: [
                {
                    window_id: this.state.window_id,
                    start_time: this.state.old_start_timeOW,
                    stop_time: this.state.old_stop_timeOW
                }
            ],
            new_operating_window: [
                {
                    window_id: this.state.window_id,
                    start_time: this.state.start_timeOW,
                    stop_time: this.state.stop_timeOW
                }
            ],
            charge_map: [
                {
                    map_id: this.state.map_id,
                    start_time: this.state.old_start_timeCM,
                    stop_time: this.state.old_stop_timeCM,
                    amount: 0
                }
            ],
            new_charge_map: this.state.data,
            generator_map: [
                {
                    map_id: this.state.old_map_id,
                    quarter_load_price: this.state.old_quarter,
                    half_load_price: this.state.old_half,
                    three_quarter_load_price: this.state.old_three_quarter,
                    full_load_price: this.state.old_full
                }
            ],
            new_generator_map: [
                {
                    quarter_load_price: this.state.quarter_load,
                    half_load_price: this.state.half_load,
                    three_quarter_load_price: this.state.three_quarter_load,
                    full_load_price: this.state.full_load
                }
            ]
        };

        console.log(body);
        console.log("here");
        console.log(this.state.window_id);
        // @ts-ignore
        EditOnePhaseGen(body, body.gen_id)
            .then(res => {
                console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Generator not successfully edited. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public renderTimer = props => {
        return (
            <div>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-native-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Start Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["start_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "start_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Stop Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["stop_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "stop_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        className="textField"
                        type="number"
                        margin="normal"
                        variant="outlined"
                        font-size="true"
                        value={"" || this.state.data[props.name]["amount"]}
                        onChange={e =>
                            this.handleTimeChange(props.name, "amount", e.target.value)
                        }
                        autoFocus={false}
                    />
                </FormControl>
            </div>
        );
    };

    public render() {
        const Timer = this.renderTimer;

        let ExtraChargeMap = [];
        //     const Timer = (props) => {
        // return (

        times(this.state.count, i => {
            ExtraChargeMap.push(
                // @ts-ignore
                <Timer name={`${i}`} />
            );
            return ExtraChargeMap;
        });

        console.log(this.state.start_timeOW);

        const ConfigLists = this.state.listconfig.map((ConfigList, id) => (
            <MenuItem key={id} value={ConfigList.configuration_id}>
                {ConfigList.configuration_name}
            </MenuItem>
        ));

        const ProbeLists = this.state.selpro.map((ProbeList, id) => (
            <MenuItem key={id} value={ProbeList.probe_id}>
                {ProbeList.probe_name}
            </MenuItem>
        ));

        const ChannelLists = this.state.selcha.map((ChannelList, id) => (
            <MenuItem key={id} value={ChannelList.channel_id}>
                {ChannelList.channel_name}
            </MenuItem>
        ));

        const FuelLists = this.state.listfuels.map((FuelList, id) => (
            <MenuItem key={id} value={FuelList.fuel_id}>
                {FuelList.name}
            </MenuItem>
        ));

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Power Source</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Name</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.names}
                                                    onChange={this.handleChange("names")}
                                                    name="names"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Signal Threshold</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    type="number"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.signal_threshold}
                                                    onChange={this.handleChange("signal_threshold")}
                                                    name="signal_threshold"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Type</TableCell>
                                            <TableCell>Single Phase Generator</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>
                                <Divider />
                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Channel Assignment</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Config</Typography>
                                                <Select
                                                    value={this.state.configuration_id}
                                                    onChange={this.handleChange(
                                                        "configuration_id",
                                                        this.probeSelection
                                                    )}
                                                    name="configuration_id"
                                                >
                                                    {ConfigLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Probe(s)</Typography>
                                                <Select
                                                    value={this.state.probe_id}
                                                    onChange={this.handleChange(
                                                        "probe_id",
                                                        this.channelSelection
                                                    )}
                                                    name="probe_id"
                                                >
                                                    {ProbeLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Phase 1 Channel</Typography>
                                                <Select
                                                    value={this.state.channel_id}
                                                    onChange={this.handleChange("channel_id")}
                                                    name="channel_id"
                                                >
                                                    {ChannelLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Capacity</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Apparent Power [kVa]</Typography>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.apparent_power_rating}
                                                    onChange={this.handleChange("apparent_power_rating")}
                                                    name="apparent_power_rating"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">
                                                        <Typography>Fuel Type</Typography>
                                                    </li>
                                                    <li className="sourcetable">
                                                        <Select
                                                            value={this.state.fuel_id}
                                                            onChange={this.handleChange("fuel_id")}
                                                            name="fuel_id"
                                                        >
                                                            {FuelLists}
                                                        </Select>
                                                    </li>
                                                </ul>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">
                                                        <Typography>Fuel Store</Typography>
                                                    </li>
                                                    <li className="sourcetable">
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.fuel_store_size}
                                                            onChange={this.handleChange("fuel_store_size")}
                                                            name="fuel_store_size"
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Operating Window</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <ul className="viewlist">
                                                        <li className="viewlist">From:</li>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">To:</li>
                                                    </ul>
                                                </Typography>

                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Start Time"
                                                            type="time"
                                                            onChange={this.handleChange("start_timeOW")}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Stop Time"
                                                            type="time"
                                                            onChange={this.handleChange("stop_timeOW")}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Charge Map</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableRow>
                                        <Button onClick={this.handleClickOpen} autoFocus={false} style={{ backgroundColor: "#337ab7" }}>
                                            Open charge map dialog
                                        </Button>
                                        <Dialog
                                            disableBackdropClick={true}
                                            disableEscapeKeyDown={true}
                                            open={this.state.open1}
                                            onClose={this.handleClose}
                                        >
                                            <DialogTitle>Fill the form</DialogTitle>
                                            <DialogContent>
                                                <form
                                                    style={{
                                                        border: "none",
                                                        display: "flex",
                                                        flexWrap: "wrap",
                                                        height: 300,
                                                        maxWidth: 300
                                                        // minHeight: 50
                                                    }}
                                                >
                                                    {/* <Timer name="0" /> */}
                                                    {ExtraChargeMap}
                                                    {/* <Timer name={`${i}`} /> */}
                                                    <Button
                                                        onClick={this.handleExtra}
                                                        color="primary"
                                                        autoFocus={false}
                                                    >
                                                        Add More
                          </Button>
                                                </form>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button
                                                    onClick={this.handleClose}
                                                    color="primary"
                                                    autoFocus={false}
                                                >
                                                    Ok
                        </Button>
                                            </DialogActions>
                                        </Dialog>
                                    </TableRow>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Fuel Consumption Map</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="1/4 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.quarter_load}
                                                            onChange={this.handleChange("quarter_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="1/2 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.half_load}
                                                            onChange={this.handleChange("half_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="3/4 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.three_quarter_load}
                                                            onChange={this.handleChange("three_quarter_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="full load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.full_load}
                                                            onChange={this.handleChange("full_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("submit")}
                                    disabled={this.state.disabled}
                                >
                                    Submit
                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Generator successfully created
                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/source/list-source">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                      </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default withStyles(styles)(EditGen1);
