import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import withStyles from "@material-ui/core/styles/withStyles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import * as React from "react";
// import { Route, Switch } from "react-router-dom";
import AdminSidebar from "../AdminComponent/AdminSideBar/AdminSideBar";
// import UserWindow from "../UserWindow/UserWindow";
import "./Header.css";

const styles = {
  appBar: {
    backgroundColor: "#fff",
    color: "#000",
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuIcon: {
    marginLeft: "-0.5em",
    marginRight: "0.5em"
  }
};

interface Istate {
  anchorEl: null;
  open: boolean;
  user: string;
  username: string;
}

// interface IProps extends WithStyles<typeof styles> {}

class Header extends React.Component<{ classes }, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      open: false,
      user: "",
      username: ""
    };
  }

  public handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  public handleClose = () => {
    this.setState({ anchorEl: null });
  };

  public handleMenuToggle = () => this.setState({ open: !this.state.open });

  public handleClose1 = () => this.setState({ open: false });

  public User = useremail => {
    // tslint:disable
    console.log(useremail);
    this.setState({ user: useremail });
    console.log(this.state.user);
  };

  public Username = username => {
    // tslint:disable
    console.log(username);
    this.setState({ username: username });
  };

  public render() {
    // const { anchorEl } = this.state;
    // const open = Boolean(anchorEl);

    // const { classes } = this.props

    return (
      <div className="appbar">
        <div className="side-nav">
          <Drawer
            open={this.state.open}
            onClose={this.handleClose1}
            onEscapeKeyDown={this.handleClose1}
            className="side-nav"
          >
            <AdminSidebar close={this.handleClose1} />
            {/* <div>hdbdh</div> */}
          </Drawer>
        </div>

        <div>
          <AppBar position="sticky" style={{ backgroundColor: "white" }}>
            <Toolbar>
              <IconButton onClick={this.handleMenuToggle} disableRipple={false}>
                <MenuIcon
                  titleAccess="Menu"
                  nativeColor="#000"
                  classes={{ root: this.props.classes.menuIcon }}
                />
              </IconButton>

              <img src={window.location.origin + "/img/logo.png"} />

              <div style={{ marginLeft: "auto" }}>
                <IconButton
                  // aria-owns={open ? "menu-appbar" : undefined}
                  // aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <Avatar
                    src={window.location.origin + "/img/avatar.png"}
                    alt="Profile picture"
                  />
                  <div>&nbsp;</div>
                  <Typography
                    // color="primary"
                    className="grow"
                    style={{ marginLeft: "auto" }}
                  >
                    Hello,
                    <b>{` ${localStorage.getItem('user_name')}`}</b>
                  </Typography>
                </IconButton>
                {/* <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    horizontal: "right",
                    vertical: "top"
                  }}
                  transformOrigin={{
                    horizontal: "right",
                    vertical: "top"
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                  <MenuItem onClick={this.handleClose}>My account</MenuItem>
                </Menu> */}
              </div>
            </Toolbar>
          </AppBar>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Header);
