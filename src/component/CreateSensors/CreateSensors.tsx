import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateSensor } from "../../Services/SensorRequests/SensorRequests";
import "./CreateSensors.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    price: number;
    profitmargin?: number;
    rating: number;
    scaling_factor: number;
    sensor_name: string;
    size: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    disabled: boolean;
    open2: boolean;
}

class CreateSensors extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            disabled: true,
            groupadmin: "",
            name: [],
            open: true,
            open2: false,
            price: 0,
            rating: 0,
            scaling_factor: 0,
            sensor_name: "",
            size: 0,
            sourceconfig: "",
            // profitmargin: '',
            value: ""
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (this.state.sensor_name && this.state.rating) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.sensor_name,
            rating: this.state.rating

            // channels: []
        };

        // @ts-ignore
        CreateSensor(body)
            .then(res => {
                // console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err);
                alert("Sensor not created. Please try again!");
            });
    };

    public render() {
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create Sensors</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Name</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.sensor_name}
                                            onChange={this.handleChange("sensor_name")}
                                            name="sensor_name"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Scaling Factor</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            type="number"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.scaling_factor}
                                            onChange={this.handleChange("scaling_factor")}
                                            name="scaling_factor"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Max Current</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            type="number"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.rating}
                                            onChange={this.handleChange("rating")}
                                            name="rating"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Size</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            type="number"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.size}
                                            onChange={this.handleChange("size")}
                                            name="size"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Price</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            type="number"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.price}
                                            onChange={this.handleChange("price")}
                                            name="price"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Save
                                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Sensor successfully created
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/sensors/list-sensors">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateSensors;
