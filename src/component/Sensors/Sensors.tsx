import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import "react-dual-listbox/lib/react-dual-listbox.css";
import { Link } from "react-router-dom";
import { ListSensor } from "../../Services/SensorRequests/SensorRequests";
import "./Sensors.css";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};


interface Istate {
  viewsensor: any[];
}
class Sensors extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      viewsensor: [],
    };
  }

  public componentWillMount() {
    ListSensor()
      .then(res => {
        this.setState({ viewsensor: res.data.data });
        // tslint:disable
        console.log(this.state.viewsensor);
      })
      .catch(err => {
        // tslint:disable
        console.log(err.response);
      });
  }

  public CreateColumn (id) {
    return <ul className="ullist">
      <li className="ullist">
        <Link to={"/sensors/view/" + id} style={style}>
          View
        </Link>
      </li>
      <li className="ullist">
        <Link to={"/sensors/edit/" + id} style={style}>
          Edit
        </Link>
      </li>
    </ul>
  }

  public render() {

    const ListSensors = this.state.viewsensor.map((ListSensor, id) => (
      <TableRow key={id} style={{ height: 50 }}>
        <TableCell>{ListSensor.name}</TableCell>
        <TableCell>{this.CreateColumn(ListSensor.sensor_type_id)}</TableCell>
      </TableRow>
    ));

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">Sensors</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>Sensor Name</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
            {ListSensors}
          </TableBody>
        </Table>
      </div>
    );
  }

}

export default Sensors;
