import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfig } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import { CreateGroup, ListGroups, ListGroupTypes } from "../../Services/ConsumerGroupRequests/ConsumerGroupRequests";
import "./CreateConsumerGroup.css";

interface Istate {
    config?: "";
    configuration: [];
    groupname: string;
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    viewgroups: any[];
    viewgrouptypes: any[];
    grouptype: [];
    listconfig: any[];
    disabled: boolean;
    open1: boolean;
}

class CreateConsumerGroup extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            configuration: [],
            disabled: true,
            groupname: "",
            grouptype: [],
            listconfig: [],
            name: [],
            open: true,
            open1: false,
            sourceconfig: "",
            // profitmargin: '',
            value: "",
            viewgroups: [],
            viewgrouptypes: []
        };
    }

    public componentWillMount() {
        // @ts-ignore works
        const fuelId = this.props.match.params.id;

        // tslint:disable

        ListGroupTypes()
            .then(res => {
                this.setState({ viewgrouptypes: res.data.data });
                console.log(res.data.data)
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListGroups()
            .then(res => {
                this.setState({ viewgroups: res.data.data });
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListConfig()
            .then(res => {
                this.setState({ listconfig: res.data.data });
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (
                this.state.configuration &&
                this.state.groupname &&
                this.state.grouptype
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            configuration_id: this.state.configuration,
            name: this.state.groupname,
            consumer_group_type_id: this.state.grouptype
        };

        // @ts-ignore
        CreateGroup(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Consumer Group not created. Please try again!");
            });
    };

    public render() {
        // @ts-ignore works

        const Devicetypes = this.state.viewgrouptypes.map((DeviceType, id) => (
            <MenuItem key={id} value={DeviceType.consumer_group_type_id}>
                {DeviceType.name}
            </MenuItem>
        ));

        const Config = this.state.listconfig.map((ListConfig, id) => (
            <MenuItem key={id} value={ListConfig.configuration_id}>
                {ListConfig.configuration_name}
            </MenuItem>
        ));
        // const FuelTypelength = this.state.listconfig.length;
        // console.log(FuelTypelength);

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Consumer Group</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Configuration</Typography>
                                    </div>
                                    <div>
                                        <Select
                                            value={this.state.configuration}
                                            onChange={this.handleChange("configuration")}
                                            name="configuration"
                                            style={{ width: '300px' }}
                                        >
                                            {Config}
                                        </Select>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Name</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.groupname}
                                            onChange={this.handleChange("groupname")}
                                            name="groupname"
                                            style={{ width: '300px' }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Type</Typography>
                                    </div>
                                    <div>
                                        <Select
                                            value={this.state.grouptype}
                                            onChange={this.handleChange("grouptype")}
                                            name="grouptype"
                                            style={{ width: '300px' }}
                                        >
                                            {Devicetypes}
                                        </Select>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    ADD GROUP
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Consumer Group successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/consumer/list-consumer">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div >
        );
    }
}

export default CreateConsumerGroup;
