import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import {
  DeleteOnePhaseGrid,
  DeleteThreePhaseGrid,
  ListOnePhaseGrid,
  ListThreePhaseGrid
} from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./ListGrid.css";

const style = {
  color: "#337ab7",
  textDecoration: "none"
};


interface Istate {
  id: string;
  listonephasegrid: any[];
  listthreephasegrid: any[];
  open1a: boolean;
  open1b: boolean;
  open3a: boolean;
  open3b: boolean;
}

class ListGrid extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      listonephasegrid: [],
      listthreephasegrid: [],
      open1a: false,
      open1b: false,
      open3a: false,
      open3b: false
    };
  }

  public componentWillMount() {
    ListOnePhaseGrid()
      .then(res => {
        this.setState({ listonephasegrid: res.data.data });
        // tslint:disable
        console.log(this.state.listonephasegrid);
        console.log(this.state.listonephasegrid.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });

    ListThreePhaseGrid()
      .then(res => {
        this.setState({ listthreephasegrid: res.data.data });
        // tslint:disable
        console.log(this.state.listthreephasegrid);
        console.log(this.state.listthreephasegrid.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public handleSubmitGrid1 = () => {
    // event.preventDefault();

    const gridid = this.state.id;
    // @ts-ignore

    // @ts-ignore
    DeleteOnePhaseGrid(gridid)
      .then(res => {
        this.setState({ open1b: true });
        // tslint:disable
        console.log(res);
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  public handleSubmitGrid3 = () => {
    // event.preventDefault();

    const gridid = this.state.id;
    // @ts-ignore

    // @ts-ignore
    DeleteThreePhaseGrid(gridid)
      .then(res => {
        this.setState({ open3b: true });
        // tslint:disable
        console.log(res);
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  handleClickOpen1 = id => {
    console.log(id);
    this.setState({ open1a: true, id: id });
  };

  handleClickOpen3 = id => {
    console.log(id);
    this.setState({ open3a: true, id: id });
  };

  handleClose1 = () => {
    this.setState({ open1a: false, open1b: false });
  };

  handleClose3 = () => {
    this.setState({ open3a: false, open3b: false });
  };

  public CreateColumn1(id) {
    return (
      <ul className="ullist">
        <li className="ullist">
          <Link to={"/source/view-grid1/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              View
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Link to={"/source/edit-grid1/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              Edit
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Button
            variant="outlined"
            color="primary"
            onClick={this.handleClickOpen1.bind(this, id)}
            style={{ border: "none", margin: 0 }}
          >
            Delete
          </Button>
          <Dialog
            open={this.state.open1a}
            onClose={this.handleClose1}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are You Sure You Want To Delete This?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose1} color="primary">
                No
              </Button>
              <Button
                onClick={this.handleSubmitGrid1.bind(this, id)}
                color="primary"
                autoFocus
              >
                Yes
              </Button>
              <Dialog
                open={this.state.open1b}
                onClose={this.handleClose1}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Deleted
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose1} color="primary">
                    Ok
                  </Button>
                </DialogActions>
              </Dialog>
            </DialogActions>
          </Dialog>
        </li>
      </ul>
    );
  }

  public CreateColumn3(id) {
    return (
      <ul className="ullist">
        <li className="ullist">
          <Link to={"/source/view-grid3/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              View
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Link to={"/source/edit-grid3/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              Edit
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Button
            variant="outlined"
            color="primary"
            onClick={this.handleClickOpen3.bind(this, id)}
            style={{ border: "none", margin: 0 }}
          >
            Delete
          </Button>
          <Dialog
            open={this.state.open3a}
            onClose={this.handleClose3}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are You Sure You Want To Delete This?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose3} color="primary">
                No
              </Button>
              <Button
                onClick={this.handleSubmitGrid3.bind(this, id)}
                color="primary"
                autoFocus
              >
                Yes
              </Button>
              <Dialog
                open={this.state.open3b}
                onClose={this.handleClose3}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Deleted
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose3} color="primary">
                    Ok
                  </Button>
                </DialogActions>
              </Dialog>
            </DialogActions>
          </Dialog>
        </li>
      </ul>
    );
  }

  public render() {
    const SinglePhaseGrid = this.state.listonephasegrid.map((ListFuel, id) => (
      <TableRow key={id} style={{ height: 50 }}>
        <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.name}</TableCell>
        <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.source_type}</TableCell>
        <TableCell style={{minWidth:100, maxWidth:100}} />
        <TableCell>{this.CreateColumn1(ListFuel.meter_id)}</TableCell>
      </TableRow>
    ));
    const SinglePhaseGridlength = this.state.listonephasegrid.length;
    console.log(SinglePhaseGridlength);

    const ThreePhaseGrid = this.state.listthreephasegrid.map((ListFuel, id) => (
      <TableRow key={id} style={{ height: 50 }}>
        <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.name}</TableCell>
        <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.source_type}</TableCell>
        <TableCell style={{minWidth:100, maxWidth:100}} />
        <TableCell>{this.CreateColumn3(ListFuel.meter_id)}</TableCell>
      </TableRow>
    ));
    const ThreePhaseGridlength = this.state.listthreephasegrid.length;
    console.log(ThreePhaseGridlength);

    return (
      <div>
        <TableBody>
          <TableRow className="tablerow">
            <TableCell>Name</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>phases</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
          {SinglePhaseGrid}
          {ThreePhaseGrid}
        </TableBody>
      </div>
    );
  }
}

export default ListGrid;
