import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateFuels } from "../../Services/FuelRequests/FuelRequests";
import "./EditRole.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    permissions: string;
    rolename: string;
    disabled: boolean;
    open2: boolean;
}

class EditRole extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            disabled: true,
            groupadmin: "",
            name: [],
            open: true,
            open2: false,
            permissions: "",
            rolename: "",
            sourceconfig: "",
            // profitmargin: '',
            value: ""
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (this.state.rolename && this.state.permissions) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    // @ts-ignore works
    public handleOpen = event => {
        // this.setState({ open: true });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.rolename,
            price: this.state.permissions
        };

        console.log(body);
        console.log("here");
        // @ts-ignore
        CreateFuels(body)
            .then(res => {
                console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Role not successfully edited. Please try again!");
            });
    };

    public render() {
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Role</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Role Name</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.rolename}
                                    onChange={this.handleChange("rolename")}
                                    name="rolename"
                                />
                            </TableCell>
                            <TableCell>
                                <Typography>Permissions</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.permissions}
                                    onChange={this.handleChange("permissions")}
                                    name="permissions"
                                />
                                <div>&nbsp;</div>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    add pm
                </Button>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Save
                </Button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Cancel
                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Role successfully created
                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/usergroup/list-users">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                      </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default EditRole;
