// import ListBox from 'react-listbox';
import {
    default as Table,
    default as TableBody
  } from "@material-ui/core/Table";
  import TableCell from "@material-ui/core/TableCell";
  import TableHead from "@material-ui/core/TableHead";
  import TableRow from "@material-ui/core/TableRow";
  import * as React from "react";
  import "react-dual-listbox/lib/react-dual-listbox.css";
  import "./Products.css";
  
  class Product extends React.Component {
    // @ts-ignore works
    constructor(props) {
      super(props);
    }
  
    public render() {
      return (
        <div>
          <Table className="table" style={{ overflowX: "auto", width: 900 }}>
            <TableHead className="head">
              <TableRow className="tablecell">
                <TableCell className="tablecell">
                  <span id="tablecell">Products</span>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow className="tablerow">
                <TableCell>Product Name</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
      );
    }
  
  }
  
  export default Product;
  