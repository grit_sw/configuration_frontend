import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateFuels } from "../../Services/FuelRequests/FuelRequests";
import "./CreateFuel.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    unitprice: number;
    fueltype: string;
    disabled: boolean;
    open1: boolean;
}

class CreateFuel extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            disabled: true,
            fueltype: "",
            groupadmin: "",
            name: [],
            open: true,
            open1: false,
            sourceconfig: "",
            // profitmargin: '',
            unitprice: 0,
            value: ""
        };
    }

    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (this.state.fueltype && this.state.unitprice) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.fueltype,
            unit_price: this.state.unitprice
        };

        // @ts-ignore
        CreateFuels(body)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Fuel not created. Please try again!");
            });
    };

    public render() {
        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Fuel Type</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <Typography>Fuel Type:</Typography>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.fueltype}
                                    onChange={this.handleChange("fueltype")}
                                    name="fueltype"
                                />
                            </TableCell>
                            <TableCell>
                                <Typography>Unit Price:</Typography>
                                <TextField
                                    className="textField"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.unitprice}
                                    onChange={this.handleChange("unitprice")}
                                    name="unitprice"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Save Device
                                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Fuel successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/fuel/list-fuel">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateFuel;
