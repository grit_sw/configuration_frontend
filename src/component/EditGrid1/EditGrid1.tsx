import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfig } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import {
    EditOnePhaseGrid,
    ViewOnePhaseGrid
} from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./EditGrid1.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    signal_threshold: number;
    sourceconfig?: "";
    value: "";
    name: [];
    names: string;
    configs: boolean;
    listconfig: any[];
    probechannels: any[];
    configuration_id: string;
    selcha: any[];
    selpro: any[];
    probe_id: string;
    channel_id: string;
    date: string;
    start_timeOW: string;
    stop_timeOW: string;
    viewonephasegrid: {};
    map_id: string;
    old_start_timeCM: string;
    old_stop_timeCM: string;
    data: any[];
    count: number;
    open1: boolean;
    disabled: boolean;
    open2: boolean;
}

const styles = theme => ({
    heading: {
        flexBasis: "33.33%",
        flexShrink: 0,
        fontSize: theme.typography.pxToRem(15)
    },

    root: {
        width: "100%"
    },

    secondaryHeading: {
        color: theme.palette.text.secondary,
        fontSize: theme.typography.pxToRem(15)
    }
});

class EditGrid1 extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            channel_id: "",
            configs: true,
            configuration_id: "",
            count: 1,
            data: [{}],
            date: "",
            disabled: true,
            groupadmin: "",
            listconfig: [],
            map_id: "",
            name: [],
            names: "",
            old_start_timeCM: "",
            old_stop_timeCM: "",
            open: true,
            open1: false,
            open2: false,
            probe_id: "",
            probechannels: [],
            selcha: [],
            selpro: [],
            signal_threshold: 0,
            sourceconfig: "",
            start_timeOW: "",
            stop_timeOW: "",
            // profitmargin: '',
            value: "",
            viewonephasegrid: {}
        };
    }

    public componentWillMount() {
        ListConfig()
            .then(res => {
                this.setState({
                    listconfig: res.data.data
                });
                // tslint:disable
                console.log(this.state.listconfig);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        // @ts-ignore
        const gridId = this.props.match.params.id;

        ViewOnePhaseGrid(gridId)
            .then(res => {
                this.setState({
                    viewonephasegrid: res.data.data,
                    map_id: res.data.data.charge_map[0].map_id,
                    old_start_timeCM: res.data.data.charge_map[0].start_time,
                    old_stop_timeCM: res.data.data.charge_map[0].stop_time
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(this.state.viewonephasegrid);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public probeSelection = () => {
        let configId = this.state.configuration_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        console.log(configId);
        const selprobe = this.state.listconfig.find(
            probe => probe.configuration_id === configId
        );
        console.log(selprobe.probe_channels);
        // const probechannels = selprobe.channels;
        this.setState({ selpro: selprobe.probe_channels });
        console.log(this.state.selpro);
    };

    public channelSelection = () => {
        let probeId = this.state.probe_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        console.log(probeId);
        const selchannel = this.state.selpro.find(
            probe => probe.probe_id === probeId
        );
        console.log(selchannel.channels);
        // const probechannels = selprobe.channels;
        this.setState({ selcha: selchannel.channels });
        console.log(this.state.selcha);
    };

    // @ts-ignore works
    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public onChange = date =>
        this.setState({ date }, () => {
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data
            ) {
                this.setState({ disabled: false });
            }
        });

    public handleTimeChange = (countNumber, where, time) => {
        let timeArr = [...this.state.data];
        timeArr[countNumber][where] = time;

        this.setState({ data: timeArr }, () => {
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleExtra = () => {
        const data = this.state.data;
        let oneData = {
            start_time: "",
            stop_time: "",
            amount: ""
        };
        data.push(oneData);
        // event.preventDefault();
        this.setState({ count: this.state.count + 1 });
        console.log(this.state.count);
        console.log(this.state.data);
        console.log(this.state);
    };

    public handleClickOpen = () => {
        this.setState({ open: true, open1: true });
    };

    public handleClose = () => {
        this.setState({ open: false, open1: false });
        console.log(this.state.count);
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // @ts-ignore
            grid_id: this.props.match.params.id,

            // tslint:disable
            name: this.state.names,
            signal_threshold: this.state.signal_threshold,
            configuration_id: this.state.configuration_id,
            probe_id: this.state.probe_id,
            probe_channel_id: this.state.channel_id,
            charge_map: [
                {
                    map_id: this.state.map_id,
                    start_time: this.state.old_start_timeCM,
                    stop_time: this.state.old_stop_timeCM,
                    amount: 0
                }
            ],
            new_charge_map: [
                {
                    start_time: this.state.start_timeOW,
                    stop_time: this.state.stop_timeOW,
                    amount: 0
                }
            ]
            // channels: []
        };

        console.log(body);
        console.log("here");
        // @ts-ignore
        EditOnePhaseGrid(body, body.grid_id)
            .then(res => {
                console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Grid not successfully edited. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public renderTimer = props => {
        return (
            <div>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-native-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Start Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["start_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "start_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Stop Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["stop_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "stop_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        className="textField"
                        type="number"
                        margin="normal"
                        variant="outlined"
                        font-size="true"
                        value={"" || this.state.data[props.name]["amount"]}
                        onChange={e =>
                            this.handleTimeChange(props.name, "amount", e.target.value)
                        }
                        autoFocus={false}
                    />
                </FormControl>
            </div>
        );
    };

    public render() {
        const Timer = this.renderTimer;

        let ExtraChargeMap = [];
        //     const Timer = (props) => {
        // return (

        times(this.state.count, i => {
            ExtraChargeMap.push(
                // @ts-ignore
                <Timer name={`${i}`} />
            );
            return ExtraChargeMap;
        });

        const ConfigLists = this.state.listconfig.map((ConfigList, id) => (
            <MenuItem key={id} value={ConfigList.configuration_id}>
                {ConfigList.configuration_name}
            </MenuItem>
        ));

        const ProbeLists = this.state.selpro.map((ProbeList, id) => (
            <MenuItem key={id} value={ProbeList.probe_id}>
                {ProbeList.probe_name}
            </MenuItem>
        ));

        const ChannelLists = this.state.selcha.map((ChannelList, id) => (
            <MenuItem key={id} value={ChannelList.channel_id}>
                {ChannelList.channel_name}
            </MenuItem>
        ));

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Power Source</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Name</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.names}
                                                    onChange={this.handleChange("names")}
                                                    name="names"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Signal Threshold</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    type="number"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.signal_threshold}
                                                    onChange={this.handleChange("signal_threshold")}
                                                    name="signal_threshold"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Type</TableCell>
                                            <TableCell>Single Phase Grid</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>
                                <Divider />
                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableHead className="head">
                                        <TableRow className="tablecell">
                                            <TableCell className="tablecell">
                                                <span id="tablecell">Channel Assignment</span>
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Config</Typography>
                                                <Select
                                                    value={this.state.configuration_id}
                                                    onChange={this.handleChange(
                                                        "configuration_id",
                                                        this.probeSelection
                                                    )}
                                                    name="configuration_id"
                                                >
                                                    {ConfigLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Probe(s)</Typography>
                                                <Select
                                                    value={this.state.probe_id}
                                                    onChange={this.handleChange(
                                                        "probe_id",
                                                        this.channelSelection
                                                    )}
                                                    name="probe_id"
                                                >
                                                    {ProbeLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Phase 1 Channel</Typography>
                                                <Select
                                                    value={this.state.channel_id}
                                                    onChange={this.handleChange("channel_id")}
                                                    name="channel_id"
                                                >
                                                    {ChannelLists}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Operating Window</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <ul className="viewlist">
                                                        <li className="viewlist">From:</li>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <li className="viewlist">To:</li>
                                                    </ul>
                                                </Typography>

                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Start Time"
                                                            type="time"
                                                            onChange={this.handleChange("start_timeOW")}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Stop Time"
                                                            type="time"
                                                            onChange={this.handleChange("stop_timeOW")}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Charge Map</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableRow>
                                        <Button onClick={this.handleClickOpen} autoFocus={false} style={{ backgroundColor: "#337ab7" }}>
                                            Open charge map dialog
                                        </Button>
                                        <Dialog
                                            disableBackdropClick={true}
                                            disableEscapeKeyDown={true}
                                            open={this.state.open1}
                                            onClose={this.handleClose}
                                        >
                                            <DialogTitle>Fill the form</DialogTitle>
                                            <DialogContent>
                                                <form
                                                    style={{
                                                        border: "none",
                                                        display: "flex",
                                                        flexWrap: "wrap",
                                                        height: 300,
                                                        maxWidth: 300
                                                        // minHeight: 50
                                                    }}
                                                >
                                                    {/* <Timer name="0" /> */}
                                                    {ExtraChargeMap}
                                                    {/* <Timer name={`${i}`} /> */}
                                                    <Button
                                                        onClick={this.handleExtra}
                                                        color="primary"
                                                        autoFocus={false}
                                                    >
                                                        Add More
                          </Button>
                                                </form>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button
                                                    onClick={this.handleClose}
                                                    color="primary"
                                                    autoFocus={false}
                                                >
                                                    Ok
                        </Button>
                                            </DialogActions>
                                        </Dialog>
                                    </TableRow>
                                </Table>

                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("submit")}
                                    disabled={this.state.disabled}
                                >
                                    Submit
                                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Grid successfully created
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/source/list-source">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default withStyles(styles)(EditGrid1);
