import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import * as React from "react";
import { ListConfigurations, ListUserPowerSources } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import { UserLine } from "../../Services/ConsumerLineRequests/ConsumerLineRequests";
import { ViewUserFuels } from "../../Services/FuelRequests/FuelRequests";
import { ViewUserProbes } from "../../Services/ProbeRequests/ProbeRequests";
// import { ListRole2, ListRole3, ListRole4, ListRole5, ListUser } from "../../Services/UsersRequests/UsersRequests";
import { ConfigUsers } from "../../Services/UsersRequests/UsersRequests";
// import { Route, Switch } from 'react-router-dom';
import AllSettings from "../AllSettings/AllSettings";
import "./SelectUser.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    selected?: "";
    listrole2: any[];
    listrole3: any[];
    listrole4: any[];
    listrole5: any[];
    listusers: any[];
    user: string;
    viewuserprobes: string
    display: boolean
    display1: boolean
    display2: boolean
    display3: boolean
    display4: boolean
    error: string
    listconfiguration: string
    viewuserfuel: string
    viewuserpowersources: string
    userline: any[]
}

interface Iprops {
    storeUser;
    storeUsername;
    storeUserProbe
    storeDisplay
    storeUserConfiguration
    storeDisplay1
    storeUserFuel
    storeDisplay2
    storeUserPowerSource
    storeDisplay3
    storeUserLine
    storeDisplay4
}
class SelectUser extends React.Component<Iprops, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            display: false,
            display1: false,
            display2: false,
            display3: false,
            display4: false,
            error: "",
            groupadmin: "",
            listconfiguration: "",
            listrole2: [],
            listrole3: [],
            listrole4: [],
            listrole5: [],
            listusers: [],
            name: [],
            open: true,
            sourceconfig: "",
            // profitmargin: '',
            user: "",
            userline: [],
            value: "",
            viewuserfuel: "",
            viewuserpowersources: "",
            viewuserprobes: ""
        };
    }

    public componentWillMount() {
        // ListRole2()
        //     .then(res => {
        //         this.setState({
        //             listrole2: res.data.data
        //             // probechannels: res.data.data.probe_channels
        //         });
        //         // tslint:disable
        //         console.log(this.state.listrole2);
        //         // console.log(this.state.probechannels);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         console.log(err);
        //     });

        // ListRole3()
        //     .then(res => {
        //         this.setState({
        //             listrole3: res.data.data
        //             // probechannels: res.data.data.probe_channels
        //         });
        //         // tslint:disable
        //         console.log(this.state.listrole3);
        //         // console.log(this.state.probechannels);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         console.log(err);
        //     });

        // ListRole4()
        //     .then(res => {
        //         this.setState({
        //             listrole4: res.data.data
        //             // probechannels: res.data.data.probe_channels
        //         });
        //         // tslint:disable
        //         console.log(this.state.listrole4);
        //         // console.log(this.state.probechannels);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         console.log(err);
        //     });

        // ListRole5()
        //     .then(res => {
        //         this.setState({
        //             listrole5: res.data.data
        //             // probechannels: res.data.data.probe_channels
        //         });
        //         // tslint:disable
        //         console.log(this.state.listrole5);
        //         // console.log(this.state.probechannels);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         console.log(err);
        //     });

        // ListUser()
        //     .then(res => {
        //         this.setState({
        //             listusers: res.data.data
        //             // probechannels: res.data.data.probe_channels
        //         });
        //         // tslint:disable
        //         console.log(this.state.listusers);
        //         // console.log(this.state.probechannels);
        //     })
        //     .catch(err => {
        //         // tslint:disable
        //         console.log(err);
        //     });

        ConfigUsers()
            .then(res => {
                this.setState({
                    listrole2: res.data.data
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(this.state.listrole2);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public UsersData = () => {
        let userdetail = this.state.config;

        // @ts-ignore
        const [id, name] = userdetail.split("*");
        console.log(id);
        console.log(name);
        let user = id;
        let username = name;
        this.props.storeUser(user);
        this.props.storeUsername(username);

        ViewUserProbes(user)
            .then(res => {
                console.log(res)
                this.setState({ viewuserprobes: res.data.data, display: true }, () => {
                    let userprobes = this.state.viewuserprobes
                    let display = this.state.display

                    this.props.storeUserProbe(userprobes)
                    this.props.storeDisplay(display)
                });
                // tslint:disable
                console.log(this.state.viewuserprobes);
                console.log(this.state.viewuserprobes.length);
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display: false });
                console.log(this.state.error);
            });

        ListConfigurations(user)
            .then(res => {
                this.setState({ listconfiguration: res.data.data, display1: true }, () => {
                    let userconfigurations = this.state.listconfiguration
                    let display1 = this.state.display1

                    this.props.storeUserConfiguration(userconfigurations)
                    this.props.storeDisplay1(display1)
                });
                // tslint:disable
                console.log(this.state.listconfiguration);
                console.log(this.state.listconfiguration.length);
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display1: false });
                console.log(this.state.error);
            });

        ViewUserFuels(user)
            .then(res => {
                this.setState({ viewuserfuel: res.data.data, display2: true });
                // tslint:disable
                // console.log(this.state.viewuserfuel);
                // console.log(this.state.viewuserfuel.length);

                let userfuels = this.state.viewuserfuel
                let display2 = this.state.display2

                this.props.storeUserConfiguration(userfuels)
                this.props.storeDisplay2(display2)
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display2: false });
                console.log(this.state.error);
            });


        ListUserPowerSources(user)
            .then(res => {
                this.setState({ viewuserpowersources: res.data, display3: true });
                // tslint:disable
                // console.log(this.state.viewuserfuel);
                // console.log(this.state.viewuserfuel.length);

                let userpowersources = this.state.viewuserpowersources
                let display3 = this.state.display3

                this.props.storeUserPowerSource(userpowersources)
                this.props.storeDisplay3(display3)
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display3: false });
                console.log(this.state.error);
            });

        UserLine(user)
            .then(res => {
                this.setState({ userline: res.data.data, display4: true });
                // tslint:disable
                console.log(res.data.data);
                console.log(this.state.userline.length);

                let userline = this.state.userline
                let display4 = this.state.display4

                this.props.storeUserLine(userline)
                this.props.storeDisplay4(display4)
            })
            .catch(err => {
                // tslint:disable
                this.setState({ error: err.response, display4: false });
                console.log(err);
            });

        // let userprobes = this.state.viewuserprobes
        // let display = this.state.display

        // this.props.storeUserProbe(userprobes)
        // this.props.storeDisplay(display) 
    };

    public render() {
        console.log(this.state.user);

        const Role2Users = this.state.listrole2.map((Role2User, id) => (
            <MenuItem key={id} value={`${Role2User.user_id}*${Role2User.full_name}`}>
                {Role2User.email}
            </MenuItem>
        ));

        const Role3Users = this.state.listrole3.map((Role3User, id) => (
            <MenuItem key={id} value={`${Role3User.user_id}*${Role3User.full_name}`}>
                {Role3User.email}
            </MenuItem>
        ));

        const Role4Users = this.state.listrole4.map((Role4User, id) => (
            <MenuItem key={id} value={`${Role4User.user_id}*${Role4User.full_name}`}>
                {Role4User.email}
            </MenuItem>
        ));

        const Role5Users = this.state.listrole5.map((Role5User, id) => (
            <MenuItem key={id} value={`${Role5User.user_id}*${Role5User.full_name}`}>
                {Role5User.email}
            </MenuItem>
        ));
        const user = (
            <div className="user">
                <div>Select User:</div>
                <Select
                    // open={this.state.configs}
                    // onClose={this.handleClose}
                    onOpen={this.handleOpen("config")}
                    value={this.state.config}
                    onChange={this.handleChange("config", this.UsersData)}
                    name="config"
                >
                    {Role2Users}
                    {Role3Users}
                    {Role4Users}
                    {Role5Users}
                </Select>
                <AllSettings />
            </div>
        );

        return <div>{user}</div>;
    }
}

export default SelectUser;
