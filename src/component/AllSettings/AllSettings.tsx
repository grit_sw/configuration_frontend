import * as React from "react";
import { Route, Switch } from "react-router-dom";
import ConfigurationSettings from "../ConfigurationSettings/ConfigurationSettings";
import FuelSettings from "../FuelSettings/FuelSettings";
import PowerConsumersSettings from "../PowerConsumersSettings/PowerConsumersSettings";
import PowerSourcesSettings from "../PowerSourcesSettings/PowerSourcesSettings";
import ProbeSettings from "../ProbeSettings/ProbeSettings";
import ProductSettings from "../ProductSettings/ProductSettings";
import UsersSettings from "../UsersSettings/UsersSettings";
import "./AllSettings.css";


class AllSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {

    return (
      <div className="usersettings">
        <Switch>
          <Route exact={true} path="/" component={ConfigurationSettings} />
          <Route path="/configuration" component={ConfigurationSettings} />
          <Route path="/usergroup" component={UsersSettings} />
          <Route path="/probe" component={ProbeSettings} />
          <Route path="/source" component={PowerSourcesSettings} />
          <Route path="/consumer" component={PowerConsumersSettings} />
          <Route path="/products" component={ProductSettings} />
          <Route path="/sensors" component={ProductSettings} />
          <Route path="/devices" component={ProductSettings} />
          <Route path="/products" component={ProductSettings} />
          <Route path="/fuel" component={FuelSettings} />
        </Switch>
      </div>
    );
  }
}

export default AllSettings;
