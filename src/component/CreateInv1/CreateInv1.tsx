import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfig, ListConfigurations } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import { CreateOnePhaseInverter } from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./CreateInv1.css";

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    listconfig: any[];
    configuration_id: string;
    selpro: any[];
    probe_id: any[];
    selcha: any[];
    names: string;
    signal_threshold: number;
    channel_id1: string;
    channel_id2: string;
    rating: number;
    efficiency: number;
    bulk_charging_current: number;
    cost: number;
    run_time: number;
    battery_capacity: number;
    battery_capacity1: number;
    battery_capacity2: number;
    battery_capacity3: number;
    battery_capacity4: number;
    battery_count: number;
    battery_voltage: number;
    battery_bank_voltage: number;
    depth_of_discharge: number;
    battery_chemistry: string;
    battery_end_of_life: number;
    dod1: number;
    dod2: number;
    dod3: number;
    dod4: number;
    number_of_cycles1: number;
    number_of_cycles2: number;
    number_of_cycles3: number;
    number_of_cycles4: number;
    start_timeOW: string;
    stop_timeOW: string;
    disabled: boolean;
    open2: boolean;
    open3: boolean
    listconfiguration: any[]
    selchavailable: any[]
    count1: number;
    userprobechannels: any[]
}

class CreateInv1 extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            battery_bank_voltage: 0,
            battery_capacity: 0,
            battery_capacity1: 0,
            battery_capacity2: 0,
            battery_capacity3: 0,
            battery_capacity4: 0,
            battery_chemistry: "",
            battery_count: 0,
            battery_end_of_life: 0,
            battery_voltage: 0,
            bulk_charging_current: 0,
            channel_id1: "",
            channel_id2: "",
            configs: true,
            configuration_id: "",
            cost: 0,
            count1: 0,
            depth_of_discharge: 0,
            disabled: true,
            dod1: 0,
            dod2: 0,
            dod3: 0,
            dod4: 0,
            efficiency: 0,
            groupadmin: "",
            listconfig: [],
            listconfiguration: [],
            name: [],
            names: "",
            number_of_cycles1: 0,
            number_of_cycles2: 0,
            number_of_cycles3: 0,
            number_of_cycles4: 0,
            open: true,
            open2: false,
            open3: false,
            probe_id: [],
            rating: 0,
            run_time: 0,
            selcha: [],
            selchavailable: [],
            selpro: [],
            signal_threshold: 0,
            sourceconfig: "",
            start_timeOW: "",
            stop_timeOW: "",
            // profitmargin: '',
            userprobechannels: [],
            value: ""
        };
    }

    public componentWillMount() {
        ListConfig()
            .then(res => {
                this.setState({
                    listconfig: res.data.data
                });
                // tslint:disable
                // console.log(this.state.listconfig);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        const userId = localStorage.getItem("user_id");

        // @ts-ignore
        ListConfigurations(userId)
            .then(res => {
                this.setState({
                    listconfiguration: res.data.data,
                    userprobechannels: res.data.data.probe_channels
                });
                // tslint:disable
                // console.log(this.state.listconfig);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public probeSelection = () => {
        let configId = this.state.configuration_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        const selprobe = this.state.listconfiguration.find(
            probe => probe.configuration_id === configId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selpro: selprobe.probe_channels }, () => {
            console.log(this.state.selpro)
            // let available: any[] = []
            // if (count > 2) {
            //     available.push(this.state.selpro[i])
            //     console.log(available)
            //     return available
            // }
            let available1: any[] = []
            for (let i = 0; i < this.state.selpro.length; i++) {
                // let count = 0
                let current = this.state.selpro[i]
                let available: any[] = []
                for (let j = 0; j < current.channels.length; j++) {
                    if (current.channels[j].assigned_to_source === false) {
                        // let count = this.state.count1
                        // console.log(count);
                        available.push(this.state.selpro[i].channels[j])
                        console.log(available)
                        // this.setState({ count1: count++ }, () => {
                        //     console.log(this.state.selpro)
                        //     console.log(this.state.count1)
                        //     // let available: any[] = []
                        //     // let channel = this.state.selpro[i]
                        // })
                        // count++;
                    }
                    else {
                        console.log(this.state.selpro)
                    }
                    this.setState({ selchavailable: available })
                }
                if (available.length > 1) {
                    available1.push(this.state.selpro[i])
                    console.log(available1)
                }
                else {
                    this.setState({
                        selpro: [],
                        open3: true
                    })
                }
            }
        });
    };

    public channelSelection = () => {
        let probeId = this.state.probe_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        const selchannel = this.state.selchavailable.find(
            probe => probe.probe_id === probeId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selcha: selchannel.channels });
    };

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.channel_id1 &&
                this.state.channel_id2 &&
                this.state.rating &&
                this.state.efficiency &&
                this.state.bulk_charging_current &&
                this.state.cost &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.battery_capacity &&
                this.state.battery_count &&
                this.state.battery_voltage &&
                this.state.battery_bank_voltage &&
                this.state.depth_of_discharge &&
                this.state.battery_chemistry &&
                this.state.battery_end_of_life &&
                this.state.dod1 &&
                this.state.number_of_cycles1 &&
                this.state.battery_capacity1 &&
                this.state.dod2 &&
                this.state.number_of_cycles2 &&
                this.state.battery_capacity2 &&
                this.state.dod3 &&
                this.state.number_of_cycles3 &&
                this.state.battery_capacity3 &&
                this.state.dod4 &&
                this.state.number_of_cycles4 &&
                this.state.battery_capacity4
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    public handleClose3 = () => {
        this.setState({ open3: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.names,
            signal_threshold: this.state.signal_threshold,
            configuration_id: this.state.configuration_id,
            input_channel_id: this.state.channel_id1,
            output_channel_id: this.state.channel_id2,
            apparent_power_rating: this.state.rating,
            efficiency: this.state.efficiency,
            bulk_charging_current: this.state.bulk_charging_current,
            cost: this.state.cost,
            operating_window: [
                {
                    start_time: this.state.start_timeOW,
                    stop_time: this.state.stop_timeOW,
                    amount: 0
                }
            ],
            battery: [
                {
                    capacity: this.state.battery_capacity,
                    count: this.state.battery_count,
                    voltage: this.state.battery_voltage,
                    bank_voltage: this.state.battery_bank_voltage,
                    depth_of_discharge: this.state.depth_of_discharge,
                    chemistry: this.state.battery_chemistry,
                    end_of_life: this.state.battery_end_of_life,
                    meta: [
                        {
                            depth_of_discharge: this.state.dod1,
                            number_of_cycles: this.state.number_of_cycles1,
                            battery_capacity: this.state.battery_capacity1
                        },
                        {
                            depth_of_discharge: this.state.dod2,
                            number_of_cycles: this.state.number_of_cycles2,
                            battery_capacity: this.state.battery_capacity2
                        },
                        {
                            depth_of_discharge: this.state.dod3,
                            number_of_cycles: this.state.number_of_cycles3,
                            battery_capacity: this.state.battery_capacity3
                        },
                        {
                            depth_of_discharge: this.state.dod4,
                            number_of_cycles: this.state.number_of_cycles4,
                            battery_capacity: this.state.battery_capacity4
                        }
                    ]
                }
            ]
            // channels: []
        };

        // @ts-ignore
        CreateOnePhaseInverter(body)
            .then(res => {
                // console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Inverter not created. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public render() {
        const ConfigLists = this.state.listconfiguration.map((ConfigList, id) => (
            <MenuItem key={id} value={ConfigList.configuration_id}>
                {ConfigList.configuration_name}
            </MenuItem>
        ));

        const ProbeLists = this.state.selpro.map((ProbeList, id) => (
            <MenuItem key={id} value={ProbeList.probe_id}>
                {ProbeList.probe_name}
            </MenuItem>
        ));

        const ChannelLists = this.state.selchavailable.map((ChannelList, id) => (
            <MenuItem key={id} value={ChannelList.channel_id}>
                {ChannelList.channel_name}
            </MenuItem>
        ));

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Power Source</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Name</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.names}
                                                    onChange={this.handleChange("names")}
                                                    name="names"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Signal Threshold</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    type="number"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.signal_threshold}
                                                    onChange={this.handleChange("signal_threshold")}
                                                    name="signal_threshold"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Type</TableCell>
                                            <TableCell>Single Phase Inverter</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>
                                <Divider />
                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Channel Assignment</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Config</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.configuration_id}
                                                            onChange={this.handleChange(
                                                                "configuration_id",
                                                                this.probeSelection
                                                            )}
                                                            name="configuration_id"
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ConfigLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Probe(s)</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.probe_id}
                                                            onChange={this.handleChange(
                                                                "probe_id",
                                                                this.channelSelection
                                                            )}
                                                            name="probe_id"
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ProbeLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                <Typography>Channel</Typography>
                                                <div>&nbsp; &nbsp;</div>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">Output</li>
                                                    <li className="sourcetable">
                                                        <Select
                                                            value={this.state.channel_id1}
                                                            onChange={this.handleChange("channel_id1")}
                                                            name="channel_id1"
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ChannelLists}
                                                        </Select>
                                                    </li>
                                                </ul>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">Input</li>
                                                    &nbsp; &nbsp;
                                                    <li className="sourcetable">
                                                        <Select
                                                            value={this.state.channel_id2}
                                                            onChange={this.handleChange("channel_id2")}
                                                            name="channel_id2"
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ChannelLists}
                                                        </Select>
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Capacity</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Rating [kVa]</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.rating}
                                                            onChange={this.handleChange("rating")}
                                                            name="rating"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Efficiency</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.efficiency}
                                                            onChange={this.handleChange("efficiency")}
                                                            name="efficiency"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Bulk Charging Current</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.bulk_charging_current}
                                                            onChange={this.handleChange(
                                                                "bulk_charging_current"
                                                            )}
                                                            name="bulk_charging_current"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Cost</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.cost}
                                                            onChange={this.handleChange("cost")}
                                                            name="cost"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Operating Window</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <ul className="viewlist">
                                                        <li className="viewlist">From:</li>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <li className="viewlist">To:</li>
                                                    </ul>
                                                </Typography>

                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Start Time"
                                                            type="time"
                                                            onChange={this.handleChange("start_timeOW")}
                                                        />
                                                    </li>
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    &nbsp; &nbsp; &nbsp;
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Stop Time"
                                                            type="time"
                                                            onChange={this.handleChange("stop_timeOW")}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Expected Run Time</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Run Time [Hours]</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.run_time}
                                                            onChange={this.handleChange("run_time")}
                                                            name="run_time"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Storage</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery Capacity</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.battery_capacity}
                                                            onChange={this.handleChange("battery_capacity")}
                                                            name="battery_capacity"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery Count</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.battery_count}
                                                            onChange={this.handleChange("battery_count")}
                                                            name="battery_count"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery Voltage [V]</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.battery_voltage}
                                                            onChange={this.handleChange("battery_voltage")}
                                                            name="battery_voltage"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery Bank Voltage [V]</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.battery_bank_voltage}
                                                            onChange={this.handleChange(
                                                                "battery_bank_voltage"
                                                            )}
                                                            name="battery_bank_voltage"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Depth of Discharge</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.depth_of_discharge}
                                                            onChange={this.handleChange("depth_of_discharge")}
                                                            name="depth_of_discharge"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery Chemistry</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.battery_chemistry}
                                                            onChange={this.handleChange("battery_chemistry")}
                                                            name="battery_chemistry"
                                                            style={{ width: '300px' }}
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            <MenuItem value="10">ten</MenuItem>
                                                            <MenuItem>ten</MenuItem>
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Battery End of Life</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.battery_end_of_life}
                                                            onChange={this.handleChange(
                                                                "battery_end_of_life"
                                                            )}
                                                            name="battery_end_of_life"
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Table style={{ margin: 10, width: 680 }}>
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell>Battery Life</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        <TableRow className="tablerow">
                                                            <TableCell>
                                                                <Typography>
                                                                    <ul className="viewlist">
                                                                        <li className="viewlist">DoD (%)</li>
                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                        &nbsp; &nbsp;
                                                                        <li className="viewlist">
                                                                            number Of Cycles
                                                                        </li>
                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                        <li className="viewlist">
                                                                            Battery Capacity (%)
                                                                        </li>
                                                                    </ul>
                                                                </Typography>

                                                                <ul className="viewlist">
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.dod1}
                                                                            onChange={this.handleChange("dod1")}
                                                                            name="dod1"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.number_of_cycles1}
                                                                            onChange={this.handleChange(
                                                                                "number_of_cycles1"
                                                                            )}
                                                                            name="number_of_cycles1"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.battery_capacity1}
                                                                            onChange={this.handleChange(
                                                                                "battery_capacity1"
                                                                            )}
                                                                            name="battery_capacity1"
                                                                        />
                                                                    </li>
                                                                </ul>

                                                                <ul className="viewlist">
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.dod2}
                                                                            onChange={this.handleChange("dod2")}
                                                                            name="dod2"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.number_of_cycles2}
                                                                            onChange={this.handleChange(
                                                                                "number_of_cycles2"
                                                                            )}
                                                                            name="number_of_cycles2"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.battery_capacity2}
                                                                            onChange={this.handleChange(
                                                                                "battery_capacity2"
                                                                            )}
                                                                            name="battery_capacity2"
                                                                        />
                                                                    </li>
                                                                </ul>

                                                                <ul className="viewlist">
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.dod3}
                                                                            onChange={this.handleChange("dod3")}
                                                                            name="dod3"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.number_of_cycles3}
                                                                            onChange={this.handleChange(
                                                                                "number_of_cycles3"
                                                                            )}
                                                                            name="number_of_cycles3"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.battery_capacity3}
                                                                            onChange={this.handleChange(
                                                                                "battery_capacity3"
                                                                            )}
                                                                            name="battery_capacity3"
                                                                        />
                                                                    </li>
                                                                </ul>

                                                                <ul className="viewlist">
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.dod4}
                                                                            onChange={this.handleChange("dod4")}
                                                                            name="dod4"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.number_of_cycles4}
                                                                            onChange={this.handleChange(
                                                                                "number_of_cycles4"
                                                                            )}
                                                                            name="number_of_cycles4"
                                                                        />
                                                                    </li>
                                                                    <li className="viewlist">
                                                                        <TextField
                                                                            className="textField"
                                                                            type="number"
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            font-size="true"
                                                                            value={this.state.battery_capacity4}
                                                                            onChange={this.handleChange(
                                                                                "battery_capacity4"
                                                                            )}
                                                                            name="battery_capacity4"
                                                                        />
                                                                    </li>
                                                                </ul>
                                                            </TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("submit")}
                                    disabled={this.state.disabled}
                                >
                                    Submit
                                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Inverter successfully created
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/source/list-source">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                                <Dialog
                                    open={this.state.open3}
                                    onClose={this.handleClose3}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Pick another Configuration, Probes in this configuration are not sufficient to create a new generator
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose3} color="primary">
                                            Close
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default CreateInv1;
