import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
// import {
//   ListRole2,
//   ListRole3,
//   ListRole4,
//   ListRole5,
//   ListUser
// } from "../../Services/UsersRequests/UsersRequests";
import { ConfigUsers, DeleteUsers } from "../../Services/UsersRequests/UsersRequests";
import "./ListUsers.css";

const style = {
    color: "#337ab7",
    textDecoration: "none"
};

interface Istate {
    listgroups: any[]
    listrole0: any[]
    listrole1: any[]
    listrole2: any[];
    listrole3: any[];
    listrole4: any[];
    listrole5: any[];
    listusers: any[];
    role0name: string;
    role1name: string;
    role2name: string;
    role3name: string;
    role4name: string;
    role5name: string;
    allroles: any[]
    open1a: boolean
    open1b: boolean
    id: string
}

class ListUsers extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);

        this.state = {
            allroles: [],
            id: "",
            listgroups: [],
            listrole0: [],
            listrole1: [],
            listrole2: [],
            listrole3: [],
            listrole4: [],
            listrole5: [],
            listusers: [],
            open1a: false,
            open1b: false,
            role0name: "",
            role1name: "",
            role2name: "",
            role3name: "",
            role4name: "",
            role5name: "",
        };
    }

    public componentWillMount() {

        ConfigUsers()
            .then(res => {
                this.setState({
                    allroles: res.data.data
                    // probechannels: res.data.data.probe_channels
                }
                );
                // tslint:disable
                console.log(this.state.allroles);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public handleSubmitFuel = () => {
        // @ts-ignore works
        const userid = this.props.match.params.id;

        // @ts-ignore
        DeleteUsers(userid)
            .then(res => {
                this.setState({ open1b: true });
                // tslint:disable
                console.log(res);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    handleClickOpen = id => {
        console.log(id);
        this.setState({ open1a: true, id: id });
    };

    handleClose = () => {
        this.setState({ open1a: false, open1b: false });
    };

    public CreateColumn(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/usergroup/view-role/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>

                </li>
                <li className="ullist">
                    <Link to={"/usergroup/edit-role/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                <Link to={"/usergroup/delete-role/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
            </ul>
        );
    }

    public CreateColumn1(id) {
        return (
            <ul className="ullist">
                <li className="ullist">
                    <Link to={"/usergroup/view-users/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            View
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Link to={"/usergroup/edit-users/" + id} style={style}>
                        <Button
                            variant="outlined"
                            color="primary"
                            style={{ border: "none", margin: 0 }}
                        >
                            Edit
                        </Button>
                    </Link>
                </li>
                <li className="ullist">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={this.handleClickOpen.bind(this, id)}
                        style={{ border: "none", margin: 0 }}
                    >
                        Delete
                    </Button>
                    <Dialog
                        open={this.state.open1a}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are You Sure You Want To Delete This?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Disagree
                             </Button>
                            <Button
                                onClick={this.handleSubmitFuel.bind(this, id)}
                                color="primary"
                                autoFocus
                            >
                                Agree
                            </Button>
                            <Dialog
                                open={this.state.open1b}
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Deleted
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose} color="primary">
                                        Ok
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </DialogActions>
                    </Dialog>
                </li>
            </ul>
        );
    }


    public render() {

        const alluser = this.state.allroles
        for (const users of alluser) {
            // tslint:disable
            console.log(users)
            const role0 = this.state.listrole0
            const role1 = this.state.listrole1
            const role2 = this.state.listrole2
            const role3 = this.state.listrole3
            const role4 = this.state.listrole4
            const role5 = this.state.listrole5
            if (users.role_id === 0) {
                // tslint:disable
                role0.push(users)
                console.log(this.state.listrole0)
            }
            else if (users.role_id === 1) {
                role1.push(users)
                console.log(this.state.listrole1)
            }
            else if (users.role_id === 2) {
                role2.push(users)
                console.log(this.state.listrole2)
            }
            else if (users.role_id === 3) {
                role3.push(users)
                console.log(this.state.listrole3)
            }
            else if (users.role_id === 4) {
                role4.push(users)
                console.log(this.state.listrole4)
            }
            else if (users.role_id === 5) {
                role5.push(users)
                console.log(this.state.listrole5)
            }
        }
        const Role5Users = this.state.listrole5.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{ListConfig.email.slice(0, ListConfig.email.indexOf("@"))}</li>
            </ul>
        ));

        const Role2Users = this.state.listrole2.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
            </ul>
        ));

        const Role3Users = this.state.listrole3.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
            </ul>
        ));

        const Role4Users = this.state.listrole4.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
            </ul>
        ));

        const Role0Users = this.state.listrole0.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
            </ul>
        ));

        const Role1Users = this.state.listrole1.map((ListConfig, id) => (
            <ul style={{ display: 'inline' }}>
                <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
            </ul>
        ));

        // const AllUsers = this.state.allroles.map((ListConfig, id) => (
        //     <ul style={{ display: 'inline' }}>
        //         <li style={{ display: 'inline' }}>{`-${ListConfig.full_name}`}</li>
        //     </ul>
        // ));

        const FuelTypelength = this.state.listrole5.length;
        console.log(FuelTypelength);

        const Users0 = this.state.listrole0.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.full_name}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));

        const Users1 = this.state.listrole1.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.full_name}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));

        const Users2 = this.state.listrole2.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.full_name}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));

        const Users3 = this.state.listrole3.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.full_name}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));

        const Users4 = this.state.listrole4.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.full_name}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));
        const Userlength = this.state.listrole5.length;
        console.log(Userlength);

        const Users5 = this.state.listrole5.map((ListConfig, id) => (
            <TableRow key={id} style={{ height: 50 }}>
                <TableCell>{ListConfig.email.slice(0, ListConfig.email.indexOf("@"))}</TableCell>
                <TableCell>{ListConfig.email}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{ListConfig.role_name}</TableCell>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell>{this.CreateColumn1(ListConfig.user_id)}</TableCell>
            </TableRow>
        ));

        return (
            <div>
                <Table className="table1" style={{ width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Roles</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell colSpan={4}>
                                <Link to={"/usergroup/create-role"}>
                                    <Button
                                        variant="contained"
                                        style={{ backgroundColor: "#337ab7" }}
                                    >
                                        Create New Role
                                    </Button>
                                </Link>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Name</TableCell>
                            <TableCell>permissions</TableCell>
                            <TableCell>Assigned To</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                        {/* {Roles} */}
                        <TableRow style={{ height: 50 }}>
                            <TableCell>Admin</TableCell>
                            <TableCell />
                            <TableCell>{Role5Users}</TableCell>
                            <TableCell>{this.CreateColumn("admin")}</TableCell>
                        </TableRow>
                        <TableRow style={{ height: 50 }}>
                            <TableCell>Guest</TableCell>
                            <TableCell />
                            <TableCell>{Role0Users}</TableCell>
                            <TableCell>{this.CreateColumn("guest")}</TableCell>
                        </TableRow>
                        <TableRow style={{ height: 50 }}>
                            <TableCell>Regional Admin</TableCell>
                            <TableCell />
                            <TableCell>{Role4Users}</TableCell>
                            <TableCell>{this.CreateColumn("regional-admin")}</TableCell>
                        </TableRow>
                        <TableRow style={{ height: 50 }}>
                            <TableCell>Super User</TableCell>
                            <TableCell />
                            <TableCell>{Role3Users}</TableCell>
                            <TableCell>{this.CreateColumn("super-user")}</TableCell>
                        </TableRow>
                        <TableRow style={{ height: 50 }}>
                            <TableCell>{this.state.role1name}</TableCell>
                            <TableCell />
                            <TableCell>{Role1Users}</TableCell>
                            <TableCell>{this.CreateColumn(this.state.role1name)}</TableCell>
                        </TableRow>
                        <TableRow style={{ height: 50 }}>
                            <TableCell>User</TableCell>
                            <TableCell />
                            <TableCell>{Role2Users}</TableCell>
                            <TableCell>{this.CreateColumn("user")}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                <div>&nbsp;</div>

                <Table className="table1" style={{ width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">User Associated with Group</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell colSpan={6}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Create New User
                                </Button>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Name</TableCell>
                            <TableCell>Email</TableCell>
                            <TableCell>Country</TableCell>
                            <TableCell>State</TableCell>
                            <TableCell>Profile Update</TableCell>
                            <TableCell>Belongs to Group(s)</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                <div>&nbsp;</div>

                <div style={{ overflowX: "auto", width: 900 }}>
                    <Table className="table1" style={{ width: 800 }}>
                        <TableHead className="head">
                            <TableRow className="tablecell">
                                <TableCell className="tablecell">
                                    <span id="tablecell">Users</span>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell colSpan={11}>
                                    <Button
                                        variant="contained"
                                        style={{ backgroundColor: "#337ab7" }}
                                    >
                                        Create New User
                                    </Button>
                                </TableCell>
                            </TableRow>
                            <TableRow className="tablerow">
                                <TableCell>Name</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Country</TableCell>
                                <TableCell>State</TableCell>
                                <TableCell>Send Invoice</TableCell>
                                <TableCell>Roles</TableCell>
                                <TableCell>Profile Update</TableCell>
                                <TableCell>Stage</TableCell>
                                <TableCell>Payment Status</TableCell>
                                <TableCell>Belongs to Groups(s)</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                            {Users0}
                            {Users1}
                            {Users2}
                            {Users3}
                            {Users4}
                            {Users5}
                        </TableBody>
                    </Table>
                </div>

                <div>&nbsp;</div>
            </div>
        );
    }
}

export default ListUsers;
