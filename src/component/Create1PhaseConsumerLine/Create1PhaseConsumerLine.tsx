import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { Link } from "react-router-dom";
import { CreateLine1, ListConfiguration, ListGroups, ListGroupType, ListProbe } from "../../Services/ConsumerLineRequests/ConsumerLineRequests";
import "./Create1PhaseConsumerLine.css";


interface Istate {
    channelss: any[];
    config?: "";
    configurations: any[];
    consumergroup: any;
    groups: any[];
    groupadmin: string;
    grouptypes: any[];
    linename: string;
    open: boolean;
    phase1: string;
    phase2: string;
    phase3: string;
    probe: string;
    probes: any[];
    profitmargin?: number;
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    threshold: string;
    type: string;
    disabled: boolean;
    open1: boolean
}

class Create1PhaseConsumerLine extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            channelss: [],
            configs: true,
            configurations: [],
            consumergroup: [],
            disabled: true,
            groupadmin: "",
            groups: [],
            grouptypes: [],
            linename: "",
            name: [],
            open: true,
            open1: false,
            phase1: "",
            phase2: "",
            phase3: "",
            probe: "",
            probes: [],
            sourceconfig: "",
            threshold: "",
            type: "",
            value: ""
        };
    }

    public componentWillMount() {
        // @ts-ignore works
        const fuelId = this.props.match.params.id;

        // tslint:disable

        ListGroupType()
            .then(res => {
                this.setState({ grouptypes: res.data.data });
                // tslint:disable
                // console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListGroups()
            .then(res => {
                this.setState({ groups: res.data.data });
                // tslint:disable
                // console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListProbe()
            .then(res => {
                this.setState({ probes: res.data.data });
                // tslint:disable
                // console.log(res.data.data);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public ListConfig = () => {
        const userId = this.state.consumergroup;
        ListConfiguration(userId)
            .then(res => {
                this.setState({ configurations: res.data.data.probe_channels });
                // tslint:disable
                // console.log(res.data.data.probe_channels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    };

    public ListChannels = () => {
        const userId = this.state.consumergroup;
        ListConfiguration(userId)
            .then(res => {
                this.setState({ channelss: res.data.data.probe_channels[0].channels });
                // tslint:disable
                // console.log(res.data.data.probe_channels[0].channels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    };

    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.linename &&
                this.state.consumergroup &&
                this.state.phase1
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    //   public handleConsumerGroupChange = event => {
    //     let selectedGroup = event.target.value;
    //   };

    public handleClose = () => {
        this.setState({ open: false });
    };

    public handleClose1 = () => {
        this.setState({ open1: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.open });
    };

    public handleSubmit = event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.linename,
            signal_threshold: 0,
            consumer_group_id: this.state.consumergroup,
            channel_id: this.state.phase1
        };

        // @ts-ignore
        CreateLine1(body, body.fuel_id)
            .then(res => {
                // console.log(res);
                this.setState({ open1: true })
            })
            .catch(err => {
                console.log(err.response);
                alert("Line not created. Please try again!");
            });
    };

    public render() {

        const Grouptypes = this.state.grouptypes.map((GroupType, id) => (
            <MenuItem key={id} value={GroupType.consumer_group_type_id}>
                {GroupType.name}
            </MenuItem>
        ));

        const Groups = this.state.groups.map((Group, id) => {
            let configId = Group.configuration.configuration_id;
            return (
                <MenuItem key={id} value={configId}>
                    {Group.name}
                </MenuItem>
            );
        });

        const Probes = this.state.configurations.map((Probe, id) => {
            return (
                <MenuItem key={id} value={Probe.probe_id}>
                    {Probe.probe_name}
                </MenuItem>
            );
        });

        const channel = this.state.channelss.map((Probe, id) => {
            let probeId = Probe.probe_id;
            if (Probe.probe_id.indexOf(probeId) > -1) {
                return (
                    <MenuItem key={id} value={Probe.channel_id}>
                        {Probe.channel_name}
                    </MenuItem>
                );
            } else {
            }
        });

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create Power Consumer Line</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Name</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.linename}
                                                    onChange={this.handleChange("linename")}
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography>Type</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Select
                                                    value={this.state.type}
                                                    onChange={this.handleChange("type")}
                                                    name="type"
                                                >
                                                    {Grouptypes}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Signal Threshold</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    type="number"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.threshold}
                                                    onChange={this.handleChange("threshold")}
                                                    name="threshold"
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography>Consumer Group</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Select
                                                    value={this.state.consumergroup}
                                                    onChange={this.handleChange(
                                                        "consumergroup",
                                                        this.ListConfig
                                                    )}
                                                    name="consumergroup"
                                                >
                                                    {Groups}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>
                                <Divider />
                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableHead className="head">
                                        <TableRow className="tablecell">
                                            <TableCell className="tablecell">
                                                <span id="tablecell">Channel Assignment</span>
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Probe(s)</Typography>
                                                <Select
                                                    value={this.state.probe}
                                                    onChange={this.handleChange(
                                                        "probe",
                                                        this.ListChannels
                                                    )}
                                                    name="probe"
                                                >
                                                    {Probes}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Phase 1 Channel</Typography>
                                                <Select
                                                    value={this.state.phase1}
                                                    onChange={this.handleChange("phase1")}
                                                    name="phase1"
                                                >
                                                    {channel}
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit}
                                    disabled={this.state.disabled}
                                >
                                    Add Consumer Line
                </Button>
                                <Dialog
                                    open={this.state.open1}
                                    onClose={this.handleClose1}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Line successfully created
                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="/consumer/list-consumer">
                                            <Button onClick={this.handleClose1} color="primary">
                                                Close
                      </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default Create1PhaseConsumerLine;
