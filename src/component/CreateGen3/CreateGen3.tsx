import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import times from "lodash/times";
import * as React from "react";
import { Link } from "react-router-dom";
import { ListConfig, ListConfigurations } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import { ListFuels } from "../../Services/FuelRequests/FuelRequests";
import { CreateThreePhaseGenerator } from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./CreateGen3.css";

interface Istate {
    config?: "";
    configuration_id: string;
    groupadmin?: "";
    open: boolean;
    probe_id: string;
    profitmargin?: number;
    signal_threshold: string;
    sourceconfig?: "";
    value: "";
    name: [];
    names: string;
    configs: boolean;
    listconfig: any[];
    channel_id1: string;
    channel_id2: string;
    channel_id3: string;
    selpro: any[];
    selcha: any[];
    fuel_id: string;
    fuel_store_size: string;
    apparent_power_rating: string;
    date: string;
    listfuels: any[];
    start_timeOW: string;
    stop_timeOW: string;
    start_timeCM: string;
    stop_timeCM: string;
    amount: number;
    open1: boolean;
    expanded: null;
    extra: number;
    count: number;
    data: any[];
    amounts: number;
    quarter_load: number;
    half_load: number;
    three_quarter_load: number;
    full_load: number;
    disabled: boolean;
    open2: boolean;
    open3: boolean
    listconfiguration: any[]
    selchavailable: any[]
    count1: number;
    userprobechannels: any[]
}

const styles = theme => ({
    heading: {
        flexBasis: "33.33%",
        flexShrink: 0,
        fontSize: theme.typography.pxToRem(15)
    },

    root: {
        width: "100%"
    },

    secondaryHeading: {
        color: theme.palette.text.secondary,
        fontSize: theme.typography.pxToRem(15)
    }
});

class CreateGen3 extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            amount: 0,
            amounts: 0,
            apparent_power_rating: "",
            channel_id1: "",
            channel_id2: "",
            channel_id3: "",
            configs: true,
            configuration_id: "",
            count: 1,
            count1: 0,
            data: [{}],
            date: "",
            disabled: true,
            expanded: null,
            extra: 0,
            fuel_id: "",
            fuel_store_size: "",
            full_load: 0,
            groupadmin: "",
            half_load: 0,
            listconfig: [],
            listconfiguration: [],
            listfuels: [],
            name: [],
            names: "",
            open: true,
            open1: false,
            open2: false,
            open3: false,
            probe_id: "",
            quarter_load: 0,
            selcha: [],
            selchavailable: [],
            selpro: [],
            signal_threshold: "",
            sourceconfig: "",
            start_timeCM: "",
            start_timeOW: "",
            stop_timeCM: "",
            stop_timeOW: "",
            three_quarter_load: 0,
            // profitmargin: '',
            userprobechannels: [],
            value: ""
        };
    }

    public componentWillMount() {
        ListConfig()
            .then(res => {
                this.setState({
                    listconfig: res.data.data
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                // console.log(this.state.listconfig);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        ListFuels()
            .then(res => {
                this.setState({ listfuels: res.data.data });
                // tslint:disable
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });

        const userId = localStorage.getItem("user_id");

        // @ts-ignore
        ListConfigurations(userId)
            .then(res => {
                this.setState({
                    listconfiguration: res.data.data,
                    userprobechannels: res.data.data.probe_channels
                });
                // tslint:disable
                // console.log(this.state.listconfig);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public probeSelection = () => {
        let configId = this.state.configuration_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        const selprobe = this.state.listconfiguration.find(
            probe => probe.configuration_id === configId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selpro: selprobe.probe_channels }, () => {
            console.log(this.state.selpro)
            // let available: any[] = []
            // if (count > 2) {
            //     available.push(this.state.selpro[i])
            //     console.log(available)
            //     return available
            // }
            let available1: any[] = []
            for (let i = 0; i < this.state.selpro.length; i++) {
                // let count = 0
                let current = this.state.selpro[i]
                let available: any[] = []
                for (let j = 0; j < current.channels.length; j++) {
                    if (current.channels[j].assigned_to_source === false) {
                        // let count = this.state.count1
                        // console.log(count);
                        available.push(this.state.selpro[i].channels[j])
                        console.log(available)
                        // this.setState({ count1: count++ }, () => {
                        //     console.log(this.state.selpro)
                        //     console.log(this.state.count1)
                        //     // let available: any[] = []
                        //     // let channel = this.state.selpro[i]
                        // })
                        // count++;
                    }
                    else {
                        console.log(this.state.selpro)
                    }
                    this.setState({ selchavailable: available })
                }
                if (available.length > 2) {
                    available1.push(this.state.selpro[i])
                    console.log(available1)
                }
                else {
                    this.setState({
                        selpro: [],
                        open3: true
                    })
                }
            }
        });
    };

    public channelSelection = () => {
        let probeId = this.state.probe_id;
        // let probeId = "cbhdbhf-dmjn-dmf";
        const selchannel = this.state.selchavailable.find(
            probe => probe.probe_id === probeId
        );
        // const probechannels = selprobe.channels;
        this.setState({ selcha: selchannel.channels });
    };

    // @ts-ignore works
    public handleChange = (name, funcName = () => { }) => event => {
        // @ts-ignore works
        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
            if (
                this.state.names &&
                this.state.signal_threshold &&
                this.state.configuration_id &&
                this.state.probe_id &&
                this.state.channel_id1 &&
                this.state.channel_id2 &&
                this.state.channel_id3 &&
                this.state.apparent_power_rating &&
                this.state.fuel_id &&
                this.state.fuel_store_size &&
                this.state.start_timeOW &&
                this.state.stop_timeOW &&
                this.state.data &&
                this.state.quarter_load &&
                this.state.half_load &&
                this.state.three_quarter_load &&
                this.state.full_load
            ) {
                this.setState({ disabled: false });
            }
        });
    };

    public handleTimeChange = (countNumber, where, time) => {
        let timeArr = [...this.state.data];
        timeArr[countNumber][where] = time;

        this.setState({ data: timeArr });
    };

    public handleExtra = () => {
        const data = this.state.data;
        let oneData = {
            start_time: "",
            stop_time: "",
            amount: ""
        };
        data.push(oneData);
        // event.preventDefault();
        this.setState({ count: this.state.count + 1 });
    };

    public handleClickOpen = () => {
        this.setState({ open: true, open1: true });
    };

    public handleClose = () => {
        this.setState({ open: false, open1: false });
    };

    public handleClose2 = () => {
        this.setState({ open2: false });
    };

    public handleClose3 = () => {
        this.setState({ open3: false });
    };

    public onChange = date => this.setState({ date });

    public handleSubmit = (name, funcName = () => { }) => event => {
        event.preventDefault();

        const body = {
            // tslint:disable
            name: this.state.names,
            signal_threshold: this.state.signal_threshold,
            configuration_id: this.state.configuration_id,
            probe_id: this.state.probe_id,

            probe_channels: [
                this.state.channel_id1,
                this.state.channel_id2,
                this.state.channel_id3
            ],
            apparent_power_rating: this.state.apparent_power_rating,
            fuel_id: this.state.fuel_id,
            fuel_store_size: 0,
            operating_window: [
                {
                    start_time: this.state.start_timeOW,
                    stop_time: this.state.stop_timeOW
                }
            ],
            charge_map: this.state.data,
            generator_map: [
                {
                    quarter_load_price: this.state.quarter_load,
                    half_load_price: this.state.half_load,
                    three_quarter_load_price: this.state.three_quarter_load,
                    full_load_price: this.state.full_load
                }
            ]
        };

        // @ts-ignore
        CreateThreePhaseGenerator(body)
            .then(res => {
                // console.log(res);
                this.setState({ open2: true });
            })
            .catch(err => {
                console.log(err.response);
                alert("Generator not created. Please try again!");
            });

        this.setState({ ...this.state, [name]: event.target.value }, () => {
            if (funcName) {
                funcName();
            }
        });
    };

    public renderTimer = props => {
        return (
            <div>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-native-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Start Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["start_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "start_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        id="time"
                        label="Stop Time"
                        type="time"
                        value={
                            this.state.data[props.name]
                                ? this.state.data[props.name]["stop_time"]
                                : ""
                        }
                        onChange={e =>
                            this.handleTimeChange(props.name, "stop_time", e.target.value)
                        }
                        style={{ marginRight: 10 }}
                        autoFocus={false}
                    />
                </FormControl>
                <FormControl style={{ maxWidth: 90 }}>
                    {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                    <TextField
                        className="textField"
                        type="number"
                        margin="normal"
                        variant="outlined"
                        font-size="true"
                        value={"" || this.state.data[props.name]["amount"]}
                        onChange={e =>
                            this.handleTimeChange(props.name, "amount", e.target.value)
                        }
                        autoFocus={false}
                    />
                </FormControl>
            </div>
        );
    };

    public render() {
        const Timer = this.renderTimer;

        const ConfigLists = this.state.listconfiguration.map((ConfigList, id) => (
            <MenuItem key={id} value={ConfigList.configuration_id} autoFocus={false}>
                {ConfigList.configuration_name}
            </MenuItem>
        ));

        const ProbeLists = this.state.selpro.map((ProbeList, id) => (
            <MenuItem key={id} value={ProbeList.probe_id} autoFocus={false}>
                {ProbeList.probe_name}
            </MenuItem>
        ));

        const ChannelLists = this.state.selchavailable.map((ChannelList, id) => (
            <MenuItem key={id} value={ChannelList.channel_id} autoFocus={false}>
                {ChannelList.channel_name}
            </MenuItem>
        ));

        const FuelLists = this.state.listfuels.map((FuelList, id) => (
            <MenuItem key={id} value={FuelList.fuel_id} autoFocus={false}>
                {FuelList.name}
            </MenuItem>
        ));

        // this.handleTimeChange(props.name, "amount", e.target.value)

        let ExtraChargeMap = [];
        //     const Timer = (props) => {
        // return (

        times(this.state.count, i => {
            ExtraChargeMap.push(
                // @ts-ignore
                <Timer name={`${i}`} />
            );
            return ExtraChargeMap;
        });
        // return ExtraChargeMap;
        // }

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New Power Source</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Table className="table">
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Name</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.names}
                                                    onChange={this.handleChange("names")}
                                                    name="names"
                                                    autoFocus={false}
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>Signal Threshold</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <TextField
                                                    className="textField"
                                                    type="number"
                                                    margin="normal"
                                                    variant="outlined"
                                                    font-size="true"
                                                    value={this.state.signal_threshold}
                                                    onChange={this.handleChange("signal_threshold")}
                                                    name="signal_threshold"
                                                    autoFocus={false}
                                                />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Type</TableCell>
                                            <TableCell>Three Phase Generator</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <div>&nbsp;</div>
                                <Divider />
                                <div>&nbsp;</div>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Channel Assignment</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Config</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.configuration_id}
                                                            onChange={this.handleChange(
                                                                "configuration_id",
                                                                this.probeSelection
                                                            )}
                                                            name="configuration_id"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ConfigLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Probe(s)</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.probe_id}
                                                            onChange={this.handleChange(
                                                                "probe_id",
                                                                this.channelSelection
                                                            )}
                                                            name="probe_id"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ProbeLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Phase 1 Channel</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.channel_id1}
                                                            onChange={this.handleChange("channel_id1")}
                                                            name="channel_id1"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ChannelLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Phase 2 Channel</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.channel_id2}
                                                            onChange={this.handleChange("channel_id2")}
                                                            name="channel_id2"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ChannelLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Phase 3 Channel</Typography>
                                                    </div>
                                                    <div>
                                                        <Select
                                                            value={this.state.channel_id3}
                                                            onChange={this.handleChange("channel_id3")}
                                                            name="channel_id3"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {ChannelLists}
                                                        </Select>
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table className="table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Capacity</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <div className='flex'>
                                                    <div className='text'>
                                                        <Typography>Apparent Power [kVa]</Typography>
                                                    </div>
                                                    <div>
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.apparent_power_rating}
                                                            onChange={this.handleChange("apparent_power_rating")}
                                                            name="apparent_power_rating"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        />
                                                    </div>
                                                </div>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">
                                                        <Typography>Fuel Type</Typography>
                                                    </li>
                                                    <li className="sourcetable">
                                                        <Select
                                                            value={this.state.fuel_id}
                                                            onChange={this.handleChange("fuel_id")}
                                                            name="fuel_id"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        >
                                                            {FuelLists}
                                                        </Select>
                                                    </li>
                                                </ul>
                                                <ul className="sourcetable">
                                                    <li className="sourcetable">
                                                        <Typography>Fuel Store</Typography>
                                                    </li>
                                                    <li className="sourcetable">
                                                        <TextField
                                                            className="textField"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.fuel_store_size}
                                                            onChange={this.handleChange("fuel_store_size")}
                                                            name="fuel_store_size"
                                                            autoFocus={false}
                                                            style={{ width: '300px' }}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Operating Window</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <Typography>
                                                    <ul className="viewlist">
                                                        <li className="viewlist">From:</li>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <li className="viewlist">To:</li>
                                                    </ul>
                                                </Typography>

                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Start Time"
                                                            type="time"
                                                            onChange={this.handleChange("start_timeOW")}
                                                            autoFocus={false}
                                                        />
                                                    </li>
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    &nbsp; &nbsp; &nbsp;
                                                    <li className="viewlist">
                                                        <TextField
                                                            id="time"
                                                            label="Stop Time"
                                                            type="time"
                                                            onChange={this.handleChange("stop_timeOW")}
                                                            autoFocus={false}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Charge Map</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableRow>
                                        <Button onClick={this.handleClickOpen} autoFocus={false} style={{ backgroundColor: "#337ab7" }}>
                                            Open charge map dialog
                                        </Button>
                                        <Dialog
                                            disableBackdropClick={true}
                                            disableEscapeKeyDown={true}
                                            open={this.state.open1}
                                            onClose={this.handleClose}
                                        >
                                            <DialogTitle>Fill the form</DialogTitle>
                                            <DialogContent>
                                                <form
                                                    style={{
                                                        border: "none",
                                                        display: "flex",
                                                        flexWrap: "wrap",
                                                        height: 300,
                                                        maxWidth: 300
                                                        // minHeight: 50
                                                    }}
                                                >
                                                    {/* <Timer name="0" /> */}
                                                    {ExtraChargeMap}
                                                    {/* <Timer name={`${i}`} /> */}
                                                    <Button
                                                        onClick={this.handleExtra}
                                                        color="primary"
                                                        autoFocus={false}
                                                    >
                                                        Add More
                                                    </Button>
                                                </form>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button
                                                    onClick={this.handleClose}
                                                    color="primary"
                                                    autoFocus={false}
                                                >
                                                    Ok
                                                </Button>
                                            </DialogActions>
                                        </Dialog>
                                    </TableRow>
                                </Table>

                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Fuel Consumption Map</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow className="tablerow">
                                            <TableCell>
                                                <ul className="viewlist">
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="1/4 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.quarter_load}
                                                            onChange={this.handleChange("quarter_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="1/2 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.half_load}
                                                            onChange={this.handleChange("half_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="3/4 load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.three_quarter_load}
                                                            onChange={this.handleChange("three_quarter_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                    <li className="viewlist">
                                                        <TextField
                                                            label="full load"
                                                            className="textField"
                                                            type="number"
                                                            margin="normal"
                                                            variant="outlined"
                                                            font-size="true"
                                                            value={this.state.full_load}
                                                            onChange={this.handleChange("full_load")}
                                                            name="signal_threshold"
                                                            InputLabelProps={{
                                                                shrink: true
                                                            }}
                                                        />
                                                    </li>
                                                </ul>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    onClick={this.handleSubmit("submit")}
                                    autoFocus={false}
                                    disabled={this.state.disabled}
                                >
                                    Submit
                                </Button>
                                <Dialog
                                    open={this.state.open2}
                                    onClose={this.handleClose2}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Generator successfully created
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Link to="source/list-source">
                                            <Button onClick={this.handleClose2} color="primary">
                                                Close
                                            </Button>
                                        </Link>
                                    </DialogActions>
                                </Dialog>
                                <Dialog
                                    open={this.state.open3}
                                    onClose={this.handleClose3}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">
                                        {"Alert!!!"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            Pick another Configuration, Probes in this configuration are not sufficient to create a new generator
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose3} color="primary">
                                            Close
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div >
        );
    }
}

export default withStyles(styles)(CreateGen3);
