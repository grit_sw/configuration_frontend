import "./UsersSettings.css";

import * as React from "react";
import { Link } from "react-router-dom";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class UsersSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className="lists">
          <li className="lists">
            <Link to="/usergroup/list-users" style={style}>
              List Users
            </Link>
          </li>

          <li className="lists">
            <Link to="/usergroup/list-group" style={style}>
              List User Groups
            </Link>
          </li>

          <li className="lists">
            <Link to="/usergroup/create-group" style={style}>
              Create new Users Group
            </Link>
          </li>

          <li className="lists">
            <Link to="/usergroup/create-domain" style={style}>
              Create Domain
            </Link>
          </li>

          <li className="lists">
            <Link to="/usergroup/list-domain" style={style}>
              List Domains
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default UsersSettings;
