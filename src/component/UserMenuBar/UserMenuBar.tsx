import { withStyles, WithStyles } from "@material-ui/core/styles";
// import { blue500, green500 } from '@material-ui/core/styles/colorManipulator';
import * as React from "react";
import { Link } from "react-router-dom";
import "./UserMenuBar.css";

const styles = {
  fullList: {
    width: "auto"
  },
  list: {
    width: 250
  }
};

const userRole = localStorage.getItem("user_role");
const userToken = localStorage.getItem("user_token");

// tslint:disable
console.log(userRole);

// const menu = (
//   <ul><li>hbf</li></ul>
// )

interface Istate {
  left: boolean;
  admin: boolean;
}

interface IProps extends WithStyles<typeof styles> {
  close: Function;
}

class UserMenuBar extends React.Component<IProps, Istate> {
  constructor(props) {
    super(props);

    this.state = {
      left: false,
      admin: false
    };
  }

  public componentWillMount() {
    // @ts-ignore
    // let tokenLength = userToken.length;
    // console.log(tokenLength);
    if (userToken && (userRole === "5")) {
      this.setState({ admin: true });
    }
  }

  public toggleDrawer = option => () => {
    this.setState({
      left: option
    });
  };

  public menu = (
    <ul>
      <li>hbf</li>
    </ul>
  );

  public render() {
    // @ts-ignore
    // console.log(userToken.length)

    console.log(this.state.admin);

    return (
      <div>
        <div className="listbar">
          {this.state.admin ? (
            // @ts-ignore

            <ul className="list2">
              <li className="list">
                <Link
                  to="/configuration/list-configuration"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Configurations
                </Link>
              </li>
              <li className="list">
                <Link
                  to="/usergroup/list-users"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Users
                </Link>
              </li>

              <li className="list">
                <Link
                  to="/probe/list-probe"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Probes
                </Link>
              </li>

              <li className="list">
                <Link
                  to="/source/list-source"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Power Sources
                </Link>
              </li>

              <li className="list">
                <Link
                  to="/consumer/list-consumer"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Power Consumers
                </Link>
              </li>
              <li className="list">
                <Link
                  to="/configuration"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Payments
                </Link>
              </li>
              <li className="list">
                <Link
                  to="/products/list-products"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Products
                </Link>
              </li>

              <li className="list">
                <Link
                  to="/fuel/list-fuel"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Fuel
                </Link>
              </li>
            </ul>
          ) : (
            <ul>
              <li className="list">
                <Link
                  to="/probe/list-probe"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Probes
                </Link>
              </li>
              <li className="list">
                <Link
                  to="/source/list-source"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Power Sources
                </Link>
              </li>
              <li className="list">
                <Link
                  to="/fuel/list-fuel"
                  style={{
                    color: "#337ab7",
                    textDecoration: "none"
                  }}
                >
                  Fuel
                </Link>
              </li>
            </ul>
          )}
        </div>
        <div className="drawer">
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(UserMenuBar);
