// import ListBox from 'react-listbox';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
  default as Table,
  default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import DualListBox from "react-dual-listbox";
import { ViewConfig } from "../../Services/ConfigurationRequests/ConfigurationRequests";
import "./ViewConfiguration.css";

interface Istate {
  config?: "";
  groupadmin: any[];
  open: boolean;
  profitmargin?: number;
  sourceconfig?: "";
  value: "";
  name: [];
  configs: boolean;
  selected?: "";
  probes?: "";
  channels?: "";
  viewconfig: any[];
  configuration_members: any[];
  error_threshold: any[];
  probe_channels: any[];
}
class ViewConfiguration extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);
    this.state = {
      configs: true,
      configuration_members: [],
      error_threshold: [],
      groupadmin: [],
      name: [],
      open: true,
      probe_channels: [],
      sourceconfig: "",
      // profitmargin: '',
      value: "",
      viewconfig: []
    };
  }

  public componentWillMount() {
    // @ts-ignore works
    const configId = this.props.match.params.id;

    // tslint:disable
    console.log(configId);

    ViewConfig(configId)
      .then(res => {
        this.setState({
          viewconfig: res.data.data,
          name: res.data.data.configuration_name,
          groupadmin: res.data.data.admin_name,
          configuration_members: res.data.data.configuration_members,
          error_threshold: res.data.data.error_threshold,
          probes: res.data.data.probe_channels[0].probe_name,
          probe_channels: res.data.data.probe_channels[0].channels
        });
        // tslint:disable
        console.log(res.data.data.probe_channels[0].channels);
        // console.log(this.state.listfuels.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public render() {
    const { selected } = this.state;

    const ConfigMembers = this.state.configuration_members.map(members => {
      return {
        value: members.member_id,
        label: members.member_name
      };
    });

    const ProbeChannels = this.state.probe_channels.map(channels => {
      return {
        value: channels.channel_id,
        label: channels.channel_name
      };
    });

    return (
      <div>
        <Table className="table" style={{ overflowX: "auto", width: 900 }}>
          <TableHead className="head">
            <TableRow className="tablecell">
              <TableCell className="tablecell">
                <span id="tablecell">View Configuration</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow className="tablerow">
              <TableCell>Name</TableCell>
              <TableCell>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.name}
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                />
              </TableCell>
              <TableCell colSpan={2} rowSpan={2} />
            </TableRow>
            <TableRow className="tablerow">
              <TableCell>Assign to User</TableCell>
              <TableCell>
                <Select value={this.state.groupadmin} name="groupadmin">
                  <MenuItem value={this.state.groupadmin}>
                    <em>{this.state.groupadmin}</em>
                  </MenuItem>
                </Select>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell />
              <TableCell colSpan={3}>
                <DualListBox
                  name="moons"
                  options={ConfigMembers}
                  selected={selected}
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Error Threshold</TableCell>
              <TableCell>
                <TextField
                  className="textField"
                  margin="normal"
                  variant="outlined"
                  font-size="true"
                  value={this.state.error_threshold}
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                />
              </TableCell>
              <TableCell colSpan={2} rowSpan={3} />
            </TableRow>
            <TableRow>
              <TableCell>Display Energy Today</TableCell>
              <TableCell>
                <Select
                  value={this.state.value}
                  name="probes"
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                >
                  <MenuItem value="1">True</MenuItem>
                  <MenuItem value="0">False</MenuItem>
                </Select>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Select Probes</TableCell>
              <TableCell>
                <Select
                  value={this.state.probes}
                  name="probes"
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                >
                  <MenuItem value={this.state.probes}>
                    <em>{this.state.probes}</em>
                  </MenuItem>
                </Select>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell />
              <TableCell>Available Channels</TableCell>
              <TableCell>Assigned Channels</TableCell>
            </TableRow>
            <TableRow>
              <TableCell />
              <TableCell colSpan={2}>
                <DualListBox
                  name="moons"
                  options={ProbeChannels}
                  selected={selected}
                  inputProps={{
                    readOnly: Boolean("readOnly"),
                    disabled: Boolean("readOnly")
                  }}
                  required
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }

  // interface IConfigProps {
  //     configprops: {};
  // }

  // interface IConfigState {
  //     configState: {value: string};
  // }
}

export default ViewConfiguration;
