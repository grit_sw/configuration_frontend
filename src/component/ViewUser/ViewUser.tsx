import { Typography } from "@material-ui/core";
import { default as Table, default as TableBody } from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import { OtherUserDetails } from "../../Services/UsersRequests/UsersRequests";
import "./ViewUser.css";


interface Istate {
    name: string;
    username: string
    email: string
    phone: string
    location: string
    bio: string
    timewindow: string
    locale: string
    timezone: string
    verified: string
    roles: string
    permissions: string
}

class ViewUser extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            bio: "",
            email: "",
            locale: "",
            location: "",
            name: "",
            permissions: "",
            phone: "",
            roles: "",
            timewindow: "",
            timezone: "",
            username: "",
            verified: ""

        };
    }

    public componentWillMount() {
        // @ts-ignore works

        const userid = this.props.match.params.id;

        OtherUserDetails(userid)
            .then(res => {
                this.setState({
                    email: res.data.data.email,
                    name: res.data.data.fullname,
                    roles: res.data.data.role_name,
                    username: res.data.data.first_name,
                    // probechannels: res.data.data.probe_channels
                });
                // tslint:disable
                console.log(res.data.data);
                // console.log(this.state.probechannels);
            })
            .catch(err => {
                // tslint:disable
                console.log(err);
            });
    }

    public render() {
        // tslint:disable

        return (
            <div>
                <Table className="table" style={{ overflowX: "auto", width: 900 }}>
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">User</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Names</Typography></div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            name="probe_name"
                                            value={this.state.name}
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Username</Typography></div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            name="probe_name"
                                            value={this.state.username}
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Email</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.email}
                                            name="channelnumber"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Phone</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.phone}
                                            name="channelnumber"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Location</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.location}
                                            name="wifi_network_name"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Bio</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.bio}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Timewindow</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.timewindow}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Locale</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.locale}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Timezone</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.timezone}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Verified</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.verified}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Roles</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.roles}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>
                                <div className='flex'>
                                    <div className='text'>
                                        <Typography>Permissions</Typography>
                                    </div>
                                    <div>
                                        <TextField
                                            className="textField"
                                            margin="normal"
                                            variant="outlined"
                                            font-size="true"
                                            value={this.state.permissions}
                                            name="wifi_password"
                                            style={{ width: '300px' }}
                                            inputProps={{
                                                readOnly: Boolean("readOnly"),
                                                disabled: Boolean("readOnly")
                                            }}
                                        />
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        {/* <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Update Firmware
                                </Button>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                    disabled={this.state.disabled}
                                >
                                    Save Probe
                                </Button>
                            </TableCell>
                        </TableRow> */}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default ViewUser;
