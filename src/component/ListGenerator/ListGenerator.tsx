import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import * as React from "react";
import { Link } from "react-router-dom";
import {
  DeleteOnePhaseGen,
  DeleteThreePhaseGen,
  ListOnePhaseGenerator,
  ListThreePhaseGenerator
} from "../../Services/PowerSourcesRequests/PowerSourcesRequests";
import "./ListGenerator.css";

const style = {
  color: "#337ab7",
  textDecoration: "none"
};


interface Istate {
  id: string;
  listonephasegenerator: any[];
  listthreephasegenerator: any[];
  open1a: boolean;
  open1b: boolean;
  open3a: boolean;
  open3b: boolean;
}

class ListGenerator extends React.Component<{}, Istate> {
  // @ts-ignore works
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      listonephasegenerator: [],
      listthreephasegenerator: [],
      open1a: false,
      open1b: false,
      open3a: false,
      open3b: false
    };
  }

  public handleSubmitGen1 = () => {

    const genid = this.state.id;

    // @ts-ignore
    DeleteOnePhaseGen(genid)
      .then(res => {
        this.setState({ open1b: true });
        // tslint:disable
        console.log(res);
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  public handleSubmitGen3 = () => {

    const genid = this.state.id;

    // @ts-ignore
    DeleteThreePhaseGen(genid)
      .then(res => {
        this.setState({ open3b: true });
        // tslint:disable
        console.log(res);
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  handleClickOpen1 = id => {
    console.log(id);
    this.setState({ open1a: true, id: id });
  };

  handleClickOpen3 = id => {
    console.log(id);
    this.setState({ open3a: true, id: id });
  };

  handleClose1 = () => {
    this.setState({ open1a: false, open1b: false });
  };

  handleClose3 = () => {
    this.setState({ open3a: false, open3b: false });
  };

  public componentWillMount() {
    ListOnePhaseGenerator()
      .then(res => {
        this.setState({ listonephasegenerator: res.data.data });
        // tslint:disable
        console.log(this.state.listonephasegenerator);
        console.log(this.state.listonephasegenerator.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });

    ListThreePhaseGenerator()
      .then(res => {
        this.setState({ listthreephasegenerator: res.data.data });
        // tslint:disable
        console.log(this.state.listthreephasegenerator);
        console.log(this.state.listthreephasegenerator.length);
      })
      .catch(err => {
        // tslint:disable
        console.log(err);
      });
  }

  public CreateColumn1(id) {
    return (
      <ul className="ullist">
        <li className="ullist">
          <Link to={"/source/view-gen1/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              View
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Link to={"/source/edit-gen1/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              Edit
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Button
            variant="outlined"
            color="primary"
            onClick={this.handleClickOpen1.bind(this, id)}
            style={{ border: "none", margin: 0 }}
          >
            Delete
          </Button>
          <Dialog
            open={this.state.open1a}
            onClose={this.handleClose1}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are You Sure You Want To Delete This?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose1} color="primary">
                No
              </Button>
              <Button
                onClick={this.handleSubmitGen1.bind(this, id)}
                color="primary"
                autoFocus
              >
                Yes
              </Button>
              <Dialog
                open={this.state.open1b}
                onClose={this.handleClose1}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Deleted
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose1} color="primary">
                    Ok
                  </Button>
                </DialogActions>
              </Dialog>
            </DialogActions>
          </Dialog>
        </li>
      </ul>
    );
  }

  public CreateColumn3(id) {
    return (
      <ul className="ullist">
        <li className="ullist">
          <Link to={"/source/view-gen3/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              View
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Link to={"/source/edit-gen3/" + id} style={style}>
            <Button
              variant="outlined"
              color="primary"
              style={{ border: "none", margin: 0 }}
            >
              Edit
            </Button>
          </Link>
        </li>
        <li className="ullist">
          <Button
            variant="outlined"
            color="primary"
            onClick={this.handleClickOpen3.bind(this, id)}
            style={{ border: "none", margin: 0 }}
          >
            Delete
          </Button>
          <Dialog
            open={this.state.open3a}
            onClose={this.handleClose3}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are You Sure You Want To Delete This?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose3} color="primary">
                No
              </Button>
              <Button
                onClick={this.handleSubmitGen3.bind(this, id)}
                color="primary"
                autoFocus
              >
                Yes
              </Button>
              <Dialog
                open={this.state.open3b}
                onClose={this.handleClose3}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">{"Alert!!!"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Deleted
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose3} color="primary">
                    Ok
                  </Button>
                </DialogActions>
              </Dialog>
            </DialogActions>
          </Dialog>
        </li>
      </ul>
    );
  }

  public render() {
    const SinglePhaseGenerator = this.state.listonephasegenerator.map(
      (ListFuel, id) => (
        <TableRow key={id} style={{ height: 50 }}>
          <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.name}</TableCell>
          <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.source_type}</TableCell>
          <TableCell style={{minWidth:100, maxWidth:100}} />
          <TableCell>{this.CreateColumn1(ListFuel.meter_id)}</TableCell>
        </TableRow>
      )
    );
    const SinglePhaseGridlength = this.state.listonephasegenerator.length;
    console.log(SinglePhaseGridlength);

    const ThreePhaseGenerator = this.state.listthreephasegenerator.map(
      (ListFuel, id) => (
        <TableRow key={id} style={{ height: 50 }}>
          <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.name}</TableCell>
          <TableCell style={{minWidth:200, maxWidth:200}}>{ListFuel.source_type}</TableCell>
          <TableCell style={{minWidth:100, maxWidth:100}} />
          <TableCell>{this.CreateColumn3(ListFuel.meter_id)}</TableCell>
        </TableRow>
      )
    );
    const ThreePhaseGridlength = this.state.listthreephasegenerator.length;
    console.log(ThreePhaseGridlength);

    return (
      <div>
        {/* <TableRow> */}
          {SinglePhaseGenerator}
          {ThreePhaseGenerator}
        {/* </TableRow> */}
        {/* </Table> */}
      </div>
    );
  }
}

export default ListGenerator;
