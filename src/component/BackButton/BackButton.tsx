import Button from "@material-ui/core/Button";
import * as React from "react";
import { withRouter } from "react-router";


class BackButton extends React.Component {
  constructor(props) {
    super(props);
  }


  public render() {
    return (
        <div className="button">
        <Button
          variant="contained"
          style={{ backgroundColor: "#337ab7" }}
          // @ts-ignore

          onClick={this.props.history.goBack}
        >
          Back
        </Button>
      </div>
    );
  }
}



export default withRouter(BackButton);
