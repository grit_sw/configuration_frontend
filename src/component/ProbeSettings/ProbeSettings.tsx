import "./ProbeSettings.css";

import * as React from "react";
import { Link } from "react-router-dom";


const style = {
  color: "#337ab7",
  textDecoration: "none"
};

class ProbeSettings extends React.Component {
  // @ts-ignore works
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <div>
        <ul className="lists">
          <li className="lists">
            <Link to="/probe/create-probe" style={style}>
              Create Probe
            </Link>
          </li>

          <li className="lists">
            <Link to="/probe/list-probe" style={style}>
              List Probes
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default ProbeSettings;
