// import ListBox from 'react-listbox';
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {
    default as Table,
    default as TableBody
} from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import DualListBox from "react-dual-listbox";
import "./CreateNewUserGroup.css";

const options = [
    { value: "luna", label: "Moon" },
    { value: "phobos", label: "Phobos" },
    { value: "deimos", label: "Deimos" },
    { value: "io", label: "Io" },
    { value: "europa", label: "Europa" },
    { value: "ganymede", label: "Ganymede" },
    { value: "callisto", label: "Callisto" },
    { value: "mimas", label: "Mimas" },
    { value: "enceladus", label: "Enceladus" },
    { value: "tethys", label: "Tethys" },
    { value: "rhea", label: "Rhea" },
    { value: "titan", label: "Titan" },
    { value: "iapetus", label: "Iapetus" }
];

interface Istate {
    config?: "";
    groupadmin?: "";
    open: boolean;
    profitmargin?: number;
    whitelabelling?
    sourceconfig?: "";
    value: "";
    name: [];
    configs: boolean;
    selected?: "";
}
class CreateNewUserGroup extends React.Component<{}, Istate> {
    // @ts-ignore works
    constructor(props) {
        super(props);
        this.state = {
            configs: true,
            groupadmin: "",
            name: [],
            open: true,
            sourceconfig: "",
            // profitmargin: '',
            value: ""
        };

        // this.handleChange = this.handleChange.bind(this);
        // this.handleClose = this.handleClose.bind(this);
    }

    // @ts-ignore works
    public handleChange = name => event => {
        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.value });
    };

    public handleClose = () => {
        this.setState({ open: false });
    };

    // @ts-ignore works
    public handleOpen = name => event => {
        // this.setState({ open: true });

        // @ts-ignore works
        this.setState({ ...this.State, [name]: event.target.open });
        // openState = this.state.open;
    };

    public onChange = selected => {
        // tslint:disable
        // console.log(selected);
        this.setState({
            selected: selected
        });
    };

    public render() {
        const { selected } = this.state;

        return (
            <div>
                <Table className="table">
                    <TableHead className="head">
                        <TableRow className="tablecell">
                            <TableCell className="tablecell">
                                <span id="tablecell">Create New User Group</span>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow className="tablerow">
                            <TableCell>Group Name</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                />
                            </TableCell>
                            <TableCell rowSpan={4} />
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Group Admin</TableCell>
                            <TableCell>
                                <Select
                                    value={this.state.groupadmin}
                                    onChange={this.handleChange("groupadmin")}
                                    name="groupadmin"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value="5">ok</MenuItem>
                                    <MenuItem>ten</MenuItem>
                                </Select>
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>Profit Margin</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    label="number"
                                    type="number"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.profitmargin}
                                    onChange={this.handleChange("profitmargin")}
                                    name="profitmargin"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell>White Labelling</TableCell>
                            <TableCell>
                                <TextField
                                    className="textField"
                                    margin="normal"
                                    variant="outlined"
                                    font-size="true"
                                    value={this.state.whitelabelling}
                                    onChange={this.handleChange("whitelabelling")}
                                    name="whitelabelling"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow className="tablerow">
                            <TableCell />
                            <TableCell>All Users</TableCell>
                            <TableCell>Group Members</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell />
                            <TableCell colSpan={3}>
                                <DualListBox
                                    name="moons"
                                    options={options}
                                    selected={selected}
                                    // tslint:disable
                                    onChange={this.onChange}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>
                                <Button
                                    variant="contained"
                                    style={{ backgroundColor: "#337ab7" }}
                                >
                                    Create User Group
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }

    // interface IConfigProps {
    //     configprops: {};
    // }

    // interface IConfigState {
    //     configState: {value: string};
    // }
}

export default CreateNewUserGroup;
